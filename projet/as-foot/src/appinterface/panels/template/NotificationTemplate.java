package appinterface.panels.template;

import appinterface.panels.ITemplate;
import appinterface.panels.PanelsManager;
import asfoot.dao.DaoManager;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

public class NotificationTemplate implements ITemplate {

    private AnchorPane pane;
    private String message;

    public void draw() {

        this.pane = new AnchorPane();
        this.pane.setTranslateY(470);
        this.pane.setTranslateX(13);
        this.pane.setMinWidth(PanelsManager.getInstance().getRoot().getScene().getWidth() - 26);
        this.pane.setMaxWidth(PanelsManager.getInstance().getRoot().getScene().getWidth() - 26);
        this.pane.setMinHeight(120);
        this.pane.setStyle("-fx-font-family: 'Trebuchet MS', Helvetica, sans-serif;-fx-padding:20px;-fx-background-color: white;-fx-border-radius: 20px;-fx-background-radius: 15;-fx-effect: dropshadow(three-pass-box, rgba(0,0,0,0.8), 10, 0, 0, 0);");


        Label label = new Label(this.message);
        label.setWrapText(true);
        label.setMaxWidth(PanelsManager.getInstance().getRoot().getScene().getWidth() - 26);
        label.setStyle("-fx-font-size: 20;");

        final Button closeButton = new Button();
        closeButton.setText("J'ai compris");
        this.pane.getChildren().add(closeButton);
        closeButton.setTranslateY(90);
        closeButton.setTranslateX(690);
        closeButton.setOnAction(e -> {
            PanelsManager.getInstance().getRoot().getChildren().remove(this.pane);
        });


        this.pane.getChildren().add(label);


    }

    public void render() {
        this.draw();
        PanelsManager.getInstance().getRoot().getChildren().add(this.pane);
    }


    public void setMessage(String message) {
        this.message = message;
    }

}
