package appinterface.panels;

public interface ITemplate {
    public void render();

    public void draw();
}
