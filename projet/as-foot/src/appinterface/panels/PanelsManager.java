package appinterface.panels;

import appinterface.panels.template.MenuTemplate;
import appinterface.panels.template.NotificationTemplate;
import appinterface.panels.types.equipes.ClassementEquipePanel;
import appinterface.panels.types.equipes.ListeEquipesPanel;
import appinterface.panels.types.equipes.ModifierEquipePanel;
import appinterface.panels.types.matchs.AjouterMatchPanel;
import appinterface.panels.types.matchs.ListeMatchsPanel;
import appinterface.panels.types.matchs.ModifierMatchPanel;
import appinterface.panels.types.profil.InscriptionPanel;
import appinterface.panels.types.profil.ProfilModificationPanel;
import appinterface.panels.types.profil.ProfilPanel;
import appinterface.panels.types.utilisateurs.AjouterUtilisateurPanel;
import appinterface.panels.types.utilisateurs.ListeUtilisateursPanel;
import appinterface.panels.types.utilisateurs.ModifierUtilisateurPanel;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.HashMap;

public class PanelsManager {
    private static PanelsManager panelsManagerInstance;

    private Group root;
    private Stage primaryStage;
    private Scene scene;
    private HashMap<String, IPanel> panels;
    private HashMap<String, ITemplate> templates;
    private String font = "http://anis.alwaysdata.net/asfoot/assets/fonts/UbuntuCondensed-Regular.ttf";


    public void init(Group root, Stage primaryStage, Scene scene) {
        this.root = root;
        this.primaryStage = primaryStage;
        this.scene = scene;
        this.panels = new HashMap<>();
        this.templates = new HashMap<>();
        System.out.print("PanelsManager prêt.\n");
        this.initPanels();
        this.initTemplates();

    }

    public Group getRoot() {
        return root;
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public Scene getScene() {
        return scene;
    }

    private void initPanels() {
        this.panels.put("ProfilPanel", new ProfilPanel());
        this.panels.put("InscriptionPanel", new InscriptionPanel());
        this.panels.put("ProfilModificationPanel", new ProfilModificationPanel());

        this.panels.put("ListeUtilisateursPanel", new ListeUtilisateursPanel());
        this.panels.put("ModifierUtilisateurPanel", new ModifierUtilisateurPanel());
        this.panels.put("AjouterUtilisateurPanel", new AjouterUtilisateurPanel());

        this.panels.put("AjouterMatchPanel", new AjouterMatchPanel());
        this.panels.put("ListeMatchsPanel", new ListeMatchsPanel());
        this.panels.put("ModifierMatchPanel", new ModifierMatchPanel());

        this.panels.put("ClassementEquipePanel", new ClassementEquipePanel());
        this.panels.put("ListeEquipesPanel", new ListeEquipesPanel());
        this.panels.put("ModifierEquipePanel", new ModifierEquipePanel());


        this.panels.get("ProfilPanel").render();
    }

    private void initTemplates() {
        this.templates.put("MenuTemplate", new MenuTemplate());
        this.templates.put("NotificationTemplate", new NotificationTemplate());
        this.drawTemplates();
    }


    public void setNotification(String message) {
        ITemplate template = this.templates.get("NotificationTemplate");
        if (template instanceof NotificationTemplate) {
            ((NotificationTemplate) template).setMessage(message);
            this.templates.get("NotificationTemplate").render();
        }
    }

    private void drawTemplates() {
        this.templates.get("MenuTemplate").render();
    }

    public static PanelsManager getInstance() {
        if (panelsManagerInstance == null)
            panelsManagerInstance = new PanelsManager();
        return panelsManagerInstance;
    }

    public String getFont() {
        return this.font;
    }

    public void renderPanel(String name) {
        this.root.getChildren().clear();
        this.drawTemplates();
        this.panels.get(name).getPane().getChildren().clear();
        this.panels.get(name).render();
    }


    public IPanel getPanel(String name) {
        return this.panels.get(name);
    }

}
