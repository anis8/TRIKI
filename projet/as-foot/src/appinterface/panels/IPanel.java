package appinterface.panels;

import javafx.scene.layout.AnchorPane;

public interface IPanel {

    public void render();

    public void draw();

    public AnchorPane getPane();
}
