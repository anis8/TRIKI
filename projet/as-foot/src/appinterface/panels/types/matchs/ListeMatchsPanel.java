package appinterface.panels.types.matchs;

import appinterface.panels.IPanel;
import appinterface.panels.PanelsManager;
import asfoot.matchs.Match;
import asfoot.matchs.MatchsManager;
import asfoot.matchs.types.MatchTermine;
import asfoot.utilisateurs.Utilisateur;
import asfoot.utilisateurs.UtilisateursManager;
import asfoot.utilisateurs.types.Dirigeant;
import asfoot.utilisateurs.types.Entraineur;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;

public class ListeMatchsPanel implements IPanel {

    AnchorPane pane;

    public ListeMatchsPanel() {
        this.pane = new AnchorPane();
        this.pane.setTranslateY(55);
        this.pane.setTranslateX(13);
        this.pane.setMinWidth(PanelsManager.getInstance().getRoot().getScene().getWidth() - 26);
        this.pane.setMinHeight(PanelsManager.getInstance().getRoot().getScene().getHeight() - 70);
        this.pane.setStyle("-fx-font-family: 'Trebuchet MS', Helvetica, sans-serif;");
    }

    @SuppressWarnings("Duplicates")
    public void draw() {
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setMinHeight(this.pane.getMinHeight());
        scrollPane.setMinWidth(this.pane.getMinWidth());

        TableView<Match> table = new TableView<>();
        table.setMinWidth(this.pane.getMinWidth());
        table.setMinHeight(this.pane.getMinHeight());

        TableColumn<Match, String> stade = new TableColumn<>("Stade");
        TableColumn<Match, String> equipeUne = new TableColumn<>("Equipe une");
        TableColumn<Match, String> equipeDeux = new TableColumn<>("Equipe deux");
        TableColumn<Match, String> date = new TableColumn<>("Date");
        TableColumn<Match, String> type = new TableColumn<>("Type");
        TableColumn<Match, String> score = new TableColumn<>("Score");


        stade.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getStade().getNom()));
        equipeUne.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getEquipeUne().getNom() + " - " + c.getValue().getEquipeUne().getCategorie().getNom()));
        equipeDeux.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getEquipeDeux().getNom() + " - " + c.getValue().getEquipeDeux().getCategorie().getNom()));
        date.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getDate()));
        type.setCellValueFactory(c -> new SimpleStringProperty((c.getValue() instanceof MatchTermine) ? "Terminé" : "En attente"));
        score.setCellValueFactory(c -> new SimpleStringProperty((c.getValue() instanceof MatchTermine) ? ((MatchTermine) c.getValue()).getEquipeUneButs() + " - " + ((MatchTermine) c.getValue()).getEquipeDeuxButs() : ""));

        for (Match match : MatchsManager.getInstance().getMatchs()) {
            table.getItems().addAll(match);
        }

        table.setRowFactory(tv -> {
            TableRow<Match> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (!row.isEmpty() && event.getButton() == MouseButton.PRIMARY
                        && event.getClickCount() == 2) {
                    Utilisateur connectedUtilisateur = UtilisateursManager.getInstance().getConnectedUtilisateur();
                    if(connectedUtilisateur instanceof Dirigeant || connectedUtilisateur instanceof Entraineur){
                        Match match = row.getItem();
                        IPanel panel = PanelsManager.getInstance().getPanel("ModifierMatchPanel");
                        if (panel instanceof ModifierMatchPanel) {
                            ((ModifierMatchPanel) panel).setMatch(match);
                        }
                        PanelsManager.getInstance().renderPanel("ModifierMatchPanel");
                    }
                }
            });
            return row;
        });

        table.getColumns().addAll(stade, equipeUne, equipeDeux, date,type,score);
        scrollPane.setContent(table);
        this.pane.getChildren().add(scrollPane);

    }

    public void render() {
        this.draw();
        PanelsManager.getInstance().getRoot().getChildren().add(this.pane);
    }

    public AnchorPane getPane() {
        return this.pane;
    }
}
