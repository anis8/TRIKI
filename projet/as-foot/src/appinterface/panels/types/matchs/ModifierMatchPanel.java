package appinterface.panels.types.matchs;

import appinterface.panels.IPanel;
import appinterface.panels.PanelsManager;
import asfoot.categories.CategoriesManager;
import asfoot.dao.DaoManager;
import asfoot.equipes.Equipe;
import asfoot.equipes.EquipesManager;
import asfoot.matchs.Match;
import asfoot.matchs.MatchsManager;
import asfoot.matchs.types.MatchTermine;
import asfoot.stades.Stade;
import asfoot.stades.StadesManager;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

public class ModifierMatchPanel implements IPanel {

    private AnchorPane pane;
    private Match match;

    public ModifierMatchPanel() {
        this.pane = new AnchorPane();
        this.pane.setTranslateY(55);
        this.pane.setTranslateX(13);
        this.pane.setMinWidth(PanelsManager.getInstance().getRoot().getScene().getWidth() - 26);
        this.pane.setMinHeight(PanelsManager.getInstance().getRoot().getScene().getHeight() - 70);
        this.pane.setStyle("-fx-font-family: 'Trebuchet MS', Helvetica, sans-serif;");
        this.match = null;
    }

    @SuppressWarnings("Duplicates")
    public void draw() {
        GridPane grid = new GridPane();
        grid.setMinWidth(this.pane.getMinWidth());
        grid.setMinHeight(this.pane.getMinHeight());
        grid.setAlignment(Pos.CENTER);


        Text scenetitle = new Text("Modifier un match");
        scenetitle.setStyle("-fx-font-size: 60px;");
        grid.add(scenetitle, 0, 0, 1, 1);

        Label stadeLabel = new Label("Stade:");
        grid.add(stadeLabel, 0, 1);
        stadeLabel.setStyle("-fx-font-size: 20px");


        ChoiceBox stadeChoice = new ChoiceBox();
        for (Stade stade : StadesManager.getInstance().getStades()) {
            stadeChoice.getItems().add(stade.getNom());
        }
        grid.add(stadeChoice, 0, 2);
        stadeChoice.setMinWidth(600);
        stadeChoice.setMinHeight(30);
        stadeChoice.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");
        stadeChoice.setValue(this.match.getStade().getNom());

        Label equipeUneLabel = new Label("Equipe une:");
        grid.add(equipeUneLabel, 0, 3);
        equipeUneLabel.setStyle("-fx-font-size: 20px");


        ChoiceBox equipeUneChoice = new ChoiceBox();
        for (Equipe equipe : EquipesManager.getInstance().getEquipes()) {
            equipeUneChoice.getItems().add(equipe.getNom() + " - " + equipe.getCategorie().getNom());
        }
        grid.add(equipeUneChoice, 0, 4);
        equipeUneChoice.setMinWidth(600);
        equipeUneChoice.setMinHeight(30);
        equipeUneChoice.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");
        equipeUneChoice.setValue(this.match.getEquipeUne().getNom() + " - " + this.match.getEquipeUne().getCategorie().getNom());

        Label equipeDeuxLabel = new Label("Equipe deux:");
        grid.add(equipeDeuxLabel, 0, 5);
        equipeDeuxLabel.setStyle("-fx-font-size: 20px");


        ChoiceBox equipeDeuxChoice = new ChoiceBox();
        for (Equipe equipe : EquipesManager.getInstance().getEquipes()) {
            equipeDeuxChoice.getItems().add(equipe.getNom() + " - " + equipe.getCategorie().getNom());
        }
        grid.add(equipeDeuxChoice, 0, 6);
        equipeDeuxChoice.setMinWidth(600);
        equipeDeuxChoice.setMinHeight(30);
        equipeDeuxChoice.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");
        equipeDeuxChoice.setValue(this.match.getEquipeDeux().getNom() + " - " + this.match.getEquipeDeux().getCategorie().getNom());


        Label dateLabel = new Label("Date:");
        grid.add(dateLabel, 0, 7);
        dateLabel.setStyle("-fx-font-size: 20px");

        TextField dateField = new TextField();
        grid.add(dateField, 0, 8);
        dateField.setMinWidth(300);
        dateField.setMinHeight(30);
        dateField.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");
        dateField.setText(this.match.getDate());


        Label matchTypeLabel = new Label("Etat du match");
        grid.add(matchTypeLabel, 0, 9);
        matchTypeLabel.setStyle("-fx-font-size: 20px");

        ChoiceBox matchTypeChoice = new ChoiceBox();
        matchTypeChoice.getItems().addAll("En attente", "Terminé");
        grid.add(matchTypeChoice, 0, 10);
        matchTypeChoice.setMinWidth(600);
        matchTypeChoice.setMinHeight(30);
        matchTypeChoice.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");
        if (this.match instanceof MatchTermine) {
            matchTypeChoice.setValue("Terminé");
        } else {
            matchTypeChoice.setValue("En attente");
        }


        Label equipeUneButsLabel = new Label("Equipe une buts:");
        grid.add(equipeUneButsLabel, 0, 11);
        equipeUneButsLabel.setStyle("-fx-font-size: 20px");

        TextField equipeUneButsField = new TextField();
        grid.add(equipeUneButsField, 0, 12);
        equipeUneButsField.setMinWidth(300);
        equipeUneButsField.setMinHeight(30);
        equipeUneButsField.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");
        if (this.match instanceof MatchTermine) {
            equipeUneButsField.setText(Integer.toString(((MatchTermine) this.match).getEquipeUneButs()));
        }


        Label equipeDeuxButsLabel = new Label("Equipe deux buts:");
        grid.add(equipeDeuxButsLabel, 0, 13);
        equipeDeuxButsLabel.setStyle("-fx-font-size: 20px");

        TextField equipeDeuxButsField = new TextField();
        grid.add(equipeDeuxButsField, 0, 14);
        equipeDeuxButsField.setMinWidth(300);
        equipeDeuxButsField.setMinHeight(30);
        equipeDeuxButsField.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");
        if (this.match instanceof MatchTermine) {
            equipeDeuxButsField.setText(Integer.toString(((MatchTermine) this.match).getEquipeDeuxButs()));
        }


        final Button ajouterButton = new Button();
        ajouterButton.setText("Mettre à jour");
        grid.add(ajouterButton, 0, 15);
        ajouterButton.setMinWidth(600);
        ajouterButton.setMinHeight(30);
        ajouterButton.setTranslateY(10);


        ajouterButton.setOnAction(e -> {
            if (stadeChoice.getValue() != null && equipeUneChoice.getValue() != null && equipeDeuxChoice.getValue() != null) {
                this.match.setStade(StadesManager.getInstance().getStadeByNom(stadeChoice.getValue().toString()));
                this.match.setEquipeUne(EquipesManager.getInstance().getEquipeByChoice(equipeUneChoice.getValue().toString()));
                this.match.setEquipeDeux(EquipesManager.getInstance().getEquipeByChoice(equipeDeuxChoice.getValue().toString()));
                this.match.setDate(dateField.getText());
                if (matchTypeChoice.getValue().toString().equals("Terminé") && (equipeUneButsField.getText().equals("") || equipeDeuxButsField.getText().equals(""))) {
                    PanelsManager.getInstance().setNotification("Veuillez remplir les champs vide.");
                    return;
                }


                if (matchTypeChoice.getValue().toString().equals("Terminé")) {
                    this.match = MatchsManager.getInstance().setMatchTermine(this.match, Integer.parseInt(equipeUneButsField.getText()), Integer.parseInt(equipeDeuxButsField.getText()));
                } else if (matchTypeChoice.getValue().toString().equals("En attente") && this.match instanceof MatchTermine) {
                    this.match = MatchsManager.getInstance().setMatchAttente(this.match);
                }


                DaoManager.getInstance().getMatchsDao().updateMatch(this.match);
                PanelsManager.getInstance().setNotification("Le match a bien été mis à jour.");
            } else {
                PanelsManager.getInstance().setNotification("Veuillez remplir les champs vide.");
            }

        });

        this.pane.getChildren().add(grid);
    }

    public void render() {
        this.draw();
        PanelsManager.getInstance().getRoot().getChildren().add(this.pane);
    }

    public AnchorPane getPane() {
        return this.pane;
    }

    public void setMatch(Match match) {
        this.match = match;
    }
}
