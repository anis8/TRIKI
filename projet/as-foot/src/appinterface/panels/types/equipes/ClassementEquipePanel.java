package appinterface.panels.types.equipes;

import appinterface.panels.IPanel;
import appinterface.panels.PanelsManager;
import appinterface.panels.types.matchs.ModifierMatchPanel;
import asfoot.equipes.Equipe;
import asfoot.equipes.EquipesManager;
import asfoot.matchs.Match;
import asfoot.matchs.MatchsManager;
import asfoot.matchs.types.MatchTermine;
import asfoot.utilisateurs.Utilisateur;
import asfoot.utilisateurs.UtilisateursManager;
import asfoot.utilisateurs.types.Dirigeant;
import asfoot.utilisateurs.types.Entraineur;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;

public class ClassementEquipePanel implements IPanel {

    private AnchorPane pane;

    public ClassementEquipePanel() {
        this.pane = new AnchorPane();
        this.pane.setTranslateY(55);
        this.pane.setTranslateX(13);
        this.pane.setMinWidth(PanelsManager.getInstance().getRoot().getScene().getWidth() - 26);
        this.pane.setMinHeight(PanelsManager.getInstance().getRoot().getScene().getHeight() - 70);
        this.pane.setStyle("-fx-font-family: 'Trebuchet MS', Helvetica, sans-serif;");
    }

    @SuppressWarnings("Duplicates")
    public void draw() {
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setMinHeight(this.pane.getMinHeight());
        scrollPane.setMinWidth(this.pane.getMinWidth());

        TableView<Equipe> table = new TableView<>();
        table.setMinWidth(this.pane.getMinWidth());
        table.setMinHeight(this.pane.getMinHeight());

        TableColumn<Equipe, String> nom = new TableColumn<>("Nom de l'équipe");
        TableColumn<Equipe, String> categorie = new TableColumn<>("Catégorie");
        TableColumn<Equipe, String> points = new TableColumn<>("Points");
        TableColumn<Equipe, String> effectif = new TableColumn<>("Effectif");
        TableColumn<Equipe, String> buts = new TableColumn<>("Nombre de buts");


        nom.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getNom()));
        categorie.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getCategorie().getNom()));
        points.setCellValueFactory(c -> new SimpleStringProperty(Integer.toString(c.getValue().getPoints())));
        effectif.setCellValueFactory(c -> new SimpleStringProperty(Integer.toString(c.getValue().getJoueurs().size())));
        buts.setCellValueFactory(c -> new SimpleStringProperty(Integer.toString(c.getValue().getNombreButs())));

        for (Equipe equipe : EquipesManager.getInstance().getEquipes()) {
            table.getItems().addAll(equipe);
        }

        table.getColumns().addAll(nom, categorie, points, effectif, buts);
        table.setEditable(true);
        scrollPane.setContent(table);
        this.pane.getChildren().add(scrollPane);

    }

    public void render() {
        this.draw();
        PanelsManager.getInstance().getRoot().getChildren().add(this.pane);
    }

    public AnchorPane getPane() {
        return this.pane;
    }
}

