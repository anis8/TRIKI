package appinterface.panels.types.profil;

import appinterface.panels.IPanel;
import appinterface.panels.PanelsManager;
import asfoot.categories.Categorie;
import asfoot.categories.CategoriesManager;
import asfoot.dao.DaoManager;
import asfoot.utilisateurs.Utilisateur;
import asfoot.utilisateurs.UtilisateursManager;
import asfoot.utilisateurs.types.Dirigeant;
import asfoot.utilisateurs.types.Entraineur;
import asfoot.utilisateurs.types.Joueur;
import asfoot.utilisateurs.types.Parent;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import jdk.jshell.execution.Util;

public class InscriptionPanel implements IPanel {

    AnchorPane pane;

    public InscriptionPanel() {
        this.pane = new AnchorPane();
        this.pane.setTranslateY(55);
        this.pane.setTranslateX(13);
        this.pane.setMinWidth(PanelsManager.getInstance().getRoot().getScene().getWidth() - 26);
        this.pane.setMinHeight(PanelsManager.getInstance().getRoot().getScene().getHeight() - 70);
        this.pane.setStyle("-fx-font-family: 'Trebuchet MS', Helvetica, sans-serif;");
    }

    @SuppressWarnings("Duplicates")
    public void draw() {
        GridPane grid = new GridPane();
        grid.setMinWidth(this.pane.getMinWidth());
        grid.setMinHeight(this.pane.getMinHeight());
        grid.setAlignment(Pos.CENTER);


        Text scenetitle = new Text("Inscription");
        scenetitle.setStyle("-fx-font-size: 60px;");
        grid.add(scenetitle, 0, 0, 2, 1);

        Label mailLabel = new Label("Adresse email:");
        grid.add(mailLabel, 0, 1);
        mailLabel.setStyle("-fx-font-size: 20px");

        TextField mailField = new TextField();
        grid.add(mailField, 0, 2);
        mailField.setMinWidth(300);
        mailField.setMinHeight(40);
        mailField.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");


        Label passwordLabel = new Label("Mot de passe:");
        grid.add(passwordLabel, 0, 3);
        passwordLabel.setStyle("-fx-font-size: 20px");

        TextField passwordField = new PasswordField();
        grid.add(passwordField, 0, 4);
        passwordField.setMinWidth(300);
        passwordField.setMinHeight(40);
        passwordField.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");


        Label numLabel = new Label("Numéro de téléphone:");
        grid.add(numLabel, 0, 5);
        numLabel.setStyle("-fx-font-size: 20px");

        TextField numField = new TextField();
        grid.add(numField, 0, 6);
        numField.setMinWidth(300);
        numField.setMinHeight(40);
        numField.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");


        Label nomLabel = new Label("Nom:");
        grid.add(nomLabel, 0, 7);
        nomLabel.setStyle("-fx-font-size: 20px");

        TextField nomField = new TextField();
        grid.add(nomField, 0, 8);
        nomField.setMinWidth(300);
        nomField.setMinHeight(40);
        nomField.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");


        Label prenomLabel = new Label("Prenom:");
        grid.add(prenomLabel, 0, 9);
        prenomLabel.setStyle("-fx-font-size: 20px");

        TextField prenomField = new TextField();
        grid.add(prenomField, 0, 10);
        prenomField.setMinWidth(300);
        prenomField.setMinHeight(40);
        prenomField.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");


        Label typeLabel = new Label("Type d'utilisateur:");
        grid.add(typeLabel, 0, 11);
        typeLabel.setStyle("-fx-font-size: 20px");


        ChoiceBox typeChoice = new ChoiceBox();
        typeChoice.getItems().addAll("Parent", "Dirigeant", "Entraineur", "Joueur");
        grid.add(typeChoice, 0, 12);
        typeChoice.setMinWidth(300);
        typeChoice.setMinHeight(40);
        typeChoice.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");


        final Button inscriptionButton = new Button();
        inscriptionButton.setText("Inscription");
        grid.add(inscriptionButton, 0, 13);
        inscriptionButton.setMinWidth(300);
        inscriptionButton.setMinHeight(30);
        inscriptionButton.setTranslateY(10);

        inscriptionButton.setOnAction(e -> {
            if (typeChoice.getValue() != null && !mailField.getText().equals("") && !numField.getText().equals("") && !nomField.getText().equals("") && !prenomField.getText().equals("")) {
                System.out.println(typeChoice.getValue().toString());
                Utilisateur utilisateur = null;
                switch (typeChoice.getValue().toString()) {
                    case "Parent":
                        utilisateur = new Parent(0, mailField.getText(), numField.getText(), nomField.getText(), prenomField.getText());
                        break;
                    case "Dirigeant":
                        utilisateur = new Dirigeant(0, mailField.getText(), numField.getText(), nomField.getText(), prenomField.getText(), "Aucune");
                        break;
                    case "Entraineur":
                        utilisateur = new Entraineur(0, mailField.getText(), numField.getText(), nomField.getText(), prenomField.getText());
                        break;
                    case "Joueur":
                        utilisateur = new Joueur(0, mailField.getText(), numField.getText(), nomField.getText(), prenomField.getText(), CategoriesManager.getInstance().getCategories().get(0),"Aucun",0,"","Aucun");
                        break;
                        default:
                }

                if(utilisateur != null){
                    utilisateur.setMdp(passwordField.getText());
                    UtilisateursManager.getInstance().addUtilisateur(utilisateur);
                    UtilisateursManager.getInstance().setConnectedUtilisateur(utilisateur);
                    DaoManager.getInstance().getUtilisateursDao().createUtilisateur(utilisateur);
                    PanelsManager.getInstance().renderPanel("ProfilPanel");
                }

            } else {
                PanelsManager.getInstance().setNotification("Veuillez remplir les champs vides.");
            }

        });


        this.pane.getChildren().add(grid);
    }

    public void render() {
        this.draw();
        PanelsManager.getInstance().getRoot().getChildren().add(this.pane);
    }

    public AnchorPane getPane() {
        return this.pane;
    }
}
