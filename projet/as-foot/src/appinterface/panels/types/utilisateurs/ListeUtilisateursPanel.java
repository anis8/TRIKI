package appinterface.panels.types.utilisateurs;

import appinterface.panels.IPanel;
import appinterface.panels.PanelsManager;
import asfoot.utilisateurs.Utilisateur;
import asfoot.utilisateurs.UtilisateursManager;
import asfoot.utilisateurs.types.Dirigeant;
import asfoot.utilisateurs.types.Entraineur;
import asfoot.utilisateurs.types.Joueur;
import asfoot.utilisateurs.types.Parent;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;

public class ListeUtilisateursPanel implements IPanel {

    AnchorPane pane;

    public ListeUtilisateursPanel() {
        this.pane = new AnchorPane();
        this.pane.setTranslateY(55);
        this.pane.setTranslateX(13);
        this.pane.setMinWidth(PanelsManager.getInstance().getRoot().getScene().getWidth() - 26);
        this.pane.setMinHeight(PanelsManager.getInstance().getRoot().getScene().getHeight() - 70);
        this.pane.setStyle("-fx-font-family: 'Trebuchet MS', Helvetica, sans-serif;");
    }

    @SuppressWarnings("Duplicates")
    public void draw() {

        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setMinHeight(this.pane.getMinHeight());
        scrollPane.setMinWidth(this.pane.getMinWidth());

        TableView<Utilisateur> table = new TableView<>();
        table.setMinWidth(this.pane.getMinWidth());
        table.setMinHeight(this.pane.getMinHeight());


        TableColumn<Utilisateur, String> email = new TableColumn<>("Email");
        TableColumn<Utilisateur, String> numeroTel = new TableColumn<>("Numéro de téléphone");
        TableColumn<Utilisateur, String> nom = new TableColumn<>("Nom");
        TableColumn<Utilisateur, String> prenom = new TableColumn<>("Prénom");
        TableColumn<Utilisateur, String> type = new TableColumn<>("Type");


        email.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getEmail()));
        numeroTel.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getNumeroTel()));
        nom.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getNom()));
        prenom.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getPrenom()));
        type.setCellValueFactory(c -> new SimpleStringProperty(UtilisateursManager.getInstance().getUtilisateurType(c.getValue())));


        for (Utilisateur utilisateur : UtilisateursManager.getInstance().getUtilisateurs()) {
            table.getItems().addAll(utilisateur);
        }

        table.setRowFactory(tv -> {
            TableRow<Utilisateur> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (!row.isEmpty() && event.getButton() == MouseButton.PRIMARY
                        && event.getClickCount() == 2) {

                    Utilisateur utilisateur = row.getItem();
                    IPanel panel = PanelsManager.getInstance().getPanel("ModifierUtilisateurPanel");
                    if (panel instanceof ModifierUtilisateurPanel) {
                        ((ModifierUtilisateurPanel) panel).setUtilisateur(utilisateur);
                    }
                    PanelsManager.getInstance().renderPanel("ModifierUtilisateurPanel");
                }
            });
            return row;
        });

        table.getColumns().addAll(email, numeroTel, nom, prenom, type);
        scrollPane.setContent(table);
        this.pane.getChildren().add(scrollPane);

    }

    public void render() {
        this.draw();
        PanelsManager.getInstance().getRoot().getChildren().add(this.pane);
    }

    public AnchorPane getPane() {
        return this.pane;
    }
}
