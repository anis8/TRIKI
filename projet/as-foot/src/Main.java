
import asfoot.categories.CategoriesManager;
import asfoot.dao.DaoManager;
import asfoot.dao.exception.DaoException;
import asfoot.equipes.EquipesManager;
import asfoot.matchs.MatchsManager;
import asfoot.stades.StadesManager;
import asfoot.utilisateurs.UtilisateursManager;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import appinterface.panels.PanelsManager;

public class Main extends Application {

    private Group root;
    private Stage primaryStage;
    private Scene scene;

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws DaoException {
        this.primaryStage = primaryStage;
        this.root = new Group();

        this.primaryStage.setTitle("AS FOOT");
        this.scene = new Scene(this.root, 800, 600, Color.WHITE);
        this.primaryStage.setScene(this.scene);
        this.primaryStage.show();
        this.initManagers();
    }

    private void initManagers() throws DaoException {
        DaoManager.getInstance().init();
        StadesManager.getInstance().init();
        CategoriesManager.getInstance().init();
        StadesManager.getInstance().init();
        UtilisateursManager.getInstance().init();
        EquipesManager.getInstance().init();
        MatchsManager.getInstance().init();

        PanelsManager.getInstance().init(this.root, this.primaryStage, this.scene);


    }
}
