package asfoot.matchs;

import asfoot.dao.DaoManager;
import asfoot.matchs.types.MatchTermine;

import java.util.ArrayList;

public class MatchsManager {

    private static MatchsManager matchsManagerInstance;
    private ArrayList<Match> matchs;

    public void init() {

        this.matchs = DaoManager.getInstance().getMatchsDao().getMatchs();
        System.out.print("Gestionnaire de matchs prêt.\n");
    }

    public void addMatch(Match match) {
        this.matchs.add(match);
    }

    public void removeMatch(Match match) {
        this.matchs.remove(match);
    }

    public MatchTermine setMatchTermine(Match match, int equipeUneButs, int equipeDeuxButs) {
        MatchTermine matchTermine = new MatchTermine(match.getStade(), match.getEquipeUne(), match.getEquipeDeux(), match.getId(), match.getDate(), equipeUneButs, equipeDeuxButs);
        this.removeMatch(match);
        this.addMatch(matchTermine);
        return matchTermine;
    }

    public Match setMatchAttente(Match match) {
        Match matchAttente = new Match(match.getStade(), match.getEquipeUne(), match.getEquipeDeux(), match.getId(), match.getDate());
        this.removeMatch(match);
        this.addMatch(matchAttente);
        return matchAttente;
    }

    public ArrayList<Match> getMatchs() {
        return this.matchs;
    }

    public static MatchsManager getInstance() {

        if (matchsManagerInstance == null)
            matchsManagerInstance = new MatchsManager();
        return matchsManagerInstance;
    }
}
