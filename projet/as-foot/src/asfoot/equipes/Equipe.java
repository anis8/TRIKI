package asfoot.equipes;

import asfoot.categories.Categorie;
import asfoot.matchs.Match;
import asfoot.matchs.MatchsManager;
import asfoot.matchs.types.MatchTermine;
import asfoot.utilisateurs.types.Entraineur;
import asfoot.utilisateurs.types.Joueur;

import java.util.ArrayList;

/**
 * Classe Equipe
 * <p>
 * Cette classe représente une équipe
 *
 * @author Anis
 */
public class Equipe {

    private int id;
    private Categorie categorie;
    private String nom;
    private String ecusson;
    private ArrayList<Joueur> joueurs;
    private ArrayList<Entraineur> entraineurs;

    /**
     * Constructeur
     *
     * @param categorie   Categorie
     * @param nom         string
     * @param ecusson     string
     * @param joueurs     ArrayList<Joueur>
     * @param entraineurs ArrayList<Entraineur>
     */
    public Equipe(Categorie categorie, String nom, String ecusson, ArrayList<Joueur> joueurs, ArrayList<Entraineur> entraineurs) {
        this.categorie = categorie;
        this.nom = nom;
        this.ecusson = ecusson;
        this.joueurs = joueurs;
        this.entraineurs = entraineurs;
        this.id = 0;
    }

    public Equipe(Categorie categorie, String nom, String ecusson) {
        this.categorie = categorie;
        this.nom = nom;
        this.ecusson = ecusson;
        this.joueurs = new ArrayList<>();
        this.entraineurs = new ArrayList<>();
        this.id = 0;
    }

    public Equipe(Categorie categorie, String nom, String ecusson, int id) {
        this.categorie = categorie;
        this.nom = nom;
        this.ecusson = ecusson;
        this.joueurs = new ArrayList<>();
        this.entraineurs = new ArrayList<>();
        this.id = id;
    }

    /**
     * Permet d'ajouter un joueur
     *
     * @param joueur Joueur
     */

    public void addJoueur(Joueur joueur) {
        if (!this.joueurs.contains(joueur)) {
            this.joueurs.add(joueur);
        }
    }

    public void removeJoueur(Joueur joueur) {
        this.joueurs.remove(joueur);

    }

    public void removeEntraineur(Entraineur entraineur) {
        this.entraineurs.remove(entraineur);
    }

    /**
     * Permet d'ajouter un entraineur
     *
     * @param entraineur Entraineur
     */
    public void addEntraineur(Entraineur entraineur) {
        if (!this.entraineurs.contains(entraineur)) {
            this.entraineurs.add(entraineur);
        }

    }

    /**
     * Retourne la catégorie
     *
     * @return categorie
     */
    public Categorie getCategorie() {
        return categorie;
    }

    /**
     * Met à jour la catégorie
     *
     * @param categorie Categorie
     */
    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    /**
     * Retourne le nom de l'équipe
     *
     * @return nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * Met à jour le nom de l'équipe
     *
     * @param nom string
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Retourne l'écusson
     *
     * @return ecusson
     */

    public String getEcusson() {
        return ecusson;
    }

    /**
     * Met à jour l'écusson
     *
     * @param ecusson string
     */
    public void setEcusson(String ecusson) {
        this.ecusson = ecusson;
    }

    /**
     * Retourne les joueurs
     *
     * @return joueurs
     */
    public ArrayList<Joueur> getJoueurs() {
        return joueurs;
    }

    /**
     * Met à jour la liste des joueurs
     *
     * @param joueurs ArrayList<Joueur>
     */
    public void setJoueurs(ArrayList<Joueur> joueurs) {
        this.joueurs = joueurs;
    }

    /**
     * Retourne la liste des entraineurs
     *
     * @return ArrayList<Entraineur>
     */
    public ArrayList<Entraineur> getEntraineurs() {
        return entraineurs;
    }

    /**
     * Met à jour les entraineurs
     *
     * @param entraineurs ArrayList<Entraineur>
     */
    public void setEntraineurs(ArrayList<Entraineur> entraineurs) {
        this.entraineurs = entraineurs;
    }

    public int getId() {
        return this.id;
    }

    public int getPoints() {
        int points = 0;
        for (Match match : MatchsManager.getInstance().getMatchs()) {
            if (match instanceof MatchTermine) {
                if (match.getEquipeUne() == this && ((MatchTermine) match).getEquipeUneButs() > ((MatchTermine) match).getEquipeDeuxButs()) {
                    points = points + 3;
                }
                if (match.getEquipeDeux() == this && ((MatchTermine) match).getEquipeDeuxButs() > ((MatchTermine) match).getEquipeUneButs()) {
                    points = points + 3;
                }
                if ((match.getEquipeDeux() == this || match.getEquipeUne() == this) && ((MatchTermine) match).getEquipeUneButs() == ((MatchTermine) match).getEquipeDeuxButs()) {
                    points = points + 1;
                }
            }

        }
        return points;
    }

    public int getNombreButs() {
        int buts = 0;
        for (Match match : MatchsManager.getInstance().getMatchs()) {
            if (match instanceof MatchTermine) {
                if (match.getEquipeUne() == this) {
                    buts = buts + ((MatchTermine) match).getEquipeUneButs();
                }
                if (match.getEquipeDeux() == this) {
                    buts = buts + ((MatchTermine) match).getEquipeDeuxButs();
                }
            }

        }
        return buts;
    }

    public void setId(int id) {
        this.id = id;
    }
}
