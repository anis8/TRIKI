package asfoot.equipes;

import asfoot.dao.DaoManager;

import java.util.ArrayList;

/**
 * Classe EquipeManager
 * <p>
 * Permet d'assurer la gestion des équipes
 *
 * @author Anis
 */
public class EquipesManager {

    private static EquipesManager equipesManagerInstance;
    private ArrayList<Equipe> equipes;


    /**
     * Permet d'initialiser la classe
     */
    public void init() {
        this.equipes = DaoManager.getInstance().getEquipesDao().getEquipes();
        System.out.print("Gestionnaire d'équipes prêt. \n");
    }

    /**
     * Permet de récupérer les équipes
     *
     * @return equipes
     */
    public ArrayList<Equipe> getEquipes() {
        return this.equipes;
    }

    public Equipe getEquipe(int id) {
        for (Equipe equipe : this.equipes) {
            if (equipe.getId() == id) {
                return equipe;
            }
        }
        return null;
    }

    public Equipe getEquipeByChoice(String choice) {
        for (Equipe equipe : this.equipes) {
            if ((equipe.getNom() + " - " + equipe.getCategorie().getNom()).equals(choice)) {
                return equipe;
            }
        }
        return null;
    }

    /**
     * Permet d'ajouter une équipe
     *
     * @param equipe Equipe
     */
    public void addEquipe(Equipe equipe) {
        this.equipes.add(equipe);
    }

    /**
     * Permet de récupérer l'instance de la classe
     *
     * @return equipesManagerInstance
     */
    public static EquipesManager getInstance() {

        if (equipesManagerInstance == null)
            equipesManagerInstance = new EquipesManager();
        return equipesManagerInstance;
    }
}
