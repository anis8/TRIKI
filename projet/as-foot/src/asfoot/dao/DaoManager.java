package asfoot.dao;

import asfoot.dao.exception.DaoException;
import asfoot.dao.types.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DaoManager {

    private static DaoManager daoManagerInstance;
    private Connection connection;
    private StadesDao stadesDao;
    private CategoriesDao categoriesDao;
    private UtilisateursDao utilisateursDao;
    private EquipesDao equipesDao;
    private MatchsDao matchsDao;


    public void init() throws DaoException {
        this.initDaos();
        try {
            Class.forName("org.mariadb.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mariadb://mysql-anis.alwaysdata.net/anis_asfoot", "anis", "mpnmpa66");
            System.out.print("DaoManager prêt. \n");
        } catch (ClassNotFoundException e) {
            throw new DaoException("Problème de connexion car driver introuvable", e);
        } catch (SQLException e) {
            throw new DaoException("Problème de connexion à la base de donnée", e);
        }
    }

    public Connection getConnection() {
        return this.connection;
    }

    public void closeConnection() throws DaoException {
        try {
            this.connection.close();
        } catch (SQLException e) {
            throw new DaoException("Impossible de fermer la connexion", e);
        }

    }

    public int getRowCount(ResultSet set) throws SQLException {
        int rowCount;
        int currentRow = set.getRow();
        rowCount = set.last() ? set.getRow() : 0;
        if (currentRow == 0)
            set.beforeFirst();
        else
            set.absolute(currentRow);
        return rowCount;
    }

    public static DaoManager getInstance() {
        if (daoManagerInstance == null)
            daoManagerInstance = new DaoManager();
        return daoManagerInstance;
    }

    private void initDaos() {
        this.stadesDao = new StadesDao();
        this.categoriesDao = new CategoriesDao();
        this.utilisateursDao = new UtilisateursDao();
        this.equipesDao = new EquipesDao();
        this.matchsDao = new MatchsDao();
    }

    public StadesDao getStadesDao() {
        return stadesDao;
    }

    public CategoriesDao getCategoriesDao() {
        return categoriesDao;
    }

    public EquipesDao getEquipesDao() {
        return this.equipesDao;
    }

    public UtilisateursDao getUtilisateursDao() {
        return utilisateursDao;
    }

    public MatchsDao getMatchsDao() {
        return this.matchsDao;
    }
}
