package asfoot.dao.types;

import asfoot.categories.CategoriesManager;
import asfoot.dao.DaoManager;
import asfoot.utilisateurs.Utilisateur;
import asfoot.utilisateurs.UtilisateursManager;
import asfoot.utilisateurs.types.Dirigeant;
import asfoot.utilisateurs.types.Entraineur;
import asfoot.utilisateurs.types.Joueur;
import asfoot.utilisateurs.types.Parent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UtilisateursDao {

    public ArrayList<Utilisateur> getUtilisateurs() {
        ResultSet utilisateurSet;
        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = DaoManager.getInstance().getConnection();
        ArrayList<Utilisateur> utilisateurs = new ArrayList<>();

        int userId;
        ResultSet resultSet;
        try {
            preparedStatement = sqlConnection.prepareStatement("SELECT * FROM utilisateurs");
            utilisateurSet = preparedStatement.executeQuery();
            while (utilisateurSet.next()) {
                userId = utilisateurSet.getInt(1);

                preparedStatement = sqlConnection.prepareStatement("SELECT * FROM utilisateur_dirigeant WHERE utilisateur_id = ?");
                preparedStatement.setInt(1, userId);
                resultSet = preparedStatement.executeQuery();
                if (DaoManager.getInstance().getRowCount(resultSet) > 0 && resultSet.next()) {
                    utilisateurs.add(new Dirigeant(
                            userId,
                            utilisateurSet.getString("email"),
                            utilisateurSet.getString("tel"),
                            utilisateurSet.getString("nom"),
                            utilisateurSet.getString("prenom"),
                            resultSet.getString("fonction")
                    ));
                }
                preparedStatement = sqlConnection.prepareStatement("SELECT * FROM utilisateur_entraineur WHERE utilisateur_id = ?");
                preparedStatement.setInt(1, userId);
                resultSet = preparedStatement.executeQuery();
                if (DaoManager.getInstance().getRowCount(resultSet) > 0 && resultSet.next()) {
                    utilisateurs.add(new Entraineur(
                            userId,
                            utilisateurSet.getString("email"),
                            utilisateurSet.getString("tel"),
                            utilisateurSet.getString("nom"),
                            utilisateurSet.getString("prenom")
                    ));
                }
                preparedStatement = sqlConnection.prepareStatement("SELECT * FROM utilisateur_joueur WHERE utilisateur_id = ?");
                preparedStatement.setInt(1, userId);
                resultSet = preparedStatement.executeQuery();
                if (DaoManager.getInstance().getRowCount(resultSet) > 0 && resultSet.next()) {
                    utilisateurs.add(new Joueur(
                            userId,
                            utilisateurSet.getString("email"),
                            utilisateurSet.getString("tel"),
                            utilisateurSet.getString("nom"),
                            utilisateurSet.getString("prenom"),
                            CategoriesManager.getInstance().getCategorie(resultSet.getInt("categorie_id")),
                            resultSet.getString("poste"),
                            resultSet.getInt("numero"),
                            resultSet.getString("datenaissance"),
                            resultSet.getString("photo")

                    ));
                }
                preparedStatement = sqlConnection.prepareStatement("SELECT * FROM utilisateur_parent WHERE utilisateur_id = ?");
                preparedStatement.setInt(1, userId);
                resultSet = preparedStatement.executeQuery();
                if (DaoManager.getInstance().getRowCount(resultSet) > 0 && resultSet.next()) {
                    utilisateurs.add(new Parent(
                            userId,
                            utilisateurSet.getString("email"),
                            utilisateurSet.getString("tel"),
                            utilisateurSet.getString("nom"),
                            utilisateurSet.getString("prenom")
                    ));
                }
            }
            utilisateurSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return utilisateurs;
    }

    @SuppressWarnings("Duplicates")
    public void createUtilisateur(Utilisateur utilisateur) {

        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = DaoManager.getInstance().getConnection();
        try {
            preparedStatement = sqlConnection.prepareStatement("INSERT INTO utilisateurs (email,mdp,tel,nom,prenom) VALUES (?,?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, utilisateur.getEmail());
            preparedStatement.setString(2, utilisateur.getMdp());
            preparedStatement.setString(3, utilisateur.getNumeroTel());
            preparedStatement.setString(4, utilisateur.getNom());
            preparedStatement.setString(5, utilisateur.getPrenom());
            preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                utilisateur.setId(rs.getInt(1));
            }
            preparedStatement.close();


            if (utilisateur instanceof Dirigeant) {
                preparedStatement = sqlConnection.prepareStatement("INSERT INTO utilisateur_dirigeant (utilisateur_id,fonction) VALUES (?,?)");
                preparedStatement.setInt(1, utilisateur.getId());
                preparedStatement.setString(2, ((Dirigeant) utilisateur).getFonction());
                preparedStatement.executeUpdate();
                preparedStatement.close();
            }

            if (utilisateur instanceof Entraineur) {
                preparedStatement = sqlConnection.prepareStatement("INSERT INTO utilisateur_entraineur (utilisateur_id) VALUES (?)");
                preparedStatement.setInt(1, utilisateur.getId());
                preparedStatement.executeUpdate();
                preparedStatement.close();
            }

            if (utilisateur instanceof Joueur) {
                preparedStatement = sqlConnection.prepareStatement("INSERT INTO utilisateur_joueur (utilisateur_id,categorie_id,poste,numero,datenaissance,photo) VALUES (?,?,?,?,?,?)");
                preparedStatement.setInt(1, utilisateur.getId());
                preparedStatement.setInt(2, ((Joueur) utilisateur).getCategorie().getId());
                preparedStatement.setString(3, ((Joueur) utilisateur).getPoste());
                preparedStatement.setInt(4, ((Joueur) utilisateur).getNumero());
                preparedStatement.setString(5, ((Joueur) utilisateur).getDatenaissance());
                preparedStatement.setString(6, ((Joueur) utilisateur).getPhoto());
                preparedStatement.executeUpdate();
                preparedStatement.close();
            }

            if (utilisateur instanceof Parent) {
                preparedStatement = sqlConnection.prepareStatement("INSERT INTO utilisateur_parent (utilisateur_id) VALUES (?)");
                preparedStatement.setInt(1, utilisateur.getId());
                preparedStatement.executeUpdate();
                preparedStatement.close();

                for (Joueur joueur : ((Parent) utilisateur).getEnfants()) {
                    preparedStatement = sqlConnection.prepareStatement("INSERT INTO parents (utilisateur_parent,utiilisateur_joueur) VALUES (?,?)");
                    preparedStatement.setInt(1, utilisateur.getId());
                    preparedStatement.setInt(2, joueur.getId());
                    preparedStatement.executeUpdate();
                    preparedStatement.close();
                }

            }


        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void updateUtilisateur(Utilisateur utilisateur) {

        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = DaoManager.getInstance().getConnection();
        try {
            if(utilisateur.getMdp() == null || utilisateur.getMdp().equals("")){
                preparedStatement = sqlConnection.prepareStatement("UPDATE utilisateurs SET email = ?,tel = ?,nom = ?,prenom = ? WHERE id = ?");
                preparedStatement.setString(1, utilisateur.getEmail());
                preparedStatement.setString(2, utilisateur.getNumeroTel());
                preparedStatement.setString(3, utilisateur.getNom());
                preparedStatement.setString(4, utilisateur.getPrenom());
                preparedStatement.setInt(5, utilisateur.getId());
                preparedStatement.executeUpdate();
                preparedStatement.close();
            } else {
                preparedStatement = sqlConnection.prepareStatement("UPDATE utilisateurs SET email = ?,mdp = ?,tel = ?,nom = ?,prenom = ? WHERE id = ?");
                preparedStatement.setString(1, utilisateur.getEmail());
                preparedStatement.setString(2, utilisateur.getMdp());
                preparedStatement.setString(3, utilisateur.getNumeroTel());
                preparedStatement.setString(4, utilisateur.getNom());
                preparedStatement.setString(5, utilisateur.getPrenom());
                preparedStatement.setInt(6, utilisateur.getId());
                preparedStatement.executeUpdate();
                preparedStatement.close();
            }


            if (utilisateur instanceof Dirigeant) {
                preparedStatement = sqlConnection.prepareStatement("UPDATE utilisateur_dirigeant SET fonction = ? WHERE utilisateur_id = ?");
                preparedStatement.setString(1, ((Dirigeant) utilisateur).getFonction());
                preparedStatement.setInt(2, utilisateur.getId());
                preparedStatement.executeUpdate();
                preparedStatement.close();
            }

            if (utilisateur instanceof Entraineur) {
                preparedStatement = sqlConnection.prepareStatement("UPDATE utilisateur_entraineur SET utilisateur_id = ? WHERE utilisateur_id = ?");
                preparedStatement.setInt(1, utilisateur.getId());
                preparedStatement.setInt(2, utilisateur.getId());
                preparedStatement.executeUpdate();
                preparedStatement.close();
            }

            if (utilisateur instanceof Joueur) {
                preparedStatement = sqlConnection.prepareStatement("UPDATE utilisateur_joueur SET categorie_id = ?,poste = ?,numero = ?,datenaissance = ?,photo = ? WHERE utilisateur_id = ?");
                preparedStatement.setInt(1, ((Joueur) utilisateur).getCategorie().getId());
                preparedStatement.setString(2, ((Joueur) utilisateur).getPoste());
                preparedStatement.setInt(3, ((Joueur) utilisateur).getNumero());
                preparedStatement.setString(4, ((Joueur) utilisateur).getDatenaissance());
                preparedStatement.setString(5, ((Joueur) utilisateur).getPhoto());
                preparedStatement.setInt(6, utilisateur.getId());
                preparedStatement.executeUpdate();
                preparedStatement.close();
            }

            if (utilisateur instanceof Parent) {
                preparedStatement = sqlConnection.prepareStatement("UPDATE utilisateur_parent SET utilisateur_id = ? WHERE utilisateur_id = ?");
                preparedStatement.setInt(1, utilisateur.getId());
                preparedStatement.setInt(2, utilisateur.getId());
                preparedStatement.executeUpdate();
                preparedStatement.close();

                preparedStatement = sqlConnection.prepareStatement("DELETE FROM parents WHERE utilisateur_parent = ?");
                preparedStatement.setInt(1, utilisateur.getId());
                preparedStatement.executeUpdate();
                preparedStatement.close();

                for (Joueur joueur : ((Parent) utilisateur).getEnfants()) {
                    preparedStatement = sqlConnection.prepareStatement("INSERT INTO parents (utilisateur_parent,utiilisateur_joueur) VALUES (?,?)");
                    preparedStatement.setInt(1, utilisateur.getId());
                    preparedStatement.setInt(2, joueur.getId());
                    preparedStatement.executeUpdate();
                    preparedStatement.close();
                }
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void deleteUtilisateur(Utilisateur utilisateur) {

        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = DaoManager.getInstance().getConnection();
        try {
            preparedStatement = sqlConnection.prepareStatement("DELETE FROM utilisateurs WHERE id = ?");
            preparedStatement.setInt(1, utilisateur.getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();

            if (utilisateur instanceof Dirigeant) {
                preparedStatement = sqlConnection.prepareStatement("DELETE FROM utilisateur_dirigeant WHERE utilisateur_id = ?");
                preparedStatement.setInt(1, utilisateur.getId());
                preparedStatement.executeUpdate();
                preparedStatement.close();
            }

            if (utilisateur instanceof Entraineur) {
                preparedStatement = sqlConnection.prepareStatement("DELETE FROM utilisateur_entraineur WHERE utilisateur_id = ?");
                preparedStatement.setInt(1, utilisateur.getId());
                preparedStatement.executeUpdate();
                preparedStatement.close();
            }

            if (utilisateur instanceof Joueur) {
                preparedStatement = sqlConnection.prepareStatement("DELETE FROM utilisateur_joueur WHERE utilisateur_id = ?");
                preparedStatement.setInt(1, utilisateur.getId());
                preparedStatement.executeUpdate();
                preparedStatement.close();
            }

            if (utilisateur instanceof Parent) {
                preparedStatement = sqlConnection.prepareStatement("DELETE FROM utilisateur_parent WHERE utilisateur_id = ?");
                preparedStatement.setInt(1, utilisateur.getId());
                preparedStatement.executeUpdate();
                preparedStatement.close();

                preparedStatement = sqlConnection.prepareStatement("DELETE FROM parents WHERE utilisateur_parent = ?");
                preparedStatement.setInt(1, utilisateur.getId());
                preparedStatement.executeUpdate();
                preparedStatement.close();
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public boolean connectUtilisateur(String email, String password) {
        ResultSet resultSet;
        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = DaoManager.getInstance().getConnection();

        try {
            preparedStatement = sqlConnection.prepareStatement("SELECT id FROM utilisateurs WHERE email = ? AND mdp = ?");
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);
            resultSet = preparedStatement.executeQuery();
            if (DaoManager.getInstance().getRowCount(resultSet) > 0 && resultSet.next()) {
                UtilisateursManager.getInstance().setConnectedUtilisateur(UtilisateursManager.getInstance().getUtilisateur(resultSet.getInt(1)));
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
