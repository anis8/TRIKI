package asfoot.dao.types;

import asfoot.categories.CategoriesManager;
import asfoot.dao.DaoManager;
import asfoot.equipes.Equipe;
import asfoot.utilisateurs.UtilisateursManager;
import asfoot.utilisateurs.types.Entraineur;
import asfoot.utilisateurs.types.Joueur;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class EquipesDao {

    public ArrayList<Equipe> getEquipes() {
        ResultSet resultSet;
        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = DaoManager.getInstance().getConnection();
        ArrayList<Equipe> equipes = new ArrayList<>();

        int equipeId;
        ResultSet equipeSet;
        try {
            preparedStatement = sqlConnection.prepareStatement("SELECT * FROM equipes");
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                equipeId = resultSet.getInt(1);
                Equipe equipe = new Equipe(CategoriesManager.getInstance().getCategorie(resultSet.getInt("categorie_id")), resultSet.getString("nom"), resultSet.getString("ecusson"), resultSet.getInt("id"));

                preparedStatement = sqlConnection.prepareStatement("SELECT joueur_id FROM equipe_joueurs WHERE equipe_id = ?");
                preparedStatement.setInt(1, equipeId);
                equipeSet = preparedStatement.executeQuery();
                while (equipeSet.next()) {
                    equipe.addJoueur((Joueur) UtilisateursManager.getInstance().getUtilisateur(equipeSet.getInt(1)));
                }
                preparedStatement = sqlConnection.prepareStatement("SELECT entraineur_id FROM equipe_entraineurs WHERE equipe_id = ?");
                preparedStatement.setInt(1, equipeId);
                equipeSet = preparedStatement.executeQuery();
                while (equipeSet.next()) {
                    equipe.addEntraineur((Entraineur) UtilisateursManager.getInstance().getUtilisateur(equipeSet.getInt(1)));
                }

                equipes.add(equipe);

            }
            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return equipes;
    }


    public void createEquipe(Equipe equipe) {
        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = DaoManager.getInstance().getConnection();
        try {
            preparedStatement = sqlConnection.prepareStatement("INSERT INTO equipes (categorie_id,nom,ecusson) VALUES (?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, equipe.getCategorie().getId());
            preparedStatement.setString(2, equipe.getNom());
            preparedStatement.setString(3, equipe.getEcusson());
            preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                equipe.setId(rs.getInt(1));
            }
            for (Joueur joueur : equipe.getJoueurs()) {
                this.addJoueurEquipe(joueur, equipe);
            }
            for (Entraineur entraineur : equipe.getEntraineurs()) {
                this.addEntraineurEquipe(entraineur, equipe);
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void updateEquipe(Equipe equipe) {
        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = DaoManager.getInstance().getConnection();
        try {
            preparedStatement = sqlConnection.prepareStatement("UPDATE equipes SET categorie_id = ?,nom = ?,ecusson = ? WHERE id = ?");
            preparedStatement.setInt(1, equipe.getCategorie().getId());
            preparedStatement.setString(2, equipe.getNom());
            preparedStatement.setString(3, equipe.getEcusson());
            preparedStatement.setInt(4, equipe.getId());
            preparedStatement.executeUpdate();

            this.clearEquipeMembres(equipe);

            for (Joueur joueur : equipe.getJoueurs()) {
                this.addJoueurEquipe(joueur, equipe);
            }
            for (Entraineur entraineur : equipe.getEntraineurs()) {
                this.addEntraineurEquipe(entraineur, equipe);
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void deleteEquipe(Equipe equipe) {
        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = DaoManager.getInstance().getConnection();
        try {
            preparedStatement = sqlConnection.prepareStatement("DELETE FROM equipes WHERE id = ?");
            preparedStatement.setInt(1, equipe.getId());
            preparedStatement.executeUpdate();

            this.clearEquipeMembres(equipe);

            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private void addJoueurEquipe(Joueur joueur, Equipe equipe) {
        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = DaoManager.getInstance().getConnection();
        try {
            preparedStatement = sqlConnection.prepareStatement("INSERT INTO equipe_joueurs (joueur_id,equipe_id) VALUES (?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, joueur.getId());
            preparedStatement.setInt(2, equipe.getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private void addEntraineurEquipe(Entraineur entraineur, Equipe equipe) {
        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = DaoManager.getInstance().getConnection();
        try {
            preparedStatement = sqlConnection.prepareStatement("INSERT INTO equipe_entraineurs (entraineur_id,equipe_id) VALUES (?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, entraineur.getId());
            preparedStatement.setInt(2, equipe.getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private void clearEquipeMembres(Equipe equipe) {
        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = DaoManager.getInstance().getConnection();
        try {
            preparedStatement = sqlConnection.prepareStatement("DELETE FROM equipe_entraineurs WHERE equipe_id = ?");
            preparedStatement.setInt(1, equipe.getId());
            preparedStatement.executeUpdate();

            preparedStatement = sqlConnection.prepareStatement("DELETE FROM equipe_joueurs WHERE equipe_id = ?");
            preparedStatement.setInt(1, equipe.getId());
            preparedStatement.executeUpdate();

            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
