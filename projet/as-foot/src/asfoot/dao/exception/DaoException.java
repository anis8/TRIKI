package asfoot.dao.exception;

public class DaoException extends Exception {

    public DaoException(String reason, Throwable e) {
        super(reason, e);
    }
}

