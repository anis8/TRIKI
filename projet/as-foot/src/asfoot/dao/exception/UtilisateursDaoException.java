package asfoot.dao.exception;

public class UtilisateursDaoException extends Exception {
    public UtilisateursDaoException(String reason, Throwable e) {
        super(reason, e);
    }
}
