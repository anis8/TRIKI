package asfoot.stades;


import asfoot.dao.DaoManager;

/**
 * Classe Stade
 * <p>
 * Cette classe représente un stade
 *
 * @author Anis
 */
public class Stade {

    private int id;
    private String nom;
    private String ville;
    private int codePostal;


    /**
     * Constructeur
     *
     * @param id         int
     * @param nom        string
     * @param ville      string
     * @param codePostal int
     */
    public Stade(int id, String nom, String ville, int codePostal) {
        this.id = id;
        this.nom = nom;
        this.ville = ville;
        this.codePostal = codePostal;
    }

    /**
     * Retourne l'id du stade
     *
     * @return id
     */

    public int getId() {
        return id;
    }

    /**
     * Met à jour l'id du stade
     *
     * @param id int
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Retourne le nom du stade
     *
     * @return nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * Met à jour le nom du stade
     *
     * @param nom string
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Retourne la ville
     *
     * @return ville
     */
    public String getVille() {
        return ville;
    }

    /**
     * Met à jour la ville
     *
     * @param ville string
     */
    public void setVille(String ville) {
        this.ville = ville;
    }


    /**
     * Retourne le code postal du stade
     *
     * @return codePostal
     */
    public int getCodePostal() {
        return codePostal;
    }

    /**
     * Met à jour le code postal
     *
     * @param codePostal int
     */
    public void setCodePostal(int codePostal) {
        this.codePostal = codePostal;
    }

    /**
     * Permet de mettre à jour le stade dans la base de donnée
     */
    public void save() {
        DaoManager.getInstance().getStadesDao().updateStade(this);
    }
}
