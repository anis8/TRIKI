package asfoot.stades;

import asfoot.dao.DaoManager;

import java.util.ArrayList;

public class StadesManager {

    private static StadesManager stadesManagerInstance;

    private ArrayList<Stade> stades;

    /**
     * Permet d'initialiser la classe
     */
    public void init() {
        this.stades = DaoManager.getInstance().getStadesDao().getStades();
        System.out.print("Gestionnaire de stades prêt.\n");
    }

    /**
     * Permet de récupérer tous les stades
     *
     * @return stades
     */
    public ArrayList<Stade> getStades() {
        return this.stades;
    }

    /**
     * Permet d'ajouter un stade
     *
     * @param stade Stade
     */
    public void addStade(Stade stade) {
        this.stades.add(stade);
    }


    public Stade getStade(int id) {
        for (Stade stade : this.stades) {
            if (stade.getId() == id) {
                return stade;
            }
        }
        return null;
    }

    public Stade getStadeByNom(String nom) {
        for (Stade stade : this.stades) {
            if (stade.getNom().equals(nom)) {
                return stade;
            }
        }
        return null;
    }

    /**
     * Permet de récupérer l'instance en statique
     *
     * @return stadesManagerInstance
     */
    public static StadesManager getInstance() {

        if (stadesManagerInstance == null)
            stadesManagerInstance = new StadesManager();
        return stadesManagerInstance;
    }
}
