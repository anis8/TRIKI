package asfoot.utilisateurs.types;

import asfoot.categories.Categorie;
import asfoot.utilisateurs.Utilisateur;

/**
 * Classe Joueur
 * Cette class représente un joueur
 *
 * @author Anis
 * @see Utilisateur
 */

public class Joueur extends Utilisateur {

    private Categorie categorie;
    private String poste;
    private int numero;
    private String datenaissance;
    private String photo;

    /**
     * Constructeur
     *
     * @param id            int
     * @param email         String
     * @param numeroTel     String
     * @param nom           String
     * @param prenom        String
     * @param categorie     Categorie
     * @param poste         String
     * @param numero        int
     * @param datenaissance int
     * @param photo         String
     */
    public Joueur(int id, String email, String numeroTel, String nom, String prenom, Categorie categorie, String poste, int numero, String datenaissance, String photo) {
        super(id, email, numeroTel, nom, prenom);
        this.categorie = categorie;
        this.poste = poste;
        this.numero = numero;
        this.datenaissance = datenaissance;
        this.photo = photo;
    }

    /**
     * Permet de récupérer la catégorie
     *
     * @return categorie
     */
    public Categorie getCategorie() {
        return categorie;
    }

    /**
     * Permet de mettre à jour la catégorie
     *
     * @param categorie Categorie
     */
    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    /**
     * Permet de récupérer le poste
     *
     * @return poste
     */
    public String getPoste() {
        return poste;
    }

    /**
     * Permet de mettre à jour les postes
     *
     * @param poste String
     */
    public void setPoste(String poste) {
        this.poste = poste;
    }

    /**
     * Permet de récupérer le numéro du joueur
     *
     * @return numero
     */
    public int getNumero() {
        return numero;
    }

    /**
     * Met à jour le numéro
     *
     * @param numero int
     */
    public void setNumero(int numero) {
        this.numero = numero;
    }

    /**
     * Permet de récupérer la date de naissance
     *
     * @return datenaissance
     */
    public String getDatenaissance() {
        return datenaissance;
    }

    /**
     * Permet de mettre à jour la date de naissance
     *
     * @param datenaissance String
     */
    public void setDatenaissance(String datenaissance) {
        this.datenaissance = datenaissance;
    }

    /**
     * Permet de récupérer une photo
     *
     * @return photo
     */
    public String getPhoto() {
        return photo;
    }

    /**
     * Permet de mettre à jour la photo
     *
     * @param photo String
     */
    public void setPhoto(String photo) {
        this.photo = photo;
    }

}
