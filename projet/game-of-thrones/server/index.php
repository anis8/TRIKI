<?php
require 'vendor/autoload.php';
require 'Database.php';

$db = new Database();

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Firebase\JWT\JWT;


$app = new \Slim\App;
$app->get('/bonjour', function () {
    echo "Le serveur répond Bonjour.";
});

$app->get('/personnage/{name}/{token}', function (Request $request, Response $response) {
    global $db;
    $name = $request->getAttribute('name');
    $token = $request->getAttribute('token');
    if (validJWT($token)) {
        $query = $db->connect()->prepare('SELECT id FROM characters WHERE name = ?');
        $query->execute([$name]);
        if ($query->rowCount() < 1) {
            return $response->withStatus(404)
                ->withHeader('Content-Type', 'text/html')
                ->write('Personnage introuvable');
        } else {
            $fetch = $query->fetch();
            $json = json_encode(["id" => $fetch['id']]);

            return $json;
        }
    } else {
        return $response->withStatus(401);
    }
});

$app->get('/user', function (Request $request, Response $response) {
    $get = $request->getQueryParams();
    if (validJWT($get['token'])) {
        global $db;
        $query = $db->connect()->prepare('SELECT email FROM utilisateurs WHERE login = ? AND password = ?');
        $query->execute([$get['login'], $get['password']]);
        if ($query->rowCount() < 1) {
            return $response->withStatus(401)
                ->withHeader('Content-Type', 'text/html')
                ->write('Utilisateurs incorrect.');
        } else {
            $fetch = $query->fetch();
            $json = json_encode(["email" => $fetch['email']]);
            return $json;
        }
    } else {
        return $response->withStatus(401);
    }
});

$app->post('/user', function (Request $request, Response $response) {
    if (validJWT($request->getParsedBody()['token'])) {
        global $db;
        $login = $request->getParsedBody()['login'];
        $email = $request->getParsedBody()['email'];
        $password = $request->getParsedBody()['password'];

        $query = $db->connect()->prepare('INSERT INTO utilisateurs (login,email,password) VALUES (?,?,?)');
        $query->execute([$login, $email, $password]);
        return $response->withStatus(200)
            ->withHeader('Content-Type', 'text/html')
            ->write('L\'utilisateur a bien été créer');
    } else {
        return $response->withStatus(401);
    }
});

$app->delete('/user/{id}/{token}', function (Request $request, Response $response) {
    $token = $request->getAttribute('token');
    if (validJWT($token)) {
        global $db;
        $id = $request->getAttribute('id');
        $query = $db->connect()->prepare('SELECT login FROM utilisateurs WHERE id = ?');
        $query->execute([$id]);
        if ($query->rowCount() < 1) {
            return $response->withStatus(404)
                ->withHeader('Content-Type', 'text/html')
                ->write('L\'utilisateur est introuvable.');
        } else {
            $fetch = $query->fetch();
            $query = $db->connect()->prepare('DELETE FROM utilisateurs WHERE id = ?');
            $query->execute([$id]);
            return $response->withStatus(200)
                ->withHeader('Content-Type', 'text/html')
                ->write('L\'utilisateur ' . $fetch['login'] . ' a bien été supprimé.');
        }
    } else {
        return $response->withStatus(401);
    }

});
$app->put('/user', function (Request $request, Response $response) {
    if (validJWT($request->getParsedBody()['token'])) {
        global $db;
        $id = $request->getParsedBody()['id'];
        $email = $request->getParsedBody()['email'];

        $query = $db->connect()->prepare('SELECT COUNT(*) FROM utilisateurs WHERE id = ?');
        $query->execute([$id]);
        if ($query->fetchColumn() < 1) {
            return $response->withStatus(404)
                ->withHeader('Content-Type', 'text/html')
                ->write('L\'utilisateur est introuvable.');
        } else {
            $query = $db->connect()->prepare('UPDATE utilisateurs SET email = ? WHERE id = ?');
            $query->execute([$email, $id]);
            return $response->withStatus(200)
                ->withHeader('Content-Type', 'text/html')
                ->write('L\'utilisateur a bien été modifié');
        }
    } else {
        return $response->withStatus(401);
    }
});
$app->get('/personnages', function (Request $request, Response $response) {
    if (validJWT($request->getQueryParams()['token'])) {
        global $db;
        $query = $db->connect()->query('SELECT * FROM characters  ORDER BY id DESC LIMIT 100');
        $query->execute();
        return json_encode($query->fetchAll());
    } else {
        return $response->withStatus(401);
    }
});


$app->get('/obtentionToken', function (Request $request, Response $response) {
    $login = $request->getQueryParam('login');
    $pass = $request->getQueryParam('password');
    $allowed = checkUser($login, $pass);
    if ($allowed) {
        $token = getTokenJWT();
        return $response->withJson($token, 200);
    } else {
        return $response->withStatus(401);
    }
});
$app->get('/personnagesStatistiques', function (Request $request, Response $response) {
    if (validJWT($request->getQueryParams()['token'])) {
        global $db;
        $query = $db->connect()->query('SELECT COUNT(*) AS charactersCount,h.name FROM characters c INNER JOIN houses h ON c.house = h.id GROUP BY h.id  HAVING charactersCount > 10');
        $query->execute();
        return json_encode($query->fetchAll());
    } else {
        return $response->withStatus(401);
    }
});

$app->get('/villesStatistiques', function (Request $request, Response $response) {
    if (validJWT($request->getQueryParams()['token'])) {
        global $db;
        $query = $db->connect()->query('SELECT COUNT(*) AS citiesCount,t.name FROM cities c INNER JOIN cities_type t ON c.type = t.id GROUP BY t.id');
        $query->execute();
        return json_encode($query->fetchAll());
    } else {
        return $response->withStatus(401);
    }
});

$app->get('/personnagesTranche', function (Request $request, Response $response) {
    if (validJWT($request->getQueryParams()['token'])) {
        ini_set("display_errors", 0);
        global $db;
        $query = $db->connect()->query('CALL getPerso100()');
        $query->execute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY);
        return json_encode($query->fetchAll());
    } else {
        return $response->withStatus(401);
    }
});

$app->post('/personnage', function (Request $request, Response $response) {
    if (validJWT($request->getParsedBody()['token'])) {
        global $db;
        $name = $request->getParsedBody()['name'];
        $house = $request->getParsedBody()['house'];
        $date = $request->getParsedBody()['date'];

        $houseId = getHouseId($house);
        if ($houseId != null) {
            $query = $db->connect()->prepare('INSERT INTO characters (name,date_of_birth,house) VALUES (?,?,?)');
            $query->execute([$name, $date, $houseId]);
            return $response->withStatus(200)
                ->withHeader('Content-Type', 'text/html')
                ->write('Le personnage a bien été créer');
        } else {
            return $response->withStatus(404)
                ->withHeader('Content-Type', 'text/html')
                ->write('La maison est introuvable');
        }
    } else {
        return $response->withStatus(401);
    }
});

$app->put('/personnage', function (Request $request, Response $response) {
    if (validJWT($request->getParsedBody()['token'])) {
        global $db;
        $name = $request->getParsedBody()['name'];
        $house = $request->getParsedBody()['house'];
        $date = $request->getParsedBody()['date'];
        if (checkPersonnage($name)) {
            $houseId = getHouseId($house);
            if ($houseId != null) {
                $query = $db->connect()->prepare('UPDATE characters SET date_of_birth =  ?, house = ? WHERE name = ?');
                $query->execute([$date, $houseId, $name]);
                return $response->withStatus(200)
                    ->withHeader('Content-Type', 'text/html')
                    ->write('Le personnage a bien été créer');
            } else {
                return $response->withStatus(404)
                    ->withHeader('Content-Type', 'text/html')
                    ->write('La maison est introuvable');
            }
        } else {
            return $response->withStatus(404)
                ->withHeader('Content-Type', 'text/html')
                ->write('Le personnage est introuvable');
        }

    } else {
        return $response->withStatus(401);
    }
});

$app->delete('/personnage', function (Request $request, Response $response) {
    $token = $request->getAttribute('token');
    if (validJWT($request->getParsedBody()['token'])) {
        global $db;
        $name = $request->getParsedBody()['name'];
        if (checkPersonnage($name)) {
            $query = $db->connect()->prepare('DELETE FROM characters WHERE name = ?');
            $query->execute([$name]);
            return $response->withStatus(200)
                ->withHeader('Content-Type', 'text/html')
                ->write('Le personnage ' . $name . ' a bien été supprimé.');
        } else {
            return $response->withStatus(404)
                ->withHeader('Content-Type', 'text/html')
                ->write('Le personnage est introuvable.');
        }
    } else {
        return $response->withStatus(401);
    }

});

$app->get('/personnageculture/{culture}/{token}', function (Request $request, Response $response) {
    global $db;
    $culture = $request->getAttribute('culture');
    $token = $request->getAttribute('token');
    if (validJWT($token)) {
        $query = $db->connect()->prepare('SELECT * FROM characters WHERE culture = ? ORDER BY id DESC LIMIT 100');
        $query->execute([$culture]);
        if ($query->rowCount() < 1) {
            return $response->withStatus(404)
                ->withHeader('Content-Type', 'text/html')
                ->write('Aucun personnage');
        } else {
            return json_encode($query->fetchAll());
        }
    } else {
        return $response->withStatus(401);
    }
});
$app->get('/seigneurs', function (Request $request, Response $response) {
    if (validJWT($request->getQueryParams()['token'])) {
        global $db;
        $query = $db->connect()->query('SELECT COUNT(*) AS count, h.name,c2.name FROM characters c INNER JOIN houses h ON c.house = h.id INNER JOIN characters c2 ON h.current_lord = c2.id GROUP BY c.house ORDER BY count DESC LIMIT 100');
        $query->execute();
        return json_encode($query->fetchAll());
    } else {
        return $response->withStatus(401);
    }
});

$app->get('/maisons', function (Request $request, Response $response) {
    if (validJWT($request->getQueryParams()['token'])) {
        global $db;
        $query = $db->connect()->query('SELECT * FROM houses ORDER BY founded DESC LIMIT 100');
        $query->execute();
        return json_encode($query->fetchAll());
    } else {
        return $response->withStatus(401);
    }
});

function checkUser($login, $password)
{
    global $db;
    $query = $db->connect()->prepare('SELECT COUNT(*) FROM utilisateurs WHERE login = ? AND password = ?');
    $query->execute([$login, $password]);
    if ($query->fetchColumn() > 0) {
        return true;
    } else {
        return false;
    }
}

function checkPersonnage($name)
{
    global $db;
    $query = $db->connect()->prepare('SELECT COUNT(*) FROM characters WHERE name = ?');
    $query->execute([$name]);
    if ($query->fetchColumn() > 0) {
        return true;
    } else {
        return false;
    }
}

function getTokenJWT()
{
    $payload = array(
        //30 min
        "exp" => time() + (24 * 60 * 60)
    );
    return JWT::encode($payload, "secret");
}


function getHouseId($name)
{
    global $db;
    $query = $db->connect()->prepare('SELECT id FROM houses WHERE name = ?');
    $query->execute([$name]);
    if ($query->rowCount() > 0) {
        $fetch = $query->fetch();
        return $fetch['id'];
    } else {
        return null;
    }
}

function validJWT($token)
{
    $res = false;
    try {
        $decoded = JWT::decode($token, "secret", array('HS256'));
    } catch (Exception $e) {
        return $res;
    }
    $res = true;
    return $res;
}

$app->run();