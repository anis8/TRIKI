var Loader;
var Notifications;
var ServerLink;
var myList = [
    {"name": "abc", "age": 50},
    {"age": "25", "hobby": "swimming"},
    {"name": "xyz", "hobby": "programming"}
];
$(document).ready(function () {
    Loader = $("#Loader");
    Notifications = $("#Notification");
    ServerLink = "../server/index.php";

    if (sessionStorage.getItem('token') != null) {
        $("#TokenButton").removeClass("TokenButton").addClass("TokenButtonGenerated").text('Token généré');
        $("#ConnectionForm").hide();
    }


});


var requests = {
    bonjour: function () {
        showLoader();
        $.ajax({
            url: ServerLink + "/bonjour",
            type: 'GET',
            dataType: 'html',
            success: function (r) {
                $("#result1").html(r);
                hideLoader();
            },
            error: function (r, s) {

            }
        });
    },
    personnageDescription: function () {
        showLoader();
        console.log(sessionStorage.getItem('token'));
        $.ajax({
            url: ServerLink + "/personnage/" + $("#personnageDescription").val() + "/" + sessionStorage.getItem('token'),
            type: 'GET',
            dataType: 'html',
            success: function (r) {
                $("#result2").html(r);
                hideLoader();
            },
            error: function (r, s) {
                setNotification("Petit soucis...", "Le personnage est introuvable sans la base de donnée");
                hideLoader();
            }
        });
    },

    login: function () {
        showLoader();
        $.ajax({
            url: ServerLink + "/user?login=" + $("#loginLogin").val() + "&password=" + $("#loginPassword").val() + "&token=" + sessionStorage.getItem('token'),
            type: 'GET',
            dataType: 'html',
            success: function (r) {
                $("#result3").html(r);
                hideLoader();
            },
            error: function (r, s) {
                setNotification("Petit soucis...", "Les identifiants sont incorrects.");
                hideLoader();
            }
        });
    },

    createUser: function () {
        showLoader();
        $.ajax({
            url: ServerLink + "/user",
            type: 'POST',
            data: {
                login: $("#createUserLogin").val(),
                email: $("#createUserEmail").val(),
                password: $("#createUserPassword").val(),
                token: sessionStorage.getItem('token')
            },
            dataType: 'html',
            success: function (r) {
                $("#result4").html(r);
                hideLoader();
            }
        });
    },
    deleteUser: function () {
        showLoader();
        $.ajax({
            url: ServerLink + "/user/" + $("#deleteUserId").val() + "/" + sessionStorage.getItem('token'),
            type: 'DELETE',
            dataType: 'html',
            success: function (r) {
                $("#result5").html(r);
                hideLoader();
            },
            error: function (r, s) {
                setNotification("Petit soucis...", "Impossible de supprimer cet utilisateur");
                hideLoader();
            }
        });
    },
    updateMailUser: function () {
        showLoader();
        $.ajax({
            url: ServerLink + "/user",
            type: 'PUT',
            data: {
                id: $("#updateUserMailId").val(),
                email: $("#updateUserMailEmail").val(),
                token: sessionStorage.getItem('token')
            },
            dataType: 'html',
            success: function (r) {
                $("#result6").html(r);
                hideLoader();
            },
            error: function (r, s) {
                setNotification("Petit soucis...", "Impossible de modifier cet utilisateur");
                hideLoader();
            }
        });
    },
    personnages: function () {
        showLoader();
        $.ajax({
            url: ServerLink + "/personnages?token=" + sessionStorage.getItem('token'),
            type: 'GET',
            dataType: 'html',
            success: function (r) {
                myList = JSON.parse(r);
                buildHtmlTable("#result7");
                hideLoader();
            }
        });
    },
    loginToken: function () {
        showLoader();
        $.ajax({
            type: "GET",
            url: ServerLink + "/obtentionToken?login=" + $("#longinLoginToken").val() + "&password=" + $("#loginPasswordToken").val(),
            success: function (data) {
                $("#ConnectionForm").hide();
                $("#TokenButton").removeClass("TokenButton").addClass("TokenButtonGenerated").text('Token généré');
                sessionStorage.setItem('token', data);
                hideLoader();
            },
            error: function (n) {
                setNotification("Petit soucis...", "Les identifiants sont incorrects.");
                hideLoader();
            }
        });

    },
    personnagesStatistiques: function () {
        showLoader();
        $.ajax({
            url: ServerLink + "/personnagesStatistiques?token=" + sessionStorage.getItem('token'),
            type: 'GET',
            dataType: 'json',
            success: function (r) {
                let labels = [];
                let data = [];
                for (let i = 0; i < r.length; i++) {
                    labels.push(r[i].name);
                    data.push(r[i].charactersCount);
                }

                setChart(labels, data, "chart1")
                //$("#result9").html(r);
                hideLoader();
            }
        });
    },

    villesStatistiques: function () {
        showLoader();
        $.ajax({
            url: ServerLink + "/villesStatistiques?token=" + sessionStorage.getItem('token'),
            type: 'GET',
            dataType: 'json',
            success: function (r) {
                let labels = [];
                let data = [];
                for (let i = 0; i < r.length; i++) {
                    labels.push(r[i].name);
                    data.push(r[i].citiesCount);
                }

                setChart(labels, data, "chart2");
                //$("#result9").html(r);
                hideLoader();
            }
        });
    },
    personnagesTranche: function () {
        showLoader();
        $.ajax({
            url: ServerLink + "/personnagesTranche?token=" + sessionStorage.getItem('token'),
            type: 'GET',
            dataType: 'json',
            success: function (r) {
                let labels = ["-100 et 0", "0 et 100", "100 et 200", "200 et 300"];
                let data = [];
                for (let i = 0; i < r.length; i++) {
                    data.push(r[i].nb);
                }

                setChart(labels, data, "chart3");
                //$("#result9").html(r);
                hideLoader();
            }
        });
    },
    createChararcter: function () {
        showLoader();
        $.ajax({
            url: ServerLink + "/personnage",
            type: 'POST',
            data: {
                name: $("#createCharacterName").val(),
                house: $("#createCharacterHouse").val(),
                date: $("#createCharacterDate").val(),
                token: sessionStorage.getItem('token')
            },
            dataType: 'html',
            success: function (r) {
                $("#result10").html(r);
                hideLoader();
            }, error: function (r) {
                $("#result10").html(r.responseText);
                hideLoader();
            }
        });
    },
    updateChararcter: function () {
        showLoader();
        $.ajax({
            url: ServerLink + "/personnage",
            type: 'PUT',
            data: {
                name: $("#updateCharacterName").val(),
                house: $("#updateCharacterHouse").val(),
                date: $("#updateCharacterDate").val(),
                token: sessionStorage.getItem('token')
            },
            dataType: 'html',
            success: function (r) {
                $("#result11").html(r);
                hideLoader();
            }, error: function (r) {
                $("#result11").html(r.responseText);
                hideLoader();
            }
        });
    },
    deleteChararcter: function () {
        showLoader();
        $.ajax({
            url: ServerLink + "/personnage",
            type: 'DELETE',
            data: {
                name: $("#deleteCharacterName").val(),
                token: sessionStorage.getItem('token')
            },
            dataType: 'html',
            success: function (r) {
                $("#result12").html(r);
                hideLoader();
            }, error: function (r) {
                $("#result12").html(r.responseText);
                hideLoader();
            }
        });
    },
    personnagesCulture: function () {
        showLoader();
        $.ajax({
            url: ServerLink + "/personnageculture/" + $("#CulturePersonnage").val() + "/" + sessionStorage.getItem('token'),
            type: 'GET',
            dataType: 'html',
            success: function (r) {
                myList = JSON.parse(r);
                buildHtmlTable("#result13");
                hideLoader();
            }, error: function (r) {
                setNotification("Petit soucis !", r.responseText);
                hideLoader();
            }
        });
    },
    seigneurs: function () {
        showLoader();
        $.ajax({
            url: ServerLink + "/seigneurs?token=" + sessionStorage.getItem('token'),
            type: 'GET',
            dataType: 'json',
            success: function (r) {
                myList = r;
                let labels = [];
                let data = [];
                for (let i = 0; i < r.length; i++) {
                    data.push(r[i].count);
                    labels.push(r[i].name);
                }
                setChart(labels, data, "chart4");

                buildHtmlTable("#result14");
                hideLoader();
            }, error: function (r) {
                setNotification("Petit soucis !", r.responseText);
                hideLoader();
            }
        });
    }, maisons: function () {
        showLoader();
        $.ajax({
            url: ServerLink + "/maisons?token=" + sessionStorage.getItem('token'),
            type: 'GET',
            dataType: 'json',
            success: function (r) {
                myList = r;

                buildHtmlTable("#result15");
                hideLoader();
            }, error: function (r) {
                setNotification("Petit soucis !", r.responseText);
                hideLoader();
            }
        });
    },
};

function showLoader() {
    Loader.css({visibility: "visible"});
}

function hideLoader() {
    setTimeout(function () {
        Loader.css({visibility: "hidden"});
    }, 500);

}

function setNotification(title, description) {
    Notifications.animate({bottom: "0px"}, 300);

    Notifications.find('.Title').html(title);
    Notifications.find('.Description').html(description);
}

function removeNotification() {
    Notifications.animate({bottom: "-112px"}, 300);
}


function buildHtmlTable(selector) {
    var columns = addAllColumnHeaders(myList, selector);

    for (var i = 0; i < myList.length; i++) {
        var row$ = $('<tr/>');
        for (var colIndex = 0; colIndex < columns.length; colIndex++) {
            var cellValue = myList[i][columns[colIndex]];
            if (cellValue == null) cellValue = "";
            row$.append($('<td/>').html(cellValue));
        }
        $(selector).append(row$);
    }
}

function addAllColumnHeaders(myList, selector) {
    var columnSet = [];
    var headerTr$ = $('<tr/>');

    for (var i = 0; i < myList.length; i++) {
        var rowHash = myList[i];
        for (var key in rowHash) {
            if ($.inArray(key, columnSet) == -1) {
                columnSet.push(key);
                headerTr$.append($('<th/>').html(key));
            }
        }
    }
    $(selector).append(headerTr$);

    return columnSet;
}

function setChart(labels, data, chartId) {
    var ctx = document.getElementById(chartId).getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: labels,
            datasets: [{
                label: 'My First dataset',
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                data: data
            }]
        },
        // Configuration options go here
        options: {}
    });
}





