<!DOCTYPE html>
<html lang="en">
<head>
    <script type="application/javascript" src="api/jquery.js"></script>
    <script type="application/javascript" src="api/GameOfThrones.js"></script>
    <script type="application/javascript" src="api/chartjs.js"></script>
    <link rel="stylesheet" href="assets/style/Interface.css">
    <meta charset="UTF-8">
    <title>Api GameOfThrones</title>
</head>
<body>
<div id="TopMenu">
    <div class="SiteName">game of thrones</div>
    <div id="TokenButton" class="TokenButton FlexPosition ClickableItem NotSelectableItem">
        Générer un Token
    </div>
</div>
<div id="App">
    <div class="GlobalMenu">
        <div id="ConnectionForm">
            <hr>
            <input id="longinLoginToken" class="ClickableItem" placeholder="Nom d'utilisateur"/>
            <input id="loginPasswordToken" type="password" class="ClickableItem" placeholder="Mot de passe"/>
            <div onclick="requests.loginToken()" class="ExecuteButton FlexPosition ClickableItem">
                S'identifier et générer mon token
            </div>
            <div id="result8" onclick="$(this).html('')" class="ResultBox"></div>
            <hr>
        </div>

        <div onclick="requests.bonjour()" class="ExecuteButton FlexPosition ClickableItem">
            Dire bonjour
        </div>
        <div id="result1" onclick="$(this).html('')" class="ResultBox"></div>
        <hr>

        <input id="personnageDescription" class="ClickableItem" placeholder="Nom du personnage (ex: Addam Osgrey)"/>
        <div onclick="requests.personnageDescription()" class="ExecuteButton FlexPosition ClickableItem">
            Récupérer la description
        </div>
        <div id="result2" onclick="$(this).html('')" class="ResultBox"></div>
        <hr>

        <input id="loginLogin" class="ClickableItem" placeholder="Nom d'utilisateur"/>
        <input id="loginPassword" type="password" class="ClickableItem" placeholder="Mot de passe"/>
        <div onclick="requests.login()" class="ExecuteButton FlexPosition ClickableItem">
            Essayer les identifiants
        </div>
        <div id="result3" onclick="$(this).html('')" class="ResultBox"></div>
        <hr>
        <input id="createUserLogin" class="ClickableItem" placeholder="Nom d'utilisateur"/>
        <input id="createUserEmail" class="ClickableItem" placeholder="Email"/>
        <input id="createUserPassword" class="ClickableItem" type="password" placeholder="Mot de passe"/>
        <div onclick="requests.createUser()" class="ExecuteButton FlexPosition ClickableItem">
            Créer l'utilisateur
        </div>
        <div id="result4" onclick="$(this).html('')" class="ResultBox"></div>
        <hr>

        <input id="deleteUserId" class="ClickableItem" placeholder="Identifiant de l'utilisateur"/>
        <div onclick="requests.deleteUser()" class="ExecuteButton FlexPosition ClickableItem">
            Supprimer l'utilisateur
        </div>
        <div id="result5" onclick="$(this).html('')" class="ResultBox"></div>
        <hr>


        <input id="updateUserMailId" class="ClickableItem" placeholder="Identifiant de l'utilisateur"/>
        <input id="updateUserMailEmail" class="ClickableItem" placeholder="Nouveau email"/>
        <div onclick="requests.updateMailUser()" class="ExecuteButton FlexPosition ClickableItem">
            Modifiler l'email de l'utilisateur
        </div>
        <div id="result6" onclick="$(this).html('')" class="ResultBox"></div>
        <hr>

        <div onclick="requests.personnages()" class="ExecuteButton FlexPosition ClickableItem">
            Charger 100 personnages
        </div>
        <table id="result7" onclick="$(this).html('')" class="TableBox" border="1"></table>
        <hr>
        <div class="CaseTitle">Statistiques graphiques</div>
        <hr>
        <div onclick="requests.personnagesStatistiques()" class="ExecuteButton FlexPosition ClickableItem">
            Charger les statistiques des personnages
        </div>
        <div onclick="$(this).html('<canvas id=\'chart1\'></canvas>')" class="ResultBox">
            <canvas id="chart1"></canvas>
        </div>
        <hr>
        <div onclick="requests.villesStatistiques()" class="ExecuteButton FlexPosition ClickableItem">
            Charger les statistiques des villes
        </div>
        <div onclick="$(this).html('<canvas id=\'chart2\'></canvas>')" class="ResultBox">
            <canvas id="chart2"></canvas>
        </div>
        <hr>
        <div onclick="requests.personnagesTranche()" class="ExecuteButton FlexPosition ClickableItem">
            Charger les personnages par tranche
        </div>
        <div onclick="$(this).html('<canvas id=\'chart3\'></canvas>')" class="ResultBox">
            <canvas id="chart3"></canvas>
        </div>

        <hr>
        <input id="createCharacterName" class="ClickableItem" placeholder="Nom du personnage"/>
        <input id="createCharacterHouse" class="ClickableItem" placeholder="Nom de la maison (House Frey)"/>
        <input id="createCharacterDate" class="ClickableItem" placeholder="Date de naissance"/>
        <div onclick="requests.createChararcter()" class="ExecuteButton FlexPosition ClickableItem">
            Créer le personnage
        </div>
        <div id="result10" onclick="$(this).html('')" class="ResultBox"></div>

        <hr>
        <input id="updateCharacterName" class="ClickableItem" placeholder="Nom du personnage"/>
        <input id="updateCharacterHouse" class="ClickableItem" placeholder="Nom de la maison"/>
        <input id="updateCharacterDate" class="ClickableItem" placeholder="Date de naissance"/>
        <div onclick="requests.updateChararcter()" class="ExecuteButton FlexPosition ClickableItem">
            Modifier le personnage
        </div>
        <div id="result11" onclick="$(this).html('')" class="ResultBox"></div>

        <hr>
        <input id="deleteCharacterName" class="ClickableItem" placeholder="Nom du personnage"/>
        <div onclick="requests.deleteChararcter()" class="ExecuteButton FlexPosition ClickableItem">
            Supprimer le personnage
        </div>
        <div id="result12" onclick="$(this).html('')" class="ResultBox"></div>

        <hr>
        <select id="CulturePersonnage">
            <option value="56fa992326c647376404c927">Northmen</option>
            <option value="56fa992426c647376404c928">Ironborn</option>
            <option value="56fa992426c647376404c929">Dornishmen</option>
            <option value="56fa992426c647376404c92a">Orphans of the Greenblood</option>
            <option value="56fa992426c647376404c92b">Vale mountain clans</option>
            <option value="56fa992426c647376404c92c">Northern mountain clans</option>
            <option value="56fa992426c647376404c92d">Crannogmen</option>
            <option value="56fa992426c647376404c92e">Skagosi</option>
            <option value="56fa992426c647376404c92f">Children of the forest</option>
            <option value="56fa992426c647376404c930">First Men</option>
            <option value="56fa992426c647376404c931">Andal</option>
            <option value="56fa992426c647376404c932">Rhoynar</option>
            <option value="56fa992426c647376404c933">Ghiscari</option>
            <option value="56fa992426c647376404c934">Valyrian Freehold</option>
            <option value="56fa992426c647376404c935">Free folk</option>
            <option value="56fa992426c647376404c936">Thenns</option>
            <option value="56fa992426c647376404c937">Giants</option>
            <option value="56fa992426c647376404c938">Others</option>
            <option value="56fa992426c647376404c939">Essos</option>
            <option value="56fa992426c647376404c93a">Free Cities</option>
            <option value="56fa992426c647376404c93b">Dothraki</option>
            <option value="56fa992426c647376404c93c">Lhazareen</option>
            <option value="56fa992426c647376404c93d">Qarth</option>
            <option value="56fa992426c647376404c93e">Yi Ti</option>
        </select>

        <div onclick="requests.personnagesCulture()" class="ExecuteButton FlexPosition ClickableItem">
            Charger 100 personnages de cette culture
        </div>
        <table id="result13" onclick="$(this).html('')" class="TableBox" border="1"></table>
        <hr>

        <div onclick="requests.seigneurs()" class="ExecuteButton FlexPosition ClickableItem">
            Afficher le nombre de personnages par seigneur
        </div>
        <table id="result14" onclick="$(this).html('')" class="TableBox" border="1"></table>
        <div onclick="$(this).html('<canvas id=\'chart4\'></canvas>')" class="ResultBox">
            <canvas id="chart4"></canvas>
        </div>
        <hr>


        <div onclick="requests.maisons()" class="ExecuteButton FlexPosition ClickableItem">
            Afficher les maisons par chronologie
        </div>
        <table id="result15" onclick="$(this).html('')" class="TableBox" border="1"></table>
        <div onclick="$(this).html('<canvas id=\'chart5\'></canvas>')" class="ResultBox">
            <canvas id="chart5"></canvas>
        </div>
        <hr>

    </div>
    <div id="Loader" class="FlexPosition">
        <div class="BigBackground"></div>
        <div class="LittleBackground"></div>
        <div class="Throne"></div>
        <div class="RotatingSpin RotatingItem"></div>
    </div>


    <div onclick="removeNotification()" id="Notification" class="Notification">
        <div class="ErrorImage"></div>
        <div class="Title">dddd
        </div>
        <div class="Description">dddd
        </div>
    </div>
</div>
</body>
</html>