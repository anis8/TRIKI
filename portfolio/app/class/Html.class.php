<?php
class Html
{
    static function meta($page,$title,$description)
    {   
	    echo '
		
		<!-- © Copyright Anis Portfolio 2017. dessiné et développé par Anis Triki. -->
		
		
		';
		echo '<html lang="fr">';
		echo '<head>';
        echo '<base href="' . URL . '" />';
        echo '<meta http-equiv="Content-type" content="text/html; charset=utf-8">';
        echo '<meta name="robots" content="index,follow,all" />';
        echo '<meta name="viewport" content="width=device-width,initial-scale=0.3">';
        echo '<meta name="theme-color" content="#F9D046" />';
        echo '<meta name="keywords" content="anis triki, anis, triki, portfolio, développeur web, web developper, design" />';
        echo '<meta name="Geography" content="France" />';
        echo '<meta name="country" content="France" />';
        echo '<meta name="Language" content="French" />';
        echo '<meta name="identifier-url" content="' . URL . '" />';
        echo '<meta name="Copyright" content="Anis Triki" />';
        echo '<meta name="language" content="fr-FR" />';
        echo '<meta name="hreflang" content="fr-FR" />';
        echo '<meta name="category" content="Website">';
        echo '<meta name="reply-to" content="anis8.dev@gmail.col">';
        echo '<meta property="og:site_name" content="Portfolio - Anis Triki" />';
        echo '<meta property="og:image" content="' . URL . 'app/assets/images/banniere.png" />';
        echo '<meta property="og:locale" content="fr_FR" />';
        echo '<meta property="fb:page_id" content="AUCUN" />';
        echo '<meta name="twitter:site" content="@AUCUB" />';
        echo '<meta name="twitter:card" content="summary" />';
        echo '<meta name="twitter:image:src" content="' . URL . 'app/assets/images/banniere.png" />';
        echo '<meta name="twitter:domain" content="' . URL . '" />';
        echo '<link rel="shortcut icon" href="app/assets/images/icone.png">';
        echo '<link rel="dns-prefetch" href="//'. URL . '" />';
        echo '<link rel="stylesheet" href="app/assets/css/'. POWER .'.css" type="text/css" media="all" />';
		echo '<meta property="og:url" content="' . URL . $page . '" />';
        echo '<meta property="og:type" content="' .$page. '" />';
        echo '<meta property="og:description" content="'.$description.'" />';
        echo '<meta property="og:title" content="'.$title.'" />';
        echo '<meta name="description" content="'.$description.'" />';
        echo '<title>'.$title.'</title>';
        echo '</head>';
		}
    }
?>
