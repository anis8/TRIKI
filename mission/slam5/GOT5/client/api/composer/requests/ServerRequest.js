export class ServerRequest {
    constructor(method, settings) {
        this.settings = settings;
        this.method = method;
    }

    getMethod() {
        return this.method;
    }

    getSettings() {
        return this.settings;
    }

    showResponse(response) {
        console.log(response);
    }
}
