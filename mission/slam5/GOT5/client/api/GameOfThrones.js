import {Interface} from "./interface/Interface.js";
import {Composer} from "./composer/Composer.js";
import {HelloRequest} from "./composer/requests/types/HelloRequest.js";

Interface.init();
Composer.init("localhost", "/", "8080");


Composer.compose(new HelloRequest());