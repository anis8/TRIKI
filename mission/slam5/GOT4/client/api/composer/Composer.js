import {Interface} from "../interface/Interface.js";

export class Composer {
    static init(host, server, port) {
        this.host = host;
        this.server = server;
        this.port = port;
    }

    static compose(request) {
        Interface.loader.render();
        let settings = request.getSettings();
        let method = request.getMethod();

        let url = "http://" + this.host + ":" + this.port + this.server + settings;
        console.log("Compose: " + url);
        let xhr = new XMLHttpRequest();
        xhr.open(method, url);
        xhr.onload = function () {
            if (this.status >= 200 && this.status < 300) {
                request.showResponse(xhr.response);
            } else {
                request.showResponse({
                    status: this.status,
                    statusText: xhr.statusText
                });
            }

            Interface.loader.remove();
        };
        xhr.onerror = function () {
            request.showResponse({
                status: this.status,
                statusText: xhr.statusText
            });
            Interface.loader.remove();
        };
        xhr.send();
    }
}