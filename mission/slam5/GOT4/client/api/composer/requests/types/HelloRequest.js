import {ServerRequest} from "../ServerRequest.js";

export class HelloRequest extends ServerRequest {
    constructor() {
        super("GET", "bonjour");
    }

    showResponse(response) {
        console.log("Hello Request");
        super.showResponse(response);
    }
}