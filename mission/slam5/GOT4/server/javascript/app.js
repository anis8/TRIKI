let express = require('express');
let logger = require('morgan');
let cors = require('cors');
let app = express();

app.use(logger('dev'));
app.use(cors());
app.get('/bonjour', function (req, res, next) {
    res.json({msg: 'Le serveur répond Bonjour.'})
});

app.listen("8080", "localhost", function () {
    console.log("ddd");
});

module.exports = app;
