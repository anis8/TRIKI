<?php
require 'vendor/autoload.php';
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
$app = new \Slim\App;


$app->get('/bonjour', function(Request $request, Response $response){
    return "Le serveur répond Bonjour.";
});
$app->run();