<?php

class Maison
{

    private $nom;
    private $devise;
    private $armoiries;
    private $dateFondation;
    private $region;


    public function __construct($nom, $devise, $armoiries, $dateFondation, $region)
    {
        $this->nom = $nom;
        $this->devise = $devise;
        $this->armoiries = $armoiries;
        $this->dateFondation = $dateFondation;
        $this->region = $region;
    }


    public function setNom($nom)
    {
        $this->nom = $nom;
    }


    public function setDevise($devise)
    {
        $this->devise = $devise;
    }


    public function setArmoiries($armoiries)
    {
        $this->armoiries = $armoiries;
    }


    public function setDateFondation($dateFondation)
    {
        $this->dateFondation = $dateFondation;
    }


    public function setRegion($region)
    {
        $this->region = $region;
    }


    public function getNom()
    {
        return $this->nom;
    }


    public function getDevise()
    {
        return $this->devise;
    }


    public function getArmoiries()
    {
        return $this->armoiries;
    }


    public function getDateFondation()
    {
        return $this->dateFondation;
    }


    public function getRegion()
    {
        return $this->region;
    }

}