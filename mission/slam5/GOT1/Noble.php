<?php

class Noble extends Characters
{
    private $maison;
    private $epoux;
    private $pere;
    private $mere;


    public function __construct($id, $nom, $dateNaissance, $dateMort, $culture, $maison, $epoux, $pere, $mere)
    {
        parent::__construct($id, $nom, $dateNaissance, $dateMort, $culture);
        $this->maison = $maison;
        $this->epoux = $epoux;
        $this->pere = $pere;
        $this->mere = $mere;
    }

    public function setMaison($maison)
    {
        $this->maison = $maison;
    }

    public function setEpoux($epoux)
    {
        $this->epoux = $epoux;
    }

    public function setPere($pere)
    {
        $this->pere = $pere;
    }

    public function setMere($mere)
    {
        $this->mere = $mere;
    }

    public function getMaison()
    {
        return $this->maison;
    }

    public function getEpoux()
    {
        return $this->epoux;
    }

    public function getPere()
    {
        return $this->pere;
    }

    public function getMere()
    {
        return $this->mere;
    }


}