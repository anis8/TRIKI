<?php

abstract class Characters
{
    private $id;
    private $nom;
    private $dateNaissance;
    private $dateMort;
    private $culture;

    public function __construct($id, $nom, $dateNaissance, $dateMort, $culture)
    {
        $this->id = $id;
        $this->nom = $nom;
        $this->dateNaissance = $dateNaissance;
        $this->dateMort = $dateMort;
        $this->culture = $culture;

        global $charactersCounter;
        $charactersCounter++;
    }


    public function setId($id)
    {
        $this->id = $id;
    }


    public function setNom($nom)
    {
        $this->nom = $nom;
    }


    public function setDateNaissance($dateNaissance)
    {
        $this->dateNaissance = $dateNaissance;
    }

    public function setDateMort($dateMort)
    {
        $this->dateMort = $dateMort;
    }

    public function setCulture($culture)
    {
        $this->culture = $culture;
    }


    public function getId()
    {
        return $this->id;
    }


    public function getNom()
    {
        return $this->nom;
    }

    public function getDateNaissance()
    {
        return $this->dateNaissance;
    }


    public function getDateMort()
    {
        return $this->dateMort;
    }

    public function getCulture()
    {
        return $this->culture;
    }

    public function __toString()
    {
        return "Nom: " . $this->nom;
    }

}