<?php

class Hero extends Characters
{
    private $nomActeur;


    public function __construct($id, $nom, $dateNaissance, $dateMort, $culture, $nomActeur)
    {
        parent::__construct($id, $nom, $dateNaissance, $dateMort, $culture);
        $this->nomActeur = $nomActeur;
    }

    public function getNomActeur()
    {
        return $this->nomActeur;
    }

    public function setNomActeur($nomActeur)
    {
        $this->nomActeur = $nomActeur;
    }

}