import body.Body;
import dao.JoueurDao;
import exception.dao.DaoException;
import exception.joueur.JoueurDaoException;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;
import joueur.Joueur;
import listeners.InterfaceListener;

import java.util.ArrayList;

public class Interface extends Application {

    private Stage stage;
    private Scene scene;
    private Group rootGroup;
    private Body body;

    public static void main(String[] args) {
        Application.launch(Interface.class, args);
    }

    @Override
    public void start(Stage primaryStage) {
        this.rootGroup = new Group();
        this.scene = new Scene(this.rootGroup, 800, 600);
        this.body = new Body(primaryStage, this.rootGroup, this.scene);
        this.stage = primaryStage;
        this.initialise();

    }

    private void initialise() {
        this.stage.setScene(this.scene);
        this.stage.show();
        this.initialiseListeners();
        this.draw();
    }

    private void initialiseListeners() {
        new InterfaceListener(this.scene, this.body);
    }

    private void draw() {
        Body.getRootGroup().getChildren().clear();
        this.body.draw();
    }

}



