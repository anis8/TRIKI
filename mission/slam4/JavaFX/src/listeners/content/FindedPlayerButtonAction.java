package listeners.content;

import body.content.FindedPlayer;
import body.content.SearchPlayer;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class FindedPlayerButtonAction implements EventHandler<ActionEvent> {

    private TextField input;
    private FindedPlayer findedPlayer;
    private SearchPlayer searchPlayer;

    public FindedPlayerButtonAction(FindedPlayer searchPlayer) {
        this.findedPlayer = searchPlayer;
        this.searchPlayer = null;
    }

    @Override
    public void handle(ActionEvent event) {
        this.findedPlayer.clear();
        if(this.searchPlayer != null){
            this.searchPlayer.setCanDraw(true);
            this.searchPlayer.draw();
        }
    }
    public void setSearchPlayer(SearchPlayer searchPlayer){
        this.searchPlayer = searchPlayer;
    }
}
