package listeners.content;

import body.content.SearchPlayer;
import dao.JoueurDao;
import exception.dao.DaoException;
import exception.joueur.JoueurDaoException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import joueur.Joueur;

import java.util.ArrayList;

public class SearchPlayerButtonAction implements EventHandler<ActionEvent> {

    private TextField input;
    private SearchPlayer searchPlayer;

    public SearchPlayerButtonAction(TextField input, SearchPlayer searchPlayer) {
        this.input = input;
        this.searchPlayer = searchPlayer;
    }

    @Override
    public void handle(ActionEvent event) {
        if (true) {
            searchPlayer.setHasResult(false);
            searchPlayer.clear();
            try {
                JoueurDao joueurDao = new JoueurDao();
                Joueur jx = joueurDao.getJoueurById( Integer.parseInt(this.input.getText()));
                joueurDao.closeConnection();

                searchPlayer.getFindedPlayer().setData(jx.getPrenom(), jx.getNom(), jx.getPhoto(), jx.getId());
                searchPlayer.getFindedPlayer().draw();

            } catch (DaoException e) {
                System.out.println("Erreur avec la base de donnée" + e);
            } catch (JoueurDaoException e) {
                System.out.println("Erreur avec un joueur");
            }

        } else {
            searchPlayer.setHasResult(true);
            searchPlayer.draw();
        }
    }
}
