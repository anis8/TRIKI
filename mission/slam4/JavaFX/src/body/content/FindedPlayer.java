package body.content;


import body.Body;
import body.IBodyElement;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import listeners.content.FindedPlayerButtonAction;
import listeners.content.SearchPlayerButtonAction;

public class FindedPlayer implements IBodyElement {

    private AnchorPane contentPane;
    private boolean canDraw;

    private String prenom;
    private String nom;
    private int licence;
    private String photo;

    public FindedPlayer() {
        this.canDraw = true;
    }

    public void draw() {
        if (this.canDraw) {
            this.contentPane = new AnchorPane();
            contentPane.setMinWidth(Body.getScene().getWidth());
            contentPane.setMinHeight(Body.getScene().getHeight() - 200);
            contentPane.setLayoutY(70);
            contentPane.setStyle("-fx-background-color: rgb(220,220,220);");

            final Label contentTitle = new Label("Joueur trouvé");
            contentTitle.setStyle("-fx-font-size: 300%");
            contentTitle.setLayoutY(5);
            contentTitle.setLayoutX((Body.getScene().getWidth() / 2));

            final Label contentName = new Label("Nom: " + this.nom);
            contentName.setStyle("-fx-font-size: 200%");
            contentName.setLayoutY(50);
            contentName.setLayoutX((Body.getScene().getWidth() / 2));

            final Label contentPrenom = new Label("Prenom: " + this.prenom);
            contentPrenom.setStyle("-fx-font-size: 200%");
            contentPrenom.setLayoutY(85);
            contentPrenom.setLayoutX((Body.getScene().getWidth() / 2));

            final Label contentLicence = new Label("Licence: " + this.licence);
            contentLicence.setStyle("-fx-font-size: 200%");
            contentLicence.setLayoutY(120);
            contentLicence.setLayoutX((Body.getScene().getWidth() / 2));


            javafx.scene.image.ImageView photo = new ImageView(new Image(this.photo, 360, 360, false, false));
            photo.setX(20);
            photo.setY(20);

            contentPane.getChildren().addAll(contentTitle, photo, contentName, contentPrenom, contentLicence);
            Body.getRootGroup().getChildren().add(contentPane);
        } else {
            this.contentPane.getChildren().clear();
        }
    }

    public void clear() {
        this.contentPane.getChildren().clear();
        this.canDraw = false;
    }

    public void setData(String prenom, String nom, String photo, int licence) {
        this.prenom = prenom;
        this.nom = nom;
        this.licence = licence;
        this.photo = photo;
    }

    public AnchorPane getContentPane() {
        return contentPane;
    }
}
