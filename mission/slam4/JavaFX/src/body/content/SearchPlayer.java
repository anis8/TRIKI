package body.content;


import body.Body;
import body.IBodyElement;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import listeners.content.SearchPlayerButtonAction;

public class SearchPlayer implements IBodyElement {

    private AnchorPane contentPane;
    private boolean canDraw;
    private boolean hasResult;

    private FindedPlayer findedPlayer;

    public SearchPlayer(FindedPlayer findedPlayer) {
        hasResult = false;
        this.canDraw = true;
        this.findedPlayer = findedPlayer;
    }


    public void draw() {
        if (this.canDraw) {
            this.contentPane = new AnchorPane();
            contentPane.setMinWidth(Body.getScene().getWidth());
            contentPane.setMinHeight(Body.getScene().getHeight() - 200);
            contentPane.setLayoutY(70);
            contentPane.setStyle("-fx-background-color: rgb(220,220,220);");

            final Label contentTitle = new Label("Rechercher un joueur");
            contentTitle.setStyle("-fx-font-size: 200%;");
            contentTitle.setLayoutY(((Body.getScene().getHeight() - 200) / 2) - 70);
            contentTitle.setLayoutX((Body.getScene().getWidth() / 2) - 115);

            final TextField input = new TextField();
            input.setMinWidth(400);
            input.setMinHeight(40);
            input.setStyle("-fx-border-style: solid;-fx-font-size:130%;-fx-border-width: 6px;-fx-border-color: white;");
            input.setLayoutY(((Body.getScene().getHeight() - 200) / 2) - 30);
            input.setLayoutX((Body.getScene().getWidth() / 2) - 200);
            input.setPromptText("Rechercher...");

            final Button buttonSearch = new Button();
            buttonSearch.setText("Rechercher");
            buttonSearch.setStyle("-fx-background-color: rgb(125,133,221);-fx-padding: 10px;-fx-font-size: 160%;-fx-text-fill: white;-fx-border-radius: 0px;-fx-background-radius: 0px;");
            buttonSearch.setMinWidth(400);
            buttonSearch.setLayoutY(((Body.getScene().getHeight() - 200) / 2) + 20);
            buttonSearch.setLayoutX((Body.getScene().getWidth() / 2) - 200);
            buttonSearch.setOnAction(new SearchPlayerButtonAction(input, this));


            final Label resultTitle;
            if (hasResult) {
                resultTitle = new Label("Le joueur est introuvable");
                resultTitle.setStyle("-fx-font-size: 200%;-fx-content-display: none;");
                resultTitle.setLayoutY((Body.getScene().getHeight() - 200));
                resultTitle.setLayoutX((Body.getScene().getWidth() / 2) - 115);
                contentPane.getChildren().addAll(contentTitle, input, buttonSearch, resultTitle);
            } else {
                contentPane.getChildren().addAll(contentTitle, input, buttonSearch);
            }
            Body.getRootGroup().getChildren().add(contentPane);
        } else {
            this.contentPane.getChildren().clear();
        }
    }

    public FindedPlayer getFindedPlayer() {
        return findedPlayer;
    }

    public void clear() {
        this.contentPane.getChildren().clear();
        this.canDraw = false;
    }

    public void setCanDraw(boolean canDraw){
        this.canDraw = canDraw;
    }

    public void setHasResult(boolean hasResult){
        this.hasResult = hasResult;
    }
    public AnchorPane getContentPane() {
        return contentPane;
    }
}
