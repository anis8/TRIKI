package body;

import body.content.FindedPlayer;
import body.content.SearchPlayer;
import body.header.Header;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Body implements IBodyElement {

    private static Stage stage;
    private static Group rootGroup;
    private static Scene scene;

    private Header header;
    private SearchPlayer searchPlayer;
    public Body(Stage bStage, Group bRootGroup, Scene bScene) {
        stage = bStage;
        rootGroup = bRootGroup;
        scene = bScene;
        this.initialiseElements();
    }

    public void draw() {
        this.header.draw();
        if (this.searchPlayer != null) {
            this.searchPlayer.draw();
        }
    }

    private void initialiseElements() {
        this.header = new Header();
        FindedPlayer findedPlayer = new FindedPlayer();
        this.searchPlayer = new SearchPlayer(findedPlayer);

    }
    public static Stage getStage() {
        return stage;
    }

    public static Group getRootGroup() {
        return rootGroup;
    }

    public static Scene getScene() {
        return scene;
    }
}
