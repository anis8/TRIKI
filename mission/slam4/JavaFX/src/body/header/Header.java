package body.header;


import body.Body;
import body.IBodyElement;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

public class Header implements IBodyElement {

    public Header() {
    }

    public void draw() {
        AnchorPane headerPane = new AnchorPane();
        headerPane.setMinWidth(Body.getScene().getWidth());
        headerPane.setMinHeight(70);
        headerPane.setMaxHeight(70);
        headerPane.setStyle("-fx-border-width: 0 0 1 0; -fx-border-color: transparent transparent rgb(210,210,210) transparent;");

        ImageView logo = new ImageView(new Image("https://i.imgur.com/fgQlPup.png", 50, 50, false, false));
        logo.setLayoutX(10);
        logo.setLayoutY(10);

        Label logoText = new Label("Base de donnée des joueurs");
        logoText.setStyle("-fx-font-size: 200%;");
        logoText.setLayoutY(15);
        logoText.setLayoutX(75);

        headerPane.getChildren().addAll(logo, logoText);
        Body.getRootGroup().getChildren().add(headerPane);
    }
}
