package body;

public interface IBodyElement {
    public void draw();
}
