package com.anis.foot;

import com.anis.foot.bdd.dao.JoueurDao;
import com.anis.foot.joueur.Joueur;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) throws Exception {

        JoueurDao joueurDao = new JoueurDao();
        Joueur joueur = new Joueur(0, "Nom", "Prenom", "Poste", 50, "Club", "19991025");

        //joueurDao.addJoueur(joueur);
        //joueurDao.deleteJoueur(joueur);

        ArrayList<Joueur> joueurs = joueurDao.getJoueurs();

        for (Joueur j : joueurs) {
            System.out.println(j);
        }

        Joueur jx = joueurDao.getJoueurById(1);
        System.out.println(jx.getPrenom());
    }
}
