package com.anis.foot.bdd.dao;

import com.anis.foot.bdd.Dao;
import com.anis.foot.joueur.Joueur;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Classe Joueur
 *
 * Cette class est pour les insertions/modification SQL d'un joueur
 * @author Anis
 * @version 1.0
 */
public class JoueurDao extends Dao {

    /**
     *
     * @return joueurs
     * @throws Exception
     */
    public ArrayList<Joueur> getJoueurs() throws Exception {

        ResultSet resultSet;
        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = this.getConnection();
        ArrayList<Joueur> joueurs = new ArrayList<>();

        preparedStatement = sqlConnection.prepareStatement("SELECT * FROM joueur");
        resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            joueurs.add(new Joueur(
                    resultSet.getInt(1),
                    resultSet.getString(2),
                    resultSet.getString(3),
                    resultSet.getString(4),
                    resultSet.getInt(5),
                    resultSet.getString(6),
                    resultSet.getString(7)
            ));
        }
        resultSet.close();
        return joueurs;
    }

    /**
     *
     * @param joueur
     * @throws Exception
     */
    public void addJoueur(Joueur joueur) throws Exception {

        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = this.getConnection();
        Date dateNaissance = new SimpleDateFormat("yyyyMMdd").parse(joueur.getDatenaissance());

        preparedStatement = sqlConnection.prepareStatement("INSERT INTO joueur (nom,prenom,poste,numero,club,datenaissance) VALUES (?,?,?,?,?,?)");
        preparedStatement.setString(1, joueur.getNom());
        preparedStatement.setString(2, joueur.getPrenom());
        preparedStatement.setString(3, joueur.getPoste());
        preparedStatement.setInt(4, joueur.getNumero());
        preparedStatement.setString(5, joueur.getClub());
        preparedStatement.setDate(6, new java.sql.Date(dateNaissance.getTime()));
        preparedStatement.executeUpdate();


    }

    /**
     *
     * @param joueur
     * @throws Exception
     */
    public void deleteJoueur(Joueur joueur) throws Exception {
        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = this.getConnection();

        preparedStatement = sqlConnection.prepareStatement("DELETE FROM joueur WHERE id = ?");
        preparedStatement.setInt(1, joueur.getId());
        preparedStatement.executeUpdate();
    }

    /**
     *
     * @param id
     * @return joueur
     * @throws Exception
     */
    public Joueur getJoueurById(int id) throws Exception {
        ResultSet resultSet;
        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = this.getConnection();

        preparedStatement = sqlConnection.prepareStatement("SELECT * FROM joueur WHERE id = ?");
        preparedStatement.setInt(1, id);
        resultSet = preparedStatement.executeQuery();
        Joueur joueur = null;
        if (resultSet.next()) {
            joueur = new Joueur(
                    resultSet.getInt(1),
                    resultSet.getString(2),
                    resultSet.getString(3),
                    resultSet.getString(4),
                    resultSet.getInt(5),
                    resultSet.getString(6),
                    resultSet.getString(7)
            );
        }


        resultSet.close();
        return joueur;
    }


}

