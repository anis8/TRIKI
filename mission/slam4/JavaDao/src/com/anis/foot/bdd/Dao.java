package com.anis.foot.bdd;

import java.sql.*;

/**
 * Classe Dao
 *
 * Cette class se connecte à la base de donnée
 * @author Anis
 * @version 1.0
 */
public class Dao {

    private Connection connection;

    /**
     * Le constructeur
     *
     *
     */


    public Dao() {
        try {
            Class.forName("org.mariadb.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/cours_java_jdbc", "root", "");
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Problème de connexion");
        }
    }

    /**
     *
     * @return connection
     */
    public Connection getConnection() {
        return this.connection;
    }
}
