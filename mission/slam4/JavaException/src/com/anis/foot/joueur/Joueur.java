package com.anis.foot.joueur;


/**
 * Classe Joueur
 *
 * Cette class représente un joueur
 * @author Anis
 * @version 1.0
 */

public class Joueur {

    private int id;
    private String prenom;
    private String nom;
    private String poste;
    private int numero;
    private String club;
    private String datenaissance;

    /**
     * Le constructeur
     *
     * @param id   un entier
     * @param nom  une chaîne de caractère
     * @param prenom une chaîne de caractère
     * @param poste une chaîne de caractère
     * @param numero un entier
     * @param club une chaîne de caractère
     * @param datenaissance une chaîne de caractère
     *
     */


    public Joueur(int id, String nom, String prenom, String poste, int numero, String club, String datenaissance) {
        this.id = id;
        this.prenom = prenom;
        this.nom = nom;
        this.poste = poste;
        this.numero = numero;
        this.club = club;
        this.datenaissance = datenaissance;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return id
     */
    public int getId() {
        return this.id;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     *
     * @return poste
     */
    public String getPoste() {
        return poste;
    }

    /**
     *
      * @param poste
     */
    public void setPoste(String poste) {
        this.poste = poste;
    }

    /**
     *
     * @return numero
     */
    public int getNumero() {
        return numero;
    }

    /**
     *
     * @param numero
     */
    public void setNumero(int numero) {
        this.numero = numero;
    }

    /**
     *
     * @return club
     */
    public String getClub() {
        return club;
    }

    /**
     *
     * @param club
     */
    public void setClub(String club) {
        this.club = club;
    }

    /**
     *
     * @return datenaissance
     */
    public String getDatenaissance() {
        return datenaissance;
    }

    /**
     *
     * @param datenaissance
     */
    public void setDatenaissance(String datenaissance) {
        this.datenaissance = datenaissance;
    }

    /**
     *
     * @return prenom
     */

    public String getPrenom() {
        return this.prenom;
    }

    /**
     *
     * @return nom
     */
    public String getNom() {
        return this.nom;
    }

    /**
     *
     * @param prenom
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    /**
     *
     * @return toString
     */
    public String toString() {
        return "ID:" + this.id + " Prenom:" + this.prenom + " Nom:" + this.nom + " Club:" + this.club + " Poste:" + this.poste + " Numero:" + this.numero;
    }
}
