package com.anis.foot.dao;

import com.anis.foot.exception.dao.DaoException;
import com.anis.foot.exception.joueur.JoueurDaoException;
import com.anis.foot.joueur.Joueur;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Classe Joueur
 * <p>
 * Cette class est pour les insertions/modification SQL d'un joueur
 *
 * @author Anis
 * @version 1.0
 */
public class JoueurDao extends Dao {

    public JoueurDao() throws DaoException {
    }

    /**
     * @return joueurs
     * @throws Exception
     */
    public ArrayList<Joueur> getJoueurs() throws JoueurDaoException {
        ResultSet resultSet;
        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = this.getConnection();
        ArrayList<Joueur> joueurs = new ArrayList<>();
        try {
            preparedStatement = sqlConnection.prepareStatement("SELECT * FROM joueur");
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                joueurs.add(new Joueur(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getInt(5),
                        resultSet.getString(6),
                        resultSet.getString(7)
                ));
            }
            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            throw new JoueurDaoException("Problème de requête SQL", e);
        }
        return joueurs;
    }

    /**
     * @param joueur
     * @throws Exception
     */
    public void addJoueur(Joueur joueur) throws JoueurDaoException {

        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = this.getConnection();

        try {
            Date dateNaissance = new SimpleDateFormat("yyyyMMdd").parse(joueur.getDatenaissance());
            preparedStatement = sqlConnection.prepareStatement("INSERT INTO joueur (nom,prenom,poste,numero,club,datenaissance) VALUES (?,?,?,?,?,?)");
            preparedStatement.setString(1, joueur.getNom());
            preparedStatement.setString(2, joueur.getPrenom());
            preparedStatement.setString(3, joueur.getPoste());
            preparedStatement.setInt(4, joueur.getNumero());
            preparedStatement.setString(5, joueur.getClub());
            preparedStatement.setDate(6, new java.sql.Date(dateNaissance.getTime()));
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            throw new JoueurDaoException("Problème requete SQL", e);
        } catch (ParseException e) {
            throw new JoueurDaoException("Problème parse", e);
        }


    }

    /**
     * @param joueur
     * @throws Exception
     */
    public void deleteJoueur(Joueur joueur) throws JoueurDaoException {
        try {
            PreparedStatement preparedStatement;
            Connection sqlConnection;
            sqlConnection = this.getConnection();

            preparedStatement = sqlConnection.prepareStatement("DELETE FROM joueur WHERE id = ?");
            preparedStatement.setInt(1, joueur.getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            throw new JoueurDaoException("Problème avec supression d'un joueur", e);
        }

    }

    /**
     * @param id
     * @return joueur
     * @throws Exception
     */
    public Joueur getJoueurById(int id) throws JoueurDaoException {
        ResultSet resultSet;
        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = this.getConnection();
        Joueur joueur = null;
        try {
            preparedStatement = sqlConnection.prepareStatement("SELECT * FROM joueur WHERE id = ?");
            preparedStatement.setInt(1, id);
            resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                joueur = new Joueur(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getInt(5),
                        resultSet.getString(6),
                        resultSet.getString(7)
                );
            }

            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            throw new JoueurDaoException("Problème requete SQL", e);
        }

        return joueur;
    }


}

