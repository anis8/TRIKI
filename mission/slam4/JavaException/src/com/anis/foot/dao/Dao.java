package com.anis.foot.dao;

import com.anis.foot.exception.dao.DaoException;

import java.sql.*;

/**
 * Classe Dao
 * Cette class se connecte à la base de donnée
 *
 * @author Anis
 * @version 1.0
 */
public class Dao {

    private Connection connection;

    /**
     * Le constructeur
     */


    public Dao() throws DaoException {
        try {
            Class.forName("org.mariadb.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/cours_java_jdbc", "root", "");
        } catch (ClassNotFoundException e) {
            throw new DaoException("Problème de connexion car driver introuvable", e);
        } catch (SQLException e) {
            throw new DaoException("Problème de connexion à la base de donnée", e);
        }
    }

    /**
     * @return connection
     */
    public Connection getConnection() {
        return this.connection;
    }

    public void closeConnection() throws DaoException {
        try {
            this.connection.close();
        } catch (SQLException e) {
            throw new DaoException("Impossible de fermer la connexion", e);
        }

    }
}
