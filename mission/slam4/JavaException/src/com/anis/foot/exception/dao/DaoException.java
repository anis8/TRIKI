package com.anis.foot.exception.dao;


public class DaoException extends Exception {

    public DaoException(String reason, Throwable e) {
        super(reason, e);
    }
}
