package com.anis.foot.exception.joueur;

public class JoueurDaoException extends Exception {
    public JoueurDaoException(String reason, Throwable e){
        super(reason,e);
    }
}
