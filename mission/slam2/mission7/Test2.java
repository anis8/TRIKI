import dao.ProfesseurDao;
import modele.Professeur;

import java.util.ArrayList;
import java.util.Scanner;

public class Test2 {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        ProfesseurDao pDao = new ProfesseurDao();
        while(true) {
            System.out.println("Que voulez-vous faire ? ------------------------------------");
            System.out.println("1- Ajout d'un nouveau professeur");
            System.out.println("2- Suppression d'un utilisateur selon sont identifiant");
            System.out.println("3- Recherche d'un professeur selon la spécialité");
            System.out.println("4- Recherche d'un professeur selon sont identifiant");

            switch (sc.nextInt()) {
                case 1:
                    System.out.println("Saisir nom");
                    String nom = sc.next();
                    System.out.println("Saisir  spécialité");
                    String specialite = sc.next();
                    System.out.println("Saisir identifiant");
                    int identifiant = sc.nextInt();
                    pDao.insertProfesseur(new Professeur(identifiant,nom,specialite));
                    break;
                case 2:
                    System.out.println("Saisir identifiant");
                    int id = sc.nextInt();
                    Professeur prof = pDao.getProfesseurById(id);
                    if(prof != null){
                        pDao.deleteProfesseur(prof);
                    }
                    break;

                case 3:
                    System.out.println("Saisir la spécialité");
                    String spec = sc.next();
                    ArrayList<Professeur> liste = pDao.getProfesseursBySpecialite(spec);
                    for(Professeur p : liste){
                        System.out.println(p);
                    }
                    break;
                case 4:
                    System.out.println("Saisir identifiant");
                    int iden = sc.nextInt();
                    System.out.println(pDao.getProfesseurById(iden));
                    break;
            }
        }



    }
}
