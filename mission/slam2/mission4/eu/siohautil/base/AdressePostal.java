package eu.siohautil.base;

/**
 * Classe AdressePostal
 *
 * Cette classe permet de créer une adresse postal associé à un voyageur ou une agence de voyage.
 * @see Voyageur,AgenceVoyage
 * @author Anis
 * @version 1.0
 */
public class AdressePostal {

    private String voie;
    private String ville;
    private String postal;

    /**
     * Le constructeur
     *
     * @param voie   une chaîne de caractère
     * @param ville  une chaîne de caractère
     * @param postal une chaîne de caractère
     */
    public AdressePostal(String voie, String ville, String postal) {
        this.voie = voie;
        this.ville = ville;
        this.postal = postal;
    }

    /**
     * Afficher l'objet
     * @return l'objet en chaîne de caractère
     */

    public String toString(){
        return "Adresse: " + this.voie + ", " + this.ville + " " + this.postal;
    }

    /**
     * Mettre la voie
     *
     * @param voie
     */
    public void setVoie(String voie) {
        this.voie = voie;
    }

    /**
     * Mette la ville
     *
     * @param ville
     */
    public void setVille(String ville) {
        this.ville = ville;
    }

    /**
     * Mettre le code postal
     *
     * @param postal
     */
    public void setPostal(String postal) {
        this.postal = postal;
    }

    /**
     * Récuperer la voie
     *
     * @return voie
     */
    public String getVoie() {
        return voie;
    }

    /**
     * Récuperer la ville
     *
     * @return ville
     */
    public String getVille() {
        return ville;
    }

    /**
     * Récuperer le code postal
     *
     * @return postal
     */
    public String getPostal() {
        return postal;
    }


}
