package fr.slam.mission5.animal;

import fr.slam.mission5.navigable.Navigable;

public class Canard extends Animal implements Navigable {

    private int nbPlumes;

    public Canard(String surnom, int age, String type){
        super(surnom,age,type);
    }

    public String toString(){
        return "Surnom: " + getSurnom() + " age: " + getAge() + " type: " + getType();
    }


    public void accoster(){

    }

    public void naviguer(){

    }

    public void couler(){

    }
}
