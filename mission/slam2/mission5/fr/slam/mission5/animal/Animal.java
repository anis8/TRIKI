package fr.slam.mission5.animal;

public class Animal {

    private String surnom;
    private int age;
    private String type;

    Animal(String surnom, int age, String type){
        this.surnom = surnom;
        this.age = age;
        this.type = type;
    }

    public String toString(){
        return "Surnom: " + getSurnom() + " age: " + getAge() + " type: " + getType();
    }

    public void setSurnom(String surnom){

    }

    public String getSurnom() {
        return surnom;
    }

    public void setAge(int age){

    }

    public int getAge(){
        return age;
    }

    public void setType(String type){

    }

    public String getType(){
        return type;
    }

}
