package fr.slam.mission5.vehicule;

import java.util.ArrayList;

public class VehiculeManager {

    private static ArrayList<Vehicule> vehicules;

    public static void initialise(){
        vehicules = new ArrayList<>();
        vehicules.add(new Voiture("Mercedes","blanc","V1"));
        vehicules.add(new Voiture("Mercedes","blanc","V2"));
        vehicules.add(new Voiture("Mercedes","blanc","V3"));
    }

    public static ArrayList<Vehicule> getVehicules(){
        return vehicules;
    }
}
