package fr.slam.mission5;

import fr.slam.mission5.animal.Animal;
import fr.slam.mission5.animal.AnimalManager;
import fr.slam.mission5.animal.Chien;
import fr.slam.mission5.navigable.Navigable;
import fr.slam.mission5.navigable.NavigableManager;
import fr.slam.mission5.vehicule.Vehicule;
import fr.slam.mission5.vehicule.VehiculeManager;
import fr.slam.mission5.vehicule.Voiture;

import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        VehiculeManager.initialise();
        AnimalManager.initialise();
        NavigableManager.initialise();


        for (Vehicule v : VehiculeManager.getVehicules()) {
            v.demarrer();
            System.out.println(v.getModele() + " vien de démarré");
        }
        for (Vehicule v : VehiculeManager.getVehicules()) {
            if (v instanceof Voiture) {
                ((Voiture) v).stationner();
                System.out.println(v.getModele() + " vien d'être stationné");
            }
        }

        for (Animal a : AnimalManager.getAnimaux()){
            System.out.println(a);
        }

        for (Animal a : AnimalManager.getAnimaux()){
            if (a instanceof Chien) {
                ((Chien) a).aboyer();
                System.out.println(a.getSurnom() + " vien d'abboyé");
            }
        }
        System.out.println("Les navigables: ---------");
        for (Navigable n : NavigableManager.getNavigable()){
            n.naviguer();
            System.out.println(n);

        }
    }


}
