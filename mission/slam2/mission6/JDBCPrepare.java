import java.sql.*;
import java.util.Scanner;

public class JDBCPrepare {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        Connection connection;
        Class.forName("org.mariadb.jdbc.Driver");
        connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/javamission6", "root", "");
        while (true) {
            System.out.println("---------------------------------------------------------------------------------------");
            System.out.println("1- Ajout d'un professeur");
            System.out.println("2- Supression d'un professeur");
            System.out.println("3- Recherche de professeur selon une spécialité");
            System.out.println("4- Recherche d'un professeur par nom");
            System.out.println("Veuillez choisir qu'elle action effectué ?");
            int choix = sc.nextInt();
            PreparedStatement pstmt;
            ResultSet result;
            String nom;
            switch (choix) {
                case 1:
                    System.out.println("Veuillez saisir le nom du professeur");
                    nom = sc.next();
                    System.out.println("Veuillez saisir la matière du professeur");
                    String specialite = sc.next();
                    String req3 = "INSERT INTO professeur (nom,specialite) VALUES (?,?)";
                    pstmt = connection.prepareStatement(req3);
                    pstmt.setString(1, nom);
                    pstmt.setString(2, specialite);
                    pstmt.executeUpdate();
                    break;
                case 2:
                    System.out.println("Veuillez saisir l'id du professeur a supprimé");
                    int id = sc.nextInt();
                    pstmt = connection.prepareStatement("DELETE FROM professeur WHERE id = ?");
                    pstmt.setInt(1, id);
                    pstmt.executeUpdate();
                    break;
                case 3:
                    System.out.println("Veuillez saisir la spécialité");
                    String recherche = sc.next();
                    pstmt = connection.prepareStatement("SELECT * FROM professeur WHERE specialite = ?");
                    pstmt.setString(1, recherche);
                    result = pstmt.executeQuery();

                    while (result.next()) {
                        System.out.print("Nom : " + result.getString(2));
                        System.out.print("Spécialité : " + result.getString(3));
                        System.out.println("\n");
                    }
                    result.close();
                    break;
                case 4:
                    System.out.println("Veuillez saisir son nom");
                    nom = sc.next();
                    pstmt = connection.prepareStatement("SELECT * FROM professeur WHERE nom = ?");
                    pstmt.setString(1, nom);
                    result = pstmt.executeQuery();

                    while (result.next()) {
                        System.out.println("Nom : " + result.getString(2));
                        System.out.println("Spécialité : " + result.getString(3));
                        System.out.println("\n");
                    }
                    result.close();
                    break;

                default:
                    break;
            }
        }
    }
}
