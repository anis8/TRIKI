1-SELECT * FROM members ORDER BY members_character_name
2-SELECT m.members_character_level FROM members m INNER JOIN races r ON m.members_character_race = r.races_id WHERE races_side = 2
3-SELECT count(m.members_character_id) as nbMonture,m.members_character_id FROM members m INNER JOIN mounts_members mou ON mou.name = m.members_character_id GROUP BY m.members_character_id HAVING nbMonture > 5
4-SELECT count(m.members_character_id) as nbMonture,m.members_character_id FROM members m INNER JOIN mounts_members mou ON mou.name = m.members_character_id GROUP BY m.members_character_id ORDER BY nbMonture DESC LIMIT 1
5-SELECT m.mounts_name FROM mounts m WHERE m.mounts_creature_id NOT IN (SELECT mm.mounts_collected_creature_id FROM mounts_members mm)
6)1-SELECT COUNT(*) as nbPet FROM pets_members p  INNER JOIN members m ON m.members_character_id = p.id_members GROUP BY p.id_members ORDER BY nbPet DESC LIMIT 1
6)2-SELECT COUNT(*) as nbPet FROM pets_members p  INNER JOIN members m ON m.members_character_id = p.id_members GROUP BY p.id_members ORDER BY nbPet DESC
6)3-SELECT COUNT(*) as nbPet,pt.pet_types_name FROM pets_members p INNER JOIN members m ON m.members_character_id = p.id_members INNER JOIN pets pe ON pe.pets_id = p.id_pet INNER JOIN pet_types pt ON pt.pet_types_id = pe.pets_type_id GROUP BY pt.pet_types_id,p.id_members ORDER BY nbPet DESC