1/DELIMITER //
CREATE TRIGGER rein_lev BEFORE UPDATE ON members 
FOR EACH row
BEGIN
IF NEW.members_character_name <> OLD.members_character_name then
SET NEW.members_character_level=1;
END IF;
END;
//
UPDATE members set members_character_name ="test" where members_character_id=1;


2/DELIMITER // CREATE TRIGGER niv_lev BEFORE INSERT ON mounts_members FOR EACH row BEGIN UPDATE members SET members.members_character_level= members.members_character_level+1 WHERE NEW.name=members.members_character_id; END; //
INSERT INTO mounts_members VALUES(-80357,311); 

3/DELIMITER // CREATE TRIGGER attribution BEFORE INSERT ON members FOR EACH ROW  BEGIN IF NEW.members_character_class=6 THEN SET NEW.members_character_level=70; END IF; END; //
INSERT INTO members VALUES (511,10,"Invoque la puissance de la Lumière pour protéger et soigner.","Reactorslock",13780,"HEALING","Hyjal","Hyjal",5,"Misery",0,"Sacré",1,6,"spell_holy_holybolt","bg-paladin-holy",20,"Kitten Cannon","hyjal/120/114312056-avatar.jpg");


4/DELIMITER //
CREATE TRIGGER interdisup BEFORE DELETE ON members
FOR EACH row
BEGIN
IF old.members_character_level  >= 100 THEN 
DELETE FROM rien;
END IF;
END;
//
DELETE FROM members where members_character_level = 110

5/DELIMITER //
CREATE TRIGGER interdit_choisir AFTER INSERT ON members
FOR EACH ROW
BEGIN
IF (NEW.members_character_race = 1 OR  NEW.members_character_race = 3
OR NEW.members_character_race = 4 OR NEW.members_character_race = 7 OR
 NEW.members_character_race = 11 OR NEW.members_character_race = 22 OR
NEW.members_character_race = 25 OR  NEW.members_character_race = 29 OR
NEW.members_character_race = 30) 
AND NEW.members_character_class = 11
THEN
INSERT INTO vide VALUES (600, 1, ' ', ' ', 13780, ' ', ' ', ' ', 1, '
', 1, ' ', 1, 1, ' ', ' ', 1, ' ', ' ');
END IF;
IF  (NEW.members_character_race = 2 OR  NEW.members_character_race = 5
OR NEW.members_character_race = 6 OR NEW.members_character_race = 8 OR
 NEW.members_character_race = 9 OR NEW.members_character_race = 10 OR
NEW.members_character_race = 26 OR  NEW.members_character_race = 27 OR
NEW.members_character_race = 28) 
AND NEW.members_character_class  = 2
THEN
INSERT INTO vide VALUES (600, 1, ' ', ' ', 13780, ' ', ' ', ' ', 1, '
', 1, ' ', 1, 1, ' ', ' ', 1, ' ', ' ');
END IF;
END;
//
INSERT INTO members VALUES (512,22,'Invoque la puissance de la Lumière pour protéger et soigner.','Bàllôu',13780,'HEALING','Hyjal','Hyjal',5,'Misery',0,'Sacré',1,11,'spell_holy_holybolt','bg-paladin-holy',0,'Kitten Cannon','hyjal/120/114312056-avatar.jpg');

6/DELIMITER //
CREATE TRIGGER offrir_monture BEFORE INSERT ON members
FOR EACH ROW
BEGIN
DECLARE montureR INT ;
SELECT mounts_creature_id INTO montureR FROM mounts ORDER BY rand() LIMIT 1 ;
INSERT INTO mounts_members VALUES (montureR , NEW.members_character_id);
END;
//
INSERT INTO members VALUES (5555,24,'Invoque la puissance de la Lumière pour protéger et soigner.','Bàllôu',13780,'HEALING','Hyjal','Hyjal',5,'Misery',0,'Sacré',1,10,'spell_holy_holybolt','bg-paladin-holy',0,'Kitten Cannon','hyjal/120/114312056-avatar.jpg');