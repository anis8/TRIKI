/*
Algo: Mission4
variable: age
          spectacle chaine de charractère
		  prix      double
		  categorie charractère
		  reduction double
		  achat     entier
		  save      double
Debut:
  Saisir achat
  Tant que achat = 1
  alors:
      save <- prix
      Saisir age
	  Tant que age < 0
	  alors:
	      Saisir age
	  Fin Tant que
	  Saisir spectacle
	  Tant que spectacle != "Opéra" ET spectacle != "Méchant" ET  spectacle != "Spéctacle" ET  spectacle != "LeGrand"
	  Saisir spectacle
	  Fin Tant que
	  
	  
	  Selon spectacle
	       Cas: "Opéra"
		   prix <- 60
		   Pause
		   Cas: "Méchant"
		   prix <- 40
		   Pause
		   Cas: "Spéctacle"
		   prix <- 25
		   Pause
		   Cas: "LeGrand"
		   prix <- 55
		   Pause
		   Défaut:
		   prix <- 50
		   Pause
	   Fin Selon


	   Si age < 3
	      alors:
		  categorie <- n
	   Sinon si age >= 3 ET age < 13
	      alors:
		  categorie <- e
	   Sinon si age > 12 ET age < 18
	      alors:
		  categorie <- a
	   Sinon si age >= 18 ET age < 56
	      alors:
		  categorie <- d
	   Sinon
	      categorie <- s
	   Fin Si


	   Si categorie = 'e'
	      alors:
		  reduction <- 0.3
	   Sinon Si categorie = 'n'
	      alors:
		  reduction <- 5 / prix
	   Sinon Si categorie = 's'
	      alors:
		  reduction <- 0.5
	   Fin Si

	   prix <- prix * reduction
	   save <- save + prix
	   Afficher "Voulez-vous continuer"
  Fin tant que
FIN
	    
*/
import java.util.Scanner;

public class Mission3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Voulez-vous arrêter l'achat immediatement?");
        //1= On continue, le reste = on arrete.
        int achat = sc.nextInt();
        double prix =0;
        double save = 0;
        while (achat == 1) {
        save = prix;

            int age;
            System.out.println("Veuillez saisir votre age:");
            age = sc.nextInt();
            while (age < 0) {
                System.out.println("Veuillez saisir un age positif:");
                age = sc.nextInt();
            }


            System.out.println("Veuillez saisir le nom du spectacle:");
            String spectacle = sc.next();
            while (!spectacle.equals("Opéra") && !spectacle.equals("Méchant") && !spectacle.equals("Spéctacle") && !spectacle.equals("LeGrand")) {
                System.out.println("Veuillez saisir un nom de spectacle valide:");
                spectacle = sc.next();
            }
            switch (spectacle) {
                case "Opéra":
                    prix = 60;
                    break;
                case "Méchant":
                    prix = 40;
                    break;
                case "Spéctacle":
                    prix = 25;
                    break;
                case "LeGrand":
                    prix = 55;
                    break;
                default:
                    prix = 0;
                    break;
            }
            char categorie;
            if (age < 3) {
                categorie = 'n'; // nourisson
            } else if (age >= 3 && age < 13) {
                categorie = 'e'; // enfant
            } else if (age > 12 && age < 18) {
                categorie = 'a'; // ado
            } else if (age >= 18 && age < 56) {
                categorie = 'd'; // adulte
            } else {
                categorie = 's'; // senior
            }
            double reduction;
            if (categorie == 'e') {
                reduction = 0.3;
            } else if (categorie == 'n') {
                reduction = 5 / prix;
            /* En multipliant par 5/prix, le resultat peu importe le prix sera de 5 exemple:
               5 / 50 = 0.1
               50 x 0.1 = 5
            */
            } else if (categorie == 's') {
                reduction = 0.5;
            } else {
                reduction = 1;
            }
            prix = prix * reduction;
            save = save + prix;
            System.out.println("Voulez-vous continuer les achats? Le coût actuel est de " + save + " euros");
            achat = sc.nextInt();


        }
        if(achat != 1){
            System.out.println("Le prix est de: " + save);
        }
    }
}