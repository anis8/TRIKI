1- COMMANDE(id,datecommande,numclient) pk:id
   Clef étrangère: numclient en référance à CLIENT numclient
   
   ARTICLE(ref,nom,prix) pk:ref
   CLIENT(numclient,nomclient,prenomclient,adresseclient,tel) pk:numclient
   
   LIGCDE(ref,boncommande,qantite)pk:red,boncommande
   Clef étrangère: ref en référance à ARTICLE ref
   Clef étrangère: boncommande en référance à COMMANDE boncommande
2-
CREATE TABLE commande(
    boncommande int PRIMARY KEY,
    datecommande date,
    numclient int );
	
CREATE TABLE article(
    ref varchar(15) PRIMARY KEY,
    nom varchar(100),
    prix double );
    
CREATE TABLE client(
    numclient int PRIMARY KEY,
    nomclient varchar(100),
    prenomclient varchar(100),
    adresseclient varchar(225),
    tel varchar(25));
CREATE TABLE ligcde (
    ref varchar(15),
    boncommande int,
    quantite int );
	
ALTER TABLE ligcde ADD PRIMARY KEY (ref,boncommande);

ALTER TABLE commande ADD INDEX (numclient);
ALTER TABLE commande ADD CONSTRAINT pournumclient
FOREIGN KEY (numclient) REFERENCES client (numclient);


ALTER TABLE ligcde ADD INDEX (ref,boncommande);
ALTER TABLE ligcde ADD CONSTRAINT pourlareference
FOREIGN KEY (ref) REFERENCES article (ref);

ALTER TABLE ligcde ADD CONSTRAINT pourlidclient
FOREIGN KEY (boncommande) REFERENCES commande (boncommande);

3-
INSERT INTO client VALUES
(35165,"FOUQUET","Reine-Marie","3 rue de la Foret 34790 Marillu","0450505050"),
(34067,"JACQUIN","Marie-Jeanne","3 rue du puits 95800 Baillon","0135353535"),
(35166,"NEYMAR","Dasilva Santos","15 Rue des champs 75000","0148484848"),
(35169,"TRIKI","Anis","8 Avenue du monde 95400","0195959595");

INSERT INTO article VALUES
("FO561","Poele Fontenel",18),
("GP651","Grille pain jaune",25),
("PQ345","Casserole Mistral",12),
("PQ346","Casserole Fastral",15),
("PQ347","Casserole Nostral",20),
("PQ348","Casserole Bistral",25),
("PQ349","Casserole Quastral",30);


INSERT INTO commande VALUES
("200504638","2005-06-15",35165),
("200504637","2005-09-15",34067),
("200504639","2005-06-14",35166),
("200504640","2005-09-21",35169);

INSERT INTO ligcde VALUES
("FO561","200504638",2),
("GP651","200504638",1),
("PQ345","200504637",2),
("FO561","200504637",1),
("PQ348","200504639",3),
("PQ349","200504640",8);



4-ALTER TABLE client ADD numero_portable varchar(25);
5-ALTER TABLE commande ADD date_livraison date;
6-SELECT * FROM client;
7-SELECT numero_portable FROM client; 
8-SELECT * FROM client 
  INNER JOIN commande ON commande.numclient = client.numclient
  INNER JOIN ligcde ON commande.boncommande = ligcde.boncommande WHERE ligcde.ref = "FO561";
9-UPDATE client SET adresseclient = "12 rue de la forêt" WHERE numclient = "35165";
10-ALTER TABLE client ADD ville varchar(255);
11-SELECT * FROM client ORDER BY ville;
12-SELECT SUM(article.prix*ligcde.quantite) FROM article 
   INNER JOIN ligcde ON article.ref = ligcde.ref
   INNER JOIN commande ON ligcde.boncommande = commande.boncommande 
13-CREATE OR REPLACE VIEW commandechere AS SELECT SUM(article.prix*ligcde.quantite) as total FROM article 
   INNER JOIN ligcde ON article.ref = ligcde.ref
   INNER JOIN commande ON ligcde.boncommande = commande.boncommande
   SELECT MAX(total) FROM commandechere;
14-UPDATE SET MAX(total) =  MAX(total) * 0.9 FROM commandechere;
15-ALTER TABLE commande ADD ( fraitdeport varchar(25) );
   UPDATE SET commande.fraitdeport = 3 FROM commandechere WHERE MAX(total) <= 100;
16-CREATE USER 'client1'@'localhost' IDENTIFIED BY 'pass';
   CREATE USER 'commercial1'@'localhost' IDENTIFIED BY 'pass';
18-GRANT SELECT ON article TO client1;
   GRANT INSERT ON commande TO commercial1;
19-// Je ne vois pas comment faire.
20-CREATE ROLE commercial;
   CREATE ROLE client;
21-GRANT commercial1 TO commercial;
   GRANT client1 TO client;
22-GRANT INSERT TO commercial,client;
23-CREATE USER 'anis'@'localhost' IDENTIFIED BY 'pass';
   GRANT ALL PRIVILEGES ON *.* TO 'anis'@'localhost'
24-DROP USER 'client1'@'localhost';