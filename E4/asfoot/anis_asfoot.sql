-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: mysql-anis.alwaysdata.net
-- Generation Time: May 18, 2019 at 01:13 PM
-- Server version: 10.2.17-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `anis_asfoot`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `nom`) VALUES
(5, 'U-19'),
(6, 'U-21'),
(7, 'U-17'),
(8, 'U-15'),
(9, 'U-13'),
(10, 'U-11');

-- --------------------------------------------------------

--
-- Table structure for table `equipes`
--

CREATE TABLE `equipes` (
  `id` int(11) NOT NULL,
  `categorie_id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `ecusson` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `equipes`
--

INSERT INTO `equipes` (`id`, `categorie_id`, `nom`, `ecusson`) VALUES
(15, 6, 'Cergy U21', '-'),
(16, 5, 'Cergy U19', '-');

-- --------------------------------------------------------

--
-- Table structure for table `equipe_entraineurs`
--

CREATE TABLE `equipe_entraineurs` (
  `entraineur_id` int(11) NOT NULL,
  `equipe_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `equipe_entraineurs`
--

INSERT INTO `equipe_entraineurs` (`entraineur_id`, `equipe_id`) VALUES
(44, 15),
(45, 16);

-- --------------------------------------------------------

--
-- Table structure for table `equipe_joueurs`
--

CREATE TABLE `equipe_joueurs` (
  `joueur_id` int(11) NOT NULL,
  `equipe_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `equipe_joueurs`
--

INSERT INTO `equipe_joueurs` (`joueur_id`, `equipe_id`) VALUES
(27, 15),
(28, 15),
(29, 15),
(30, 16),
(31, 16),
(32, 16);

-- --------------------------------------------------------

--
-- Table structure for table `matchs`
--

CREATE TABLE `matchs` (
  `id` int(11) NOT NULL,
  `stade_id` int(11) NOT NULL,
  `equipe_une_id` int(11) NOT NULL,
  `equipe_deux_id` int(11) NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `matchs`
--

INSERT INTO `matchs` (`id`, `stade_id`, `equipe_une_id`, `equipe_deux_id`, `date`) VALUES
(4, 10, 15, 16, '17/05/2019'),
(5, 10, 16, 15, '22/05/2019');

-- --------------------------------------------------------

--
-- Table structure for table `match_termine`
--

CREATE TABLE `match_termine` (
  `match_id` int(11) NOT NULL,
  `equipe_un_buts` int(11) NOT NULL,
  `equipe_deux_buts` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `match_termine`
--

INSERT INTO `match_termine` (`match_id`, `equipe_un_buts`, `equipe_deux_buts`) VALUES
(4, 4, 2),
(5, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `parents`
--

CREATE TABLE `parents` (
  `id` int(11) NOT NULL,
  `utilisateur_parent` int(11) NOT NULL,
  `utiilisateur_joueur` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stades`
--

CREATE TABLE `stades` (
  `id` int(11) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `ville` varchar(255) NOT NULL,
  `code_postal` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stades`
--

INSERT INTO `stades` (`id`, `nom`, `ville`, `code_postal`) VALUES
(10, 'Salif Keita', 'Cergy', 95800),
(11, 'Jouy-le-Moutier Stadium', 'Jouy-le-Moutier', 95280),
(12, 'Osny Stadium', 'Osny', 95520);

-- --------------------------------------------------------

--
-- Table structure for table `utilisateurs`
--

CREATE TABLE `utilisateurs` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mdp` varchar(255) NOT NULL,
  `tel` varchar(255) NOT NULL,
  `nom` varchar(255) NOT NULL,
  `prenom` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `utilisateurs`
--

INSERT INTO `utilisateurs` (`id`, `email`, `mdp`, `tel`, `nom`, `prenom`) VALUES
(26, 'd', 'd', '0677983444', 'Triki', 'Anis'),
(27, 'j', 'j', '0677983458', 'Mbappe', 'Kylian'),
(28, 'j', 'j', '0677983458', 'Neymar', 'Dasilva'),
(29, 'j', 'j', '0677983458', 'Lionel', 'Messi'),
(30, 'j', 'j', '0677983458', 'Ronaldo', 'Cristiano'),
(31, 'j', 'j', '0677983458', 'Hazard', 'Eden'),
(32, 'j', 'j', '0677983458', 'Pogba', 'Paul'),
(44, 'e', 'e', '0244569852', 'Enrique', 'Luis'),
(45, 'e3', 'e3', '0654123658', 'Sissoko', 'Moussa');

-- --------------------------------------------------------

--
-- Table structure for table `utilisateur_dirigeant`
--

CREATE TABLE `utilisateur_dirigeant` (
  `utilisateur_id` int(11) NOT NULL,
  `fonction` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `utilisateur_dirigeant`
--

INSERT INTO `utilisateur_dirigeant` (`utilisateur_id`, `fonction`) VALUES
(26, 'Président');

-- --------------------------------------------------------

--
-- Table structure for table `utilisateur_entraineur`
--

CREATE TABLE `utilisateur_entraineur` (
  `utilisateur_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `utilisateur_entraineur`
--

INSERT INTO `utilisateur_entraineur` (`utilisateur_id`) VALUES
(44),
(45);

-- --------------------------------------------------------

--
-- Table structure for table `utilisateur_joueur`
--

CREATE TABLE `utilisateur_joueur` (
  `utilisateur_id` int(11) NOT NULL,
  `categorie_id` int(11) NOT NULL,
  `poste` varchar(255) NOT NULL,
  `numero` int(11) NOT NULL,
  `datenaissance` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `utilisateur_joueur`
--

INSERT INTO `utilisateur_joueur` (`utilisateur_id`, `categorie_id`, `poste`, `numero`, `datenaissance`, `photo`) VALUES
(27, 5, 'AD', 11, '', 'Aucun'),
(28, 10, 'AG', 10, '', ''),
(29, 9, 'MO', 10, '', ''),
(30, 8, '', 0, '', ''),
(31, 5, '', 0, '', ''),
(32, 8, '', 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `utilisateur_parent`
--

CREATE TABLE `utilisateur_parent` (
  `utilisateur_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `equipes`
--
ALTER TABLE `equipes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categorie_id` (`categorie_id`);

--
-- Indexes for table `equipe_entraineurs`
--
ALTER TABLE `equipe_entraineurs`
  ADD PRIMARY KEY (`entraineur_id`),
  ADD KEY `equipe_equipe` (`equipe_id`);

--
-- Indexes for table `equipe_joueurs`
--
ALTER TABLE `equipe_joueurs`
  ADD PRIMARY KEY (`joueur_id`),
  ADD KEY `vers_equipe` (`equipe_id`);

--
-- Indexes for table `matchs`
--
ALTER TABLE `matchs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `match_stade` (`stade_id`),
  ADD KEY `match_equipe_un` (`equipe_une_id`),
  ADD KEY `match_equipe_deux` (`equipe_deux_id`);

--
-- Indexes for table `match_termine`
--
ALTER TABLE `match_termine`
  ADD PRIMARY KEY (`match_id`);

--
-- Indexes for table `parents`
--
ALTER TABLE `parents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_joueur` (`utiilisateur_joueur`),
  ADD KEY `parent_parent` (`utilisateur_parent`);

--
-- Indexes for table `stades`
--
ALTER TABLE `stades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `utilisateur_dirigeant`
--
ALTER TABLE `utilisateur_dirigeant`
  ADD PRIMARY KEY (`utilisateur_id`);

--
-- Indexes for table `utilisateur_entraineur`
--
ALTER TABLE `utilisateur_entraineur`
  ADD PRIMARY KEY (`utilisateur_id`);

--
-- Indexes for table `utilisateur_joueur`
--
ALTER TABLE `utilisateur_joueur`
  ADD PRIMARY KEY (`utilisateur_id`),
  ADD KEY `joueur_categorie` (`categorie_id`);

--
-- Indexes for table `utilisateur_parent`
--
ALTER TABLE `utilisateur_parent`
  ADD PRIMARY KEY (`utilisateur_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `equipes`
--
ALTER TABLE `equipes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `matchs`
--
ALTER TABLE `matchs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `parents`
--
ALTER TABLE `parents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stades`
--
ALTER TABLE `stades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `equipes`
--
ALTER TABLE `equipes`
  ADD CONSTRAINT `equipe_categorie` FOREIGN KEY (`categorie_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `equipe_entraineurs`
--
ALTER TABLE `equipe_entraineurs`
  ADD CONSTRAINT `equipe_entraineur` FOREIGN KEY (`entraineur_id`) REFERENCES `utilisateur_entraineur` (`utilisateur_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `equipe_equipe` FOREIGN KEY (`equipe_id`) REFERENCES `equipes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `equipe_joueurs`
--
ALTER TABLE `equipe_joueurs`
  ADD CONSTRAINT `vers_equipe` FOREIGN KEY (`equipe_id`) REFERENCES `equipes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `vers_joueurs` FOREIGN KEY (`joueur_id`) REFERENCES `utilisateur_joueur` (`utilisateur_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `matchs`
--
ALTER TABLE `matchs`
  ADD CONSTRAINT `match_equipe_deux` FOREIGN KEY (`equipe_deux_id`) REFERENCES `equipes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `match_equipe_un` FOREIGN KEY (`equipe_une_id`) REFERENCES `equipes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `match_stade` FOREIGN KEY (`stade_id`) REFERENCES `stades` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `match_termine`
--
ALTER TABLE `match_termine`
  ADD CONSTRAINT `matche_termine` FOREIGN KEY (`match_id`) REFERENCES `matchs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `parents`
--
ALTER TABLE `parents`
  ADD CONSTRAINT `parent_joueur` FOREIGN KEY (`utiilisateur_joueur`) REFERENCES `utilisateur_joueur` (`utilisateur_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `parent_parent` FOREIGN KEY (`utilisateur_parent`) REFERENCES `utilisateur_parent` (`utilisateur_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `utilisateur_dirigeant`
--
ALTER TABLE `utilisateur_dirigeant`
  ADD CONSTRAINT `utilisateur_dirigeant` FOREIGN KEY (`utilisateur_id`) REFERENCES `utilisateurs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `utilisateur_entraineur`
--
ALTER TABLE `utilisateur_entraineur`
  ADD CONSTRAINT `utilisateur_entraineur` FOREIGN KEY (`utilisateur_id`) REFERENCES `utilisateurs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `utilisateur_joueur`
--
ALTER TABLE `utilisateur_joueur`
  ADD CONSTRAINT `joueur_categorie` FOREIGN KEY (`categorie_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `utilisateur_joueur` FOREIGN KEY (`utilisateur_id`) REFERENCES `utilisateurs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `utilisateur_parent`
--
ALTER TABLE `utilisateur_parent`
  ADD CONSTRAINT `utilisateur_parent` FOREIGN KEY (`utilisateur_id`) REFERENCES `utilisateurs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
