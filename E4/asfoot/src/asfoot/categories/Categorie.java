package asfoot.categories;

/**
 * Classe Categorie
 * La classe catégorie représente une catégorie
 *
 * @author Anis
 */
public class Categorie {

    private int id;
    private String nom;

    /**
     * Permet d'instancier la classe
     *
     * @param id  int
     * @param nom string
     */
    public Categorie(int id, String nom) {
        this.id = id;
        this.nom = nom;
    }

    public Categorie(int id) {
        this.id = id;
        this.nom = null;
    }

    /**
     * Retourne l'id de la catégorie
     *
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * Met à jour l'id de la catégorie
     *
     * @param id int
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Retourne le nom de la catégorie
     *
     * @return nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * Met à jour le nom de la catégorie
     *
     * @param nom string
     */
    public void setNom(String nom) {
        this.nom = nom;
    }
}
