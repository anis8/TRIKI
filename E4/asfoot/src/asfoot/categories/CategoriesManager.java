package asfoot.categories;

import asfoot.dao.DaoManager;
import asfoot.utilisateurs.UtilisateursManager;

import java.util.ArrayList;

/**
 * Classe CategoriesManager
 * <p>
 * Cette permet d'assurer la gestion des catégories.
 *
 * @author Anis
 */
public class CategoriesManager {

    private static CategoriesManager categoriesManagerInstance;
    private ArrayList<Categorie> categories;

    /**
     * Il permet d'instencier la classe
     */
    public void init() {
        this.categories = DaoManager.getInstance().getCategoriesDao().getCategories();
        System.out.print("Gestionnaire de catégories prêt. \n");
    }

    /**
     * Permet de récupérer toutes les catégories
     *
     * @return categories
     */
    public ArrayList<Categorie> getCategories() {
        return this.categories;
    }

    /**
     * Permet d'ajouter une catégorie
     *
     * @param categorie Categorie
     */
    public void addCategorie(Categorie categorie) {
        this.categories.add(categorie);
    }

    /**
     * Récupérer une catégorie par son id
     * @param id int
     * @return Categorie
     */
    public Categorie getCategorie(int id) {
        for (Categorie categorie : this.categories) {
            if (categorie.getId() == id) {
                return categorie;
            }
        }
        return null;
    }

    /**
     * Permet de récupérer une catégorie par son nom
     * @param nom String
     * @return Categorie
     */
    public Categorie getCategorieByNom(String nom){
        for(Categorie categorie : this.categories){
            if(categorie.getNom().equals(nom)){
                return categorie;
            }
        }
        return null;
    }

    /**
     * Permet de récupérer l'instance de la classe
     *
     * @return categoriesManagerInstance
     */
    public static CategoriesManager getInstance() {
        if (categoriesManagerInstance == null)
            categoriesManagerInstance = new CategoriesManager();
        return categoriesManagerInstance;
    }


}
