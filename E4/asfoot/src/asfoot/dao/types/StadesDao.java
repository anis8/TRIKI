package asfoot.dao.types;

import asfoot.dao.DaoManager;
import asfoot.stades.Stade;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class StadesDao {

    public ArrayList<Stade> getStades() {
        ResultSet resultSet;
        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = DaoManager.getInstance().getConnection();
        ArrayList<Stade> stades = new ArrayList<>();
        try {
            preparedStatement = sqlConnection.prepareStatement("SELECT * FROM stades");
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                stades.add(new Stade(
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getInt(4)
                ));
            }
            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return stades;
    }

    public void createStade(Stade stade) {
        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = DaoManager.getInstance().getConnection();
        try {
            preparedStatement = sqlConnection.prepareStatement("INSERT INTO stades (nom,ville,code_postal) VALUES (?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, stade.getNom());
            preparedStatement.setString(2, stade.getVille());
            preparedStatement.setInt(3, stade.getCodePostal());
            preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                stade.setId(rs.getInt(1));
            }

            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void updateStade(Stade stade) {
        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = DaoManager.getInstance().getConnection();
        try {
            preparedStatement = sqlConnection.prepareStatement("UPDATE stades SET nom = ?,ville = ?,code_postal = ? WHERE id = ?");
            preparedStatement.setString(1, stade.getNom());
            preparedStatement.setString(2, stade.getVille());
            preparedStatement.setInt(3, stade.getCodePostal());
            preparedStatement.setInt(4, stade.getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void deleteStade(Stade stade) {
        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = DaoManager.getInstance().getConnection();
        try {
            preparedStatement = sqlConnection.prepareStatement("DELETE FROM stades WHERE id = ?");
            preparedStatement.setInt(1, stade.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
