package asfoot.dao.types;

import asfoot.dao.DaoManager;
import asfoot.equipes.EquipesManager;
import asfoot.matchs.Match;
import asfoot.matchs.types.MatchTermine;
import asfoot.stades.StadesManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MatchsDao {

    public ArrayList<Match> getMatchs() {
        ResultSet matchSet;
        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = DaoManager.getInstance().getConnection();
        ArrayList<Match> matchs = new ArrayList<>();
        int matchId;
        ResultSet resultSet;
        try {
            preparedStatement = sqlConnection.prepareStatement("SELECT * FROM matchs");
            matchSet = preparedStatement.executeQuery();
            while (matchSet.next()) {
                matchId = matchSet.getInt(1);
                preparedStatement = sqlConnection.prepareStatement("SELECT * FROM match_termine WHERE match_id = ?");
                preparedStatement.setInt(1, matchId);
                resultSet = preparedStatement.executeQuery();
                if (DaoManager.getInstance().getRowCount(resultSet) > 0 && resultSet.next()) {
                    matchs.add(new MatchTermine(
                            StadesManager.getInstance().getStade(matchSet.getInt("stade_id")),
                            EquipesManager.getInstance().getEquipe(matchSet.getInt("equipe_une_id")),
                            EquipesManager.getInstance().getEquipe(matchSet.getInt("equipe_deux_id")),
                            matchId,
                            matchSet.getString("date"),
                            resultSet.getInt("equipe_un_buts"),
                            resultSet.getInt("equipe_deux_buts")
                    ));
                } else {
                    matchs.add(new Match(
                            StadesManager.getInstance().getStade(matchSet.getInt("stade_id")),
                            EquipesManager.getInstance().getEquipe(matchSet.getInt("equipe_une_id")),
                            EquipesManager.getInstance().getEquipe(matchSet.getInt("equipe_deux_id")),
                            matchId,
                            matchSet.getString("date")
                    ));
                }
            }
            matchSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return matchs;
    }

    public void createMatch(Match match) {
        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = DaoManager.getInstance().getConnection();
        try {
            preparedStatement = sqlConnection.prepareStatement("INSERT INTO matchs (stade_id,equipe_une_id,equipe_deux_id,date) VALUES (?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, match.getStade().getId());
            preparedStatement.setInt(2, match.getEquipeUne().getId());
            preparedStatement.setInt(3, match.getEquipeDeux().getId());
            preparedStatement.setString(4, match.getDate());
            preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                match.setId(rs.getInt(1));
            }

            if (match instanceof MatchTermine) {
                preparedStatement = sqlConnection.prepareStatement("INSERT INTO match_termine (match_id,equipe_un_buts,equipe_deux_buts) VALUES (?,?,?)");
                preparedStatement.setInt(1, rs.getInt(1));
                preparedStatement.setInt(2, ((MatchTermine) match).getEquipeUneButs());
                preparedStatement.setInt(3, ((MatchTermine) match).getEquipeDeuxButs());
                preparedStatement.executeUpdate();
                preparedStatement.close();
            }

            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateMatch(Match match) {
        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = DaoManager.getInstance().getConnection();
        try {
            preparedStatement = sqlConnection.prepareStatement("UPDATE matchs SET stade_id = ?,equipe_une_id = ?,equipe_deux_id = ?, date = ? WHERE id = ?");
            preparedStatement.setInt(1, match.getStade().getId());
            preparedStatement.setInt(2, match.getEquipeUne().getId());
            preparedStatement.setInt(3, match.getEquipeDeux().getId());
            preparedStatement.setString(4, match.getDate());
            preparedStatement.setInt(5, match.getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();

            if (match instanceof MatchTermine) {

                preparedStatement = sqlConnection.prepareStatement("SELECT * FROM match_termine WHERE match_id = ?");
                preparedStatement.setInt(1, match.getId());
                ResultSet resultSet = preparedStatement.executeQuery();
                if (DaoManager.getInstance().getRowCount(resultSet) < 1) {
                    preparedStatement = sqlConnection.prepareStatement("INSERT INTO match_termine (match_id,equipe_un_buts,equipe_deux_buts) VALUES (?,?,?)");
                    preparedStatement.setInt(1, match.getId());
                    preparedStatement.setInt(2, ((MatchTermine) match).getEquipeUneButs());
                    preparedStatement.setInt(3, ((MatchTermine) match).getEquipeDeuxButs());
                    preparedStatement.executeUpdate();
                    preparedStatement.close();
                } else {
                    preparedStatement = sqlConnection.prepareStatement("UPDATE match_termine SET equipe_un_buts = ?,equipe_deux_buts = ? WHERE match_id = ?");
                    preparedStatement.setInt(1, ((MatchTermine) match).getEquipeUneButs());
                    preparedStatement.setInt(2, ((MatchTermine) match).getEquipeDeuxButs());
                    preparedStatement.setInt(3, match.getId());
                    preparedStatement.executeUpdate();
                    preparedStatement.close();
                }
            } else {
                preparedStatement = sqlConnection.prepareStatement("SELECT * FROM match_termine WHERE match_id = ?");
                preparedStatement.setInt(1, match.getId());
                ResultSet resultSet = preparedStatement.executeQuery();
                if (DaoManager.getInstance().getRowCount(resultSet) > 0 && resultSet.next()) {
                    preparedStatement = sqlConnection.prepareStatement("DELETE FROM match_termine WHERE match_id = ?");
                    preparedStatement.setInt(1, match.getId());
                    preparedStatement.executeUpdate();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteMatch(Match match) {
        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = DaoManager.getInstance().getConnection();
        try {
            preparedStatement = sqlConnection.prepareStatement("DELETE FROM matchs WHERE id = ?");
            preparedStatement.setInt(1, match.getId());
            preparedStatement.executeUpdate();

            if (match instanceof MatchTermine) {
                preparedStatement = sqlConnection.prepareStatement("DELETE FROM match_termine WHERE match_id = ?");
                preparedStatement.setInt(1, match.getId());
                preparedStatement.executeUpdate();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
