package asfoot.dao.types;

import asfoot.categories.Categorie;
import asfoot.dao.DaoManager;
import asfoot.stades.Stade;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CategoriesDao {

    public ArrayList<Categorie> getCategories() {
        ResultSet resultSet;
        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = DaoManager.getInstance().getConnection();
        ArrayList<Categorie> categories = new ArrayList<>();
        try {
            preparedStatement = sqlConnection.prepareStatement("SELECT * FROM categories");
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                categories.add(new Categorie(
                        resultSet.getInt(1),
                        resultSet.getString(2)
                ));
            }
            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return categories;
    }

    public void createCategorie(Categorie categorie) {
        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = DaoManager.getInstance().getConnection();
        try {
            preparedStatement = sqlConnection.prepareStatement("INSERT INTO categories (nom) VALUES (?)", PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, categorie.getNom());
            preparedStatement.executeUpdate();
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()) {
                categorie.setId(rs.getInt(1));
            }

            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateCategorie(Categorie categorie) {
        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = DaoManager.getInstance().getConnection();
        try {
            preparedStatement = sqlConnection.prepareStatement("UPDATE categories SET nom = ? WHERE id = ?");
            preparedStatement.setString(1, categorie.getNom());
            preparedStatement.setInt(2, categorie.getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteCategorie(Categorie categorie) {
        PreparedStatement preparedStatement;
        Connection sqlConnection;
        sqlConnection = DaoManager.getInstance().getConnection();
        try {
            preparedStatement = sqlConnection.prepareStatement("DELETE FROM categories WHERE id = ?");
            preparedStatement.setInt(1, categorie.getId());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
