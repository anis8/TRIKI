package asfoot.utilisateurs;

/**
 * Classe Utilisateur
 * Cette classe représente l'objet d'un Utilisateur
 *
 * @author Anis
 */
public class Utilisateur {

    private int id;
    private String email;
    private String numeroTel;
    private String nom;
    private String prenom;
    private String mdp;

    /**
     * Constructeur
     *
     * @param id        int
     * @param email     string
     * @param numeroTel string
     * @param nom       string
     * @param prenom    string
     */
    public Utilisateur(int id, String email, String numeroTel, String nom, String prenom) {
        this.id = id;
        this.email = email;
        this.numeroTel = numeroTel;
        this.nom = nom;
        this.prenom = prenom;
        this.mdp = null;
    }


    /**
     * Constructeur
     *
     * @param id        int
     * @param email     string
     * @param numeroTel string
     * @param nom       string
     * @param prenom    string
     * @param mdp       String
     */

    public Utilisateur(int id, String email, String numeroTel, String nom, String prenom, String mdp) {
        this.id = id;
        this.email = email;
        this.numeroTel = numeroTel;
        this.nom = nom;
        this.prenom = prenom;
        this.mdp = mdp;
    }

    /**
     * Retourne l'id de l'utilisateur
     *
     * @return id
     */
    public int getId() {
        return id;
    }


    /**
     * Met à jour l'id de l'utilisateur
     *
     * @param id int
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Retourne l'email de l'utilisateur
     *
     * @return email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Met à jour l'email de l'utilisateur
     *
     * @param email string
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Le numéro de téléphone de l'utilisateur
     *
     * @return numeroTel
     */
    public String getNumeroTel() {
        return numeroTel;
    }

    /**
     * Met à jour le numéro de téléphone de l'utilisateur
     *
     * @param numeroTel string
     */
    public void setNumeroTel(String numeroTel) {
        this.numeroTel = numeroTel;
    }

    /**
     * Retourne le nom de l'utilisateur
     *
     * @return nom
     */
    public String getNom() {
        return nom;
    }

    /**
     * Met à jour le nom de l'utilisateur
     *
     * @param nom string
     */

    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * Retourne le prénom de l'utilisateur
     *
     * @return prenom
     */
    public String getPrenom() {
        return prenom;
    }

    /**
     * Met à jour le prénom de l'utilisateur
     *
     * @param prenom string
     */
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }


    /**
     * Mettre à jour le mot de passe
     *
     * @param mdp String
     */
    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    /**
     * Récupérer le mot de passe
     *
     * @return mdp
     */
    public String getMdp() {
        return this.mdp;
    }

}

