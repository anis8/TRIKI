package asfoot.utilisateurs;

import asfoot.dao.DaoManager;
import asfoot.utilisateurs.types.Dirigeant;
import asfoot.utilisateurs.types.Entraineur;
import asfoot.utilisateurs.types.Joueur;
import asfoot.utilisateurs.types.Parent;

import java.util.ArrayList;

/**
 * Classe UtilisateursManager
 * Cette classe permet d'assurer la gestion des utilisateurs
 *
 * @author Anis
 */
public class UtilisateursManager {

    private static UtilisateursManager utilisateursManagerInstance;
    private ArrayList<Utilisateur> utilisateurs;
    private Utilisateur connectedUtilisateur;

    /**
     * Permet d'instancier la classe
     */
    public void init() {
        this.utilisateurs = DaoManager.getInstance().getUtilisateursDao().getUtilisateurs();
        this.connectedUtilisateur = null;
        System.out.print("Gestionnaire d'utilisateurs prêt. \n");
    }


    /**
     * Permet de récupérer l'utilisateur par l'ID
     * @param id int
     * @return Utilisateur
     */
    public Utilisateur getUtilisateur(int id) {
        for (Utilisateur utilisateur : this.utilisateurs) {
            if (utilisateur.getId() == id) {
                return utilisateur;
            }
        }
        return null;
    }

    /**
     * Permet de récupérer tous les utilisateurs
     *
     * @return utilisateurs
     */
    public ArrayList<Utilisateur> getUtilisateurs() {
        return this.utilisateurs;
    }

    /**
     * Permet d'ajouter un utilisateur
     *
     * @param utilisateur string
     */
    public void addUtilisateur(Utilisateur utilisateur) {
        this.utilisateurs.add(utilisateur);
    }

    /**
     * Permet de récupérer l'instance de manière statique
     *
     * @return utilisateursManagerInstance
     */
    public static UtilisateursManager getInstance() {
        if (utilisateursManagerInstance == null)
            utilisateursManagerInstance = new UtilisateursManager();
        return utilisateursManagerInstance;
    }

    /**
     * Permet de connecter un utilisateur à l'appli
     * @param utilisateur Utilisateur
     */
    public void setConnectedUtilisateur(Utilisateur utilisateur) {
        this.connectedUtilisateur = utilisateur;
    }

    public Utilisateur getConnectedUtilisateur() {
        return this.connectedUtilisateur;
    }

    /**
     * Permet de récupérer un joueur par ChoiceBox
     * @param choice
     * @return
     */

    public Utilisateur getJoueurByChoice(String choice) {
        for (Utilisateur utilisateur : this.utilisateurs) {
            if (utilisateur instanceof Joueur) {
                if ((utilisateur.getNom() + " - " + ((Joueur) utilisateur).getPoste()).equals(choice)) {
                    return utilisateur;
                }
            }
        }
        return null;
    }

    /**
     * Permet de récupérer un entraineur par choiceBox
     * @param choice
     * @return
     */
    public Utilisateur getEntraineurByChoice(String choice) {
        for (Utilisateur utilisateur : this.utilisateurs) {
            if (utilisateur instanceof Entraineur) {
                if ((utilisateur.getNom() + " - " + utilisateur.getPrenom()).equals(choice)) {
                    return utilisateur;
                }
            }
        }
        return null;
    }

    /**
     * Récupérer le type de l'utilisateur
     * @param utilisateur
     * @return
     */
    public String getUtilisateurType(Utilisateur utilisateur) {

        if (utilisateur instanceof Dirigeant) {
            return "Dirigeant";
        } else if (utilisateur instanceof Parent) {
            return "Parent";
        } else if (utilisateur instanceof Joueur) {
            return "Joueur";
        } else if (utilisateur instanceof Entraineur) {
            return "Entraineur";
        }

        return null;
    }

}
