package asfoot.utilisateurs.types;

import asfoot.utilisateurs.Utilisateur;

/**
 * Classe Entraineur
 * <p>
 * Cette classe représente un entraineur
 *
 * @see Utilisateur
 */
public class Entraineur extends Utilisateur {
    public Entraineur(int id, String email, String numeroTel, String nom, String prenom) {
        super(id, email, numeroTel, nom, prenom);
    }
}
