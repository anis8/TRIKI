package asfoot.utilisateurs.types;

import asfoot.utilisateurs.Utilisateur;

import java.util.ArrayList;

/**
 * Classe Parent
 * <p>
 * Cette classe représente un parent
 *
 * @see Utilisateur
 */
public class Parent extends Utilisateur {

    private ArrayList<Joueur> enfants;

    public Parent(int id, String email, String numeroTel, String nom, String prenom) {
        super(id, email, numeroTel, nom, prenom);
        this.enfants = new ArrayList<>();
    }

    public ArrayList<Joueur> getEnfants() {
        return this.enfants;
    }

    public Joueur getEnfant(int id) {
        for (Joueur joueur : this.enfants) {
            if (joueur.getId() == id) {
                return joueur;
            }
        }
        return null;
    }

    /**
     * Permet d'ajouter un enfant au parrent
     * @param joueur Joueur
     */

    public void addEnfant(Joueur joueur) {
        this.enfants.add(joueur);
    }

    /**
     * Permet de retirer un enfant au parent
     * @param joueur Joueur
     */
    public void removeEnfant(Joueur joueur) {
        this.enfants.remove(joueur);
    }
}
