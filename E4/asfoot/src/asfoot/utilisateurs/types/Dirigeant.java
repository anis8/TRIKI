package asfoot.utilisateurs.types;

import asfoot.utilisateurs.Utilisateur;

/**
 * Classe Dirigeant
 * <p>
 * Cette classe représente un dirigeant
 *
 * @see Utilisateur
 */
public class Dirigeant extends Utilisateur {

    private String fonction;

    public Dirigeant(int id, String email, String numeroTel, String nom, String prenom, String fonction) {
        super(id, email, numeroTel, nom, prenom);
        this.fonction = fonction;
    }

    public String getFonction() {
        return fonction;
    }

    public void setFonction(String fonction) {
        this.fonction = fonction;
    }
}
