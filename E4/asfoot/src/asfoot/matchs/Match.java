package asfoot.matchs;

import asfoot.equipes.Equipe;
import asfoot.stades.Stade;

/**
 * Classe Match
 * <p>
 * Cette classe représente un match
 *
 * @author Anis
 */
public class Match {
    private Stade stade;
    private Equipe equipeUne;
    private Equipe equipeDeux;
    private int id;
    private String date;

    /**
     * Constructeur
     * @param stade Stade
     * @param equipeUne Equipe
     * @param equipeDeux Equipe
     * @param id int
     * @param date String
     */
    public Match(Stade stade, Equipe equipeUne, Equipe equipeDeux, int id, String date) {
        this.stade = stade;
        this.equipeUne = equipeUne;
        this.equipeDeux = equipeDeux;
        this.id = id;
        this.date = date;
    }

    public Stade getStade() {
        return stade;
    }

    public void setStade(Stade stade) {
        this.stade = stade;
    }

    public Equipe getEquipeUne() {
        return equipeUne;
    }

    public void setEquipeUne(Equipe equipeUne) {
        this.equipeUne = equipeUne;
    }

    public Equipe getEquipeDeux() {
        return equipeDeux;
    }

    public void setEquipeDeux(Equipe equipeDeux) {
        this.equipeDeux = equipeDeux;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
