package asfoot.matchs.types;

import asfoot.equipes.Equipe;
import asfoot.matchs.Match;
import asfoot.stades.Stade;

/**
 * Classe MatchTermine
 * <p>
 * Cette classe représente un MatchTermine
 *
 * @see Match
 * @author Anis
 */

public class MatchTermine extends Match {

    private int equipeUneButs;
    private int equipeDeuxButs;

    /**
     *
     * @param stade Stade
     * @param equipeUne Equipe
     * @param equipeDeux Equipe
     * @param id int
     * @param date String
     * @param equipeUneButs int
     * @param equipeDeuxButs int
     */
    public MatchTermine(Stade stade, Equipe equipeUne, Equipe equipeDeux, int id, String date, int equipeUneButs, int equipeDeuxButs) {
        super(stade, equipeUne, equipeDeux, id, date);
        this.equipeUneButs = equipeUneButs;
        this.equipeDeuxButs = equipeDeuxButs;
    }


    public int getEquipeUneButs() {
        return equipeUneButs;
    }

    public void setEquipeUneButs(int equipeUneButs) {
        this.equipeUneButs = equipeUneButs;
    }

    public int getEquipeDeuxButs() {
        return equipeDeuxButs;
    }

    public void setEquipeDeuxButs(int equipeDeuxButs) {
        this.equipeDeuxButs = equipeDeuxButs;
    }
}
