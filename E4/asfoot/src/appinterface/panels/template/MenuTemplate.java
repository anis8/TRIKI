package appinterface.panels.template;

import appinterface.panels.ITemplate;
import appinterface.panels.PanelsManager;
import asfoot.utilisateurs.Utilisateur;
import asfoot.utilisateurs.UtilisateursManager;
import asfoot.utilisateurs.types.Dirigeant;
import asfoot.utilisateurs.types.Entraineur;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;

public class MenuTemplate implements ITemplate {

    private MenuBar menuBar;

    public void draw() {
        this.menuBar = new MenuBar();
        this.menuBar.setMinWidth(PanelsManager.getInstance().getRoot().getScene().getWidth());
        this.menuBar.setStyle("-fx-font-family: 'Trebuchet MS', Helvetica, sans-serif;-fx-background-color: white;-fx-border-width: 0 0 1 0;-fx-font-size: 20px; -fx-border-color: red blue rgb(200,200,200) yellow;");

        Utilisateur utilisateur = UtilisateursManager.getInstance().getConnectedUtilisateur();
        // Create menus

        Menu profilMenue = new Menu("Mon profil");
        Menu equipesMenue = new Menu("Equipes");
        Menu matchsMenue = new Menu("Matchs");
        Menu stadesMenue = new Menu("Stades");
        Menu categoriesMenue = new Menu("Catégories");

        // Create MenuItems
        MenuItem pageDeProfilItem = new MenuItem("Page de profil");

        MenuItem modifierMonCompteItem = new MenuItem("Modifier mon compte");
        MenuItem deconnexionItem = new MenuItem("Déconnexion");
        MenuItem inscriptionItem = new MenuItem("Inscription");
        if (utilisateur != null) {
            profilMenue.getItems().addAll(pageDeProfilItem, modifierMonCompteItem, deconnexionItem);
        } else {
            profilMenue.getItems().addAll(pageDeProfilItem, inscriptionItem);
        }


        MenuItem classementItem = new MenuItem("Classement");
        MenuItem toutesLesEquipesItem = new MenuItem("Toutes les équipes");
        MenuItem ajouterUneEquipeItem = new MenuItem("Ajouter une équipe");


        ajouterUneEquipeItem.setOnAction(e -> {
            PanelsManager.getInstance().renderPanel("AjouterEquipePanel");
        });

        if (utilisateur instanceof Dirigeant) {
            equipesMenue.getItems().addAll(classementItem, toutesLesEquipesItem, ajouterUneEquipeItem);
        } else {
            equipesMenue.getItems().addAll(classementItem, toutesLesEquipesItem);
        }

        toutesLesEquipesItem.setOnAction(e -> {
            PanelsManager.getInstance().renderPanel("ListeEquipesPanel");
        });

        MenuItem listeDesMatchsItem = new MenuItem("Liste des matchs");
        MenuItem ajouterUnMatchItem = new MenuItem("Ajouter un match");
        if (utilisateur instanceof Entraineur || utilisateur instanceof Dirigeant) {
            matchsMenue.getItems().addAll(listeDesMatchsItem, ajouterUnMatchItem);
        } else {
            matchsMenue.getItems().add(listeDesMatchsItem);
        }

        MenuItem listeDesStadesItem = new MenuItem("Liste des stades");
        MenuItem ajouterUnStadeItem = new MenuItem("Ajouter un stade");
        if (utilisateur instanceof Dirigeant) {
            stadesMenue.getItems().addAll(listeDesStadesItem, ajouterUnStadeItem);

            ajouterUnStadeItem.setOnAction(event -> {
                PanelsManager.getInstance().renderPanel("AjouterStadePanel");
            });
        } else {
            stadesMenue.getItems().addAll(listeDesStadesItem);
        }

        listeDesStadesItem.setOnAction(event -> {
            PanelsManager.getInstance().renderPanel("ListeStadesPanel");
        });


        MenuItem listeDesCategoriesItem = new MenuItem("Liste des catégories");

        listeDesCategoriesItem.setOnAction(event -> {
            PanelsManager.getInstance().renderPanel("ListeCategoriesPanel");
        });

        MenuItem ajouterUnCategorieItem = new MenuItem("Ajouter un catégorie");
        ajouterUnCategorieItem.setOnAction(event -> {
            PanelsManager.getInstance().renderPanel("AjouterCategoriePanel");
        });

        if (utilisateur instanceof Dirigeant) {
            categoriesMenue.getItems().addAll(listeDesCategoriesItem, ajouterUnCategorieItem);
        } else {
            categoriesMenue.getItems().addAll(listeDesCategoriesItem);
        }

        modifierMonCompteItem.setOnAction(e -> {
            PanelsManager.getInstance().renderPanel("ProfilModificationPanel");
        });

        inscriptionItem.setOnAction(e -> {
            PanelsManager.getInstance().renderPanel("InscriptionPanel");
        });

        classementItem.setOnAction(e -> {
            PanelsManager.getInstance().renderPanel("ClassementEquipePanel");
        });

        pageDeProfilItem.setOnAction(e -> {
            PanelsManager.getInstance().renderPanel("ProfilPanel");
        });
        deconnexionItem.setOnAction(e -> {
            UtilisateursManager.getInstance().setConnectedUtilisateur(null);
            PanelsManager.getInstance().renderPanel("ProfilPanel");
        });
        ajouterUnMatchItem.setOnAction(e -> {
            PanelsManager.getInstance().renderPanel("AjouterMatchPanel");
        });
        listeDesMatchsItem.setOnAction(e -> {
            PanelsManager.getInstance().renderPanel("ListeMatchsPanel");
        });

        this.menuBar.getMenus().addAll(profilMenue, equipesMenue, matchsMenue, stadesMenue, categoriesMenue);

        if (utilisateur instanceof Dirigeant) {
            MenuItem listeDesUtilisateursItem = new MenuItem("Liste des utilisateurs");
            Menu utilisateursMenue = new Menu("Utilisateurs");
            MenuItem ajouterUnUtilisateurItem = new MenuItem("Ajouter un utilisateur");
            utilisateursMenue.getItems().addAll(listeDesUtilisateursItem, ajouterUnUtilisateurItem);

            listeDesUtilisateursItem.setOnAction(e -> {
                PanelsManager.getInstance().renderPanel("ListeUtilisateursPanel");
            });

            ajouterUnUtilisateurItem.setOnAction(e -> {
                PanelsManager.getInstance().renderPanel("AjouterUtilisateurPanel");
            });


            this.menuBar.getMenus().add(utilisateursMenue);
        }


    }

    public void render() {
        this.draw();
        PanelsManager.getInstance().getRoot().getChildren().add(this.menuBar);
    }

}
