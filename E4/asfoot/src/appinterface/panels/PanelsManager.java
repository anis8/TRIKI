package appinterface.panels;

import appinterface.panels.template.MenuTemplate;
import appinterface.panels.template.NotificationTemplate;
import appinterface.panels.types.categories.AjouterCategoriePanel;
import appinterface.panels.types.categories.ListeCategoriesPanel;
import appinterface.panels.types.categories.ModifierCategoriePanel;
import appinterface.panels.types.equipes.AjouterEquipePanel;
import appinterface.panels.types.equipes.ClassementEquipePanel;
import appinterface.panels.types.equipes.ListeEquipesPanel;
import appinterface.panels.types.equipes.ModifierEquipePanel;
import appinterface.panels.types.matchs.AjouterMatchPanel;
import appinterface.panels.types.matchs.ListeMatchsPanel;
import appinterface.panels.types.matchs.ModifierMatchPanel;
import appinterface.panels.types.profil.InscriptionPanel;
import appinterface.panels.types.profil.ProfilModificationPanel;
import appinterface.panels.types.profil.ProfilPanel;
import appinterface.panels.types.stades.AjouterStadePanel;
import appinterface.panels.types.stades.ListeStadesPanel;
import appinterface.panels.types.stades.ModifierStadesPanel;
import appinterface.panels.types.utilisateurs.AjouterUtilisateurPanel;
import appinterface.panels.types.utilisateurs.ListeUtilisateursPanel;
import appinterface.panels.types.utilisateurs.ModifierUtilisateurPanel;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.HashMap;

/**
 * Classe PanelsManager
 * <p>
 * Cette classe permet la gestion des panel
 *
 * @author Anis
 */
public class PanelsManager {
    private static PanelsManager panelsManagerInstance;

    private Group root;
    private Stage primaryStage;
    private Scene scene;
    private HashMap<String, IPanel> panels;
    private HashMap<String, ITemplate> templates;
    private String font = "http://anis.alwaysdata.net/asfoot/assets/fonts/UbuntuCondensed-Regular.ttf";

    /**
     * Initialise la classe
     * @param root Group
     * @param primaryStage Stage
     * @param scene Scene
     */
    public void init(Group root, Stage primaryStage, Scene scene) {
        this.root = root;
        this.primaryStage = primaryStage;
        this.scene = scene;
        this.panels = new HashMap<>();
        this.templates = new HashMap<>();
        System.out.print("PanelsManager prêt.\n");
        this.initPanels();
        this.initTemplates();

    }

    public Group getRoot() {
        return root;
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public Scene getScene() {
        return scene;
    }


    /**
     * Permet d'initialiser les panels
     */
    private void initPanels() {
        this.panels.put("ProfilPanel", new ProfilPanel());
        this.panels.put("InscriptionPanel", new InscriptionPanel());
        this.panels.put("ProfilModificationPanel", new ProfilModificationPanel());

        this.panels.put("ListeUtilisateursPanel", new ListeUtilisateursPanel());
        this.panels.put("ModifierUtilisateurPanel", new ModifierUtilisateurPanel());
        this.panels.put("AjouterUtilisateurPanel", new AjouterUtilisateurPanel());

        this.panels.put("AjouterMatchPanel", new AjouterMatchPanel());
        this.panels.put("ListeMatchsPanel", new ListeMatchsPanel());
        this.panels.put("ModifierMatchPanel", new ModifierMatchPanel());

        this.panels.put("ClassementEquipePanel", new ClassementEquipePanel());
        this.panels.put("ListeEquipesPanel", new ListeEquipesPanel());
        this.panels.put("ModifierEquipePanel", new ModifierEquipePanel());
        this.panels.put("AjouterEquipePanel", new AjouterEquipePanel());

        this.panels.put("AjouterStadePanel", new AjouterStadePanel());
        this.panels.put("ListeStadesPanel", new ListeStadesPanel());
        this.panels.put("ModifierStadesPanel", new ModifierStadesPanel());

        this.panels.put("AjouterCategoriePanel", new AjouterCategoriePanel());
        this.panels.put("ModifierCategoriePanel", new ModifierCategoriePanel());
        this.panels.put("ListeCategoriesPanel", new ListeCategoriesPanel());

        this.panels.get("ProfilPanel").render();
    }

    /**
     * Permet d'initialiser l'interface de départ
     */
    private void initTemplates() {
        this.templates.put("MenuTemplate", new MenuTemplate());
        this.templates.put("NotificationTemplate", new NotificationTemplate());
        this.drawTemplates();
    }


    /**
     * Permet d'afficher un message sur popup
     * @param message String
     */
    public void setNotification(String message) {
        ITemplate template = this.templates.get("NotificationTemplate");
        if (template instanceof NotificationTemplate) {
            ((NotificationTemplate) template).setMessage(message);
            this.templates.get("NotificationTemplate").render();
        }
    }

    /**
     * Permet de d'afficher la template et le menu
     */
    private void drawTemplates() {
        this.templates.get("MenuTemplate").render();
    }

    public static PanelsManager getInstance() {
        if (panelsManagerInstance == null)
            panelsManagerInstance = new PanelsManager();
        return panelsManagerInstance;
    }

    public String getFont() {
        return this.font;
    }

    /**
     * Permet d'afficher un panel
     * @param name String
     */

    public void renderPanel(String name) {
        this.root.getChildren().clear();
        this.drawTemplates();
        this.panels.get(name).getPane().getChildren().clear();
        this.panels.get(name).render();
    }


    public IPanel getPanel(String name) {
        return this.panels.get(name);
    }

}
