package appinterface.panels.types.categories;

import appinterface.panels.IPanel;
import appinterface.panels.PanelsManager;
import appinterface.panels.types.matchs.ModifierMatchPanel;
import asfoot.categories.Categorie;
import asfoot.categories.CategoriesManager;
import asfoot.matchs.Match;
import asfoot.matchs.MatchsManager;
import asfoot.matchs.types.MatchTermine;
import asfoot.utilisateurs.Utilisateur;
import asfoot.utilisateurs.UtilisateursManager;
import asfoot.utilisateurs.types.Dirigeant;
import asfoot.utilisateurs.types.Entraineur;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;

public class ListeCategoriesPanel implements IPanel {

    private AnchorPane pane;

    public ListeCategoriesPanel() {
        this.pane = new AnchorPane();
        this.pane.setTranslateY(55);
        this.pane.setTranslateX(13);
        this.pane.setMinWidth(PanelsManager.getInstance().getRoot().getScene().getWidth() - 26);
        this.pane.setMinHeight(PanelsManager.getInstance().getRoot().getScene().getHeight() - 70);
        this.pane.setStyle("-fx-font-family: 'Trebuchet MS', Helvetica, sans-serif;");
    }

    @SuppressWarnings("Duplicates")
    public void draw() {
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setMinHeight(this.pane.getMinHeight());
        scrollPane.setMinWidth(this.pane.getMinWidth());

        TableView<Categorie> table = new TableView<>();
        table.setMinWidth(this.pane.getMinWidth());
        table.setMinHeight(this.pane.getMinHeight());

        TableColumn<Categorie, String> nom = new TableColumn<>("Nom de la catégorie");
        nom.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getNom()));


        for (Categorie categorie : CategoriesManager.getInstance().getCategories()) {
            table.getItems().addAll(categorie);
        }

        table.setRowFactory(tv -> {
            TableRow<Categorie> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (!row.isEmpty() && event.getButton() == MouseButton.PRIMARY
                        && event.getClickCount() == 2) {
                    Utilisateur connectedUtilisateur = UtilisateursManager.getInstance().getConnectedUtilisateur();
                    if (connectedUtilisateur instanceof Dirigeant) {
                        Categorie categorie = row.getItem();
                        IPanel panel = PanelsManager.getInstance().getPanel("ModifierCategoriePanel");
                        if (panel instanceof ModifierCategoriePanel) {
                            ((ModifierCategoriePanel) panel).setCategorie(categorie);
                        }
                        PanelsManager.getInstance().renderPanel("ModifierCategoriePanel");
                    }
                }
            });
            return row;
        });

        table.getColumns().add(nom);
        scrollPane.setContent(table);
        this.pane.getChildren().add(scrollPane);

    }

    public void render() {
        this.draw();
        PanelsManager.getInstance().getRoot().getChildren().add(this.pane);
    }

    public AnchorPane getPane() {
        return this.pane;
    }
}

