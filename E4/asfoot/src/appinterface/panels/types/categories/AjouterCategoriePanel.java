package appinterface.panels.types.categories;

import appinterface.panels.IPanel;
import appinterface.panels.PanelsManager;
import asfoot.categories.Categorie;
import asfoot.categories.CategoriesManager;
import asfoot.dao.DaoManager;
import asfoot.equipes.Equipe;
import asfoot.equipes.EquipesManager;
import asfoot.matchs.Match;
import asfoot.matchs.MatchsManager;
import asfoot.stades.Stade;
import asfoot.stades.StadesManager;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

public class AjouterCategoriePanel implements IPanel {

    private AnchorPane pane;

    public AjouterCategoriePanel() {
        this.pane = new AnchorPane();
        this.pane.setTranslateY(55);
        this.pane.setTranslateX(13);
        this.pane.setMinWidth(PanelsManager.getInstance().getRoot().getScene().getWidth() - 26);
        this.pane.setMinHeight(PanelsManager.getInstance().getRoot().getScene().getHeight() - 70);
        this.pane.setStyle("-fx-font-family: 'Trebuchet MS', Helvetica, sans-serif;");
    }

    @SuppressWarnings("Duplicates")
    public void draw() {
        GridPane grid = new GridPane();
        grid.setMinWidth(this.pane.getMinWidth());
        grid.setMinHeight(this.pane.getMinHeight());
        grid.setAlignment(Pos.CENTER);

        Text scenetitle = new Text("Ajouter une catégorie");
        scenetitle.setStyle("-fx-font-size: 60px;");
        grid.add(scenetitle, 0, 0, 1, 1);

        Label nomLabel = new Label("Non de la catégorie:");
        grid.add(nomLabel, 0, 7);
        nomLabel.setStyle("-fx-font-size: 20px");

        TextField nomField = new TextField();
        grid.add(nomField, 0, 8);
        nomField.setMinWidth(300);
        nomField.setMinHeight(40);
        nomField.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");


        final Button ajouterButton = new Button();
        ajouterButton.setText("Ajouter");
        grid.add(ajouterButton, 0, 9);
        ajouterButton.setMinWidth(600);
        ajouterButton.setMinHeight(30);
        ajouterButton.setTranslateY(10);


        ajouterButton.setOnAction(e -> {

            if (!nomField.getText().equals("")) {
                Categorie categorie = new Categorie(0, nomField.getText());
                CategoriesManager.getInstance().addCategorie(categorie);
                DaoManager.getInstance().getCategoriesDao().createCategorie(categorie);
                PanelsManager.getInstance().setNotification("La catégorie a bien été ajoutée.");
            } else {
                PanelsManager.getInstance().setNotification("Veuillez remplir les champs vide.");
            }

        });

        this.pane.getChildren().add(grid);
    }

    public void render() {
        this.draw();
        PanelsManager.getInstance().getRoot().getChildren().add(this.pane);
    }

    public AnchorPane getPane() {
        return this.pane;
    }
}

