package appinterface.panels.types.categories;

import appinterface.panels.IPanel;
import appinterface.panels.PanelsManager;
import asfoot.categories.Categorie;
import asfoot.categories.CategoriesManager;
import asfoot.dao.DaoManager;

import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;

public class ModifierCategoriePanel implements IPanel {

    private AnchorPane pane;
    private Categorie categorie;

    public ModifierCategoriePanel() {
        this.pane = new AnchorPane();
        this.pane.setTranslateY(55);
        this.pane.setTranslateX(13);
        this.pane.setMinWidth(PanelsManager.getInstance().getRoot().getScene().getWidth() - 26);
        this.pane.setMinHeight(PanelsManager.getInstance().getRoot().getScene().getHeight() - 70);
        this.pane.setStyle("-fx-font-family: 'Trebuchet MS', Helvetica, sans-serif;");
    }

    @SuppressWarnings("Duplicates")
    public void draw() {
        GridPane grid = new GridPane();
        grid.setMinWidth(this.pane.getMinWidth());
        grid.setMinHeight(this.pane.getMinHeight());
        grid.setAlignment(Pos.CENTER);

        Label nomLabel = new Label("Non de la catégorie:");
        grid.add(nomLabel, 0, 7);
        nomLabel.setStyle("-fx-font-size: 20px");

        TextField nomField = new TextField();
        grid.add(nomField, 0, 8);
        nomField.setMinWidth(300);
        nomField.setMinHeight(40);
        nomField.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");
        nomField.setText(this.categorie.getNom());

        final Button ajouterButton = new Button();
        ajouterButton.setText("Modifier");
        grid.add(ajouterButton, 0, 9);
        ajouterButton.setMinWidth(600);
        ajouterButton.setMinHeight(30);
        ajouterButton.setTranslateY(10);

        final Button supprimerButton = new Button();
        supprimerButton.setText("Supprimer");
        grid.add(supprimerButton, 0, 10);
        supprimerButton.setMinWidth(600);
        supprimerButton.setMinHeight(30);
        supprimerButton.setTranslateY(10);

        supprimerButton.setOnAction(event -> {

            CategoriesManager.getInstance().getCategories().remove(this.categorie);
            DaoManager.getInstance().getCategoriesDao().deleteCategorie(this.categorie);
            PanelsManager.getInstance().renderPanel("ListeCategoriesPanel");
            PanelsManager.getInstance().setNotification("La catégorie a bien été supprimé.");

        });

        ajouterButton.setOnAction(e -> {

            if (!nomField.getText().equals("")) {
                this.categorie.setNom(nomField.getText());
                DaoManager.getInstance().getCategoriesDao().updateCategorie(categorie);
                PanelsManager.getInstance().setNotification("La catégorie a bien été mise à jour.");
            } else {
                PanelsManager.getInstance().setNotification("Veuillez remplir les champs vide.");
            }

        });

        this.pane.getChildren().add(grid);
    }

    public void render() {
        this.draw();
        PanelsManager.getInstance().getRoot().getChildren().add(this.pane);
    }

    public AnchorPane getPane() {
        return this.pane;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }
}


