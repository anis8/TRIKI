package appinterface.panels.types.utilisateurs;

import appinterface.panels.IPanel;
import appinterface.panels.PanelsManager;
import asfoot.categories.Categorie;
import asfoot.categories.CategoriesManager;
import asfoot.dao.DaoManager;
import asfoot.equipes.Equipe;
import asfoot.equipes.EquipesManager;
import asfoot.utilisateurs.Utilisateur;
import asfoot.utilisateurs.UtilisateursManager;
import asfoot.utilisateurs.types.Dirigeant;
import asfoot.utilisateurs.types.Entraineur;
import asfoot.utilisateurs.types.Joueur;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

public class ModifierUtilisateurPanel implements IPanel {

    private AnchorPane pane;
    private Utilisateur utilisateur;


    public ModifierUtilisateurPanel() {
        this.utilisateur = null;
        this.pane = new AnchorPane();
        this.pane.setTranslateY(55);
        this.pane.setTranslateX(13);
        this.pane.setMinWidth(PanelsManager.getInstance().getRoot().getScene().getWidth() - 26);
        this.pane.setMinHeight(PanelsManager.getInstance().getRoot().getScene().getHeight() - 70);
        this.pane.setStyle("-fx-font-family: 'Trebuchet MS', Helvetica, sans-serif;");
    }

    @SuppressWarnings("Duplicates")

    public void draw() {
        if (this.utilisateur != null) {
            GridPane grid = new GridPane();
            grid.setMinWidth(this.pane.getMinWidth());
            grid.setMinHeight(this.pane.getMinHeight());
            grid.setAlignment(Pos.CENTER);

            Text scenetitle = new Text("Profil du compte");
            scenetitle.setStyle("-fx-font-size: 60px;");
            grid.add(scenetitle, 0, 0, 2, 1);

            Label mailLabel = new Label("Adresse email:");
            grid.add(mailLabel, 0, 1);
            mailLabel.setStyle("-fx-font-size: 20px");

            TextField mailField = new TextField();
            grid.add(mailField, 0, 2);
            mailField.setMinWidth(300);
            mailField.setMinHeight(40);
            mailField.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");
            mailField.setText(utilisateur.getEmail());

            Label passwordLabel = new Label("Mot de passe:");
            grid.add(passwordLabel, 0, 3);
            passwordLabel.setStyle("-fx-font-size: 20px");

            TextField passwordField = new PasswordField();
            grid.add(passwordField, 0, 4);
            passwordField.setMinWidth(300);
            passwordField.setMinHeight(40);
            passwordField.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");
            passwordField.setText(utilisateur.getMdp());

            Label numLabel = new Label("Numéro de téléphone:");
            grid.add(numLabel, 0, 5);
            numLabel.setStyle("-fx-font-size: 20px");

            TextField numField = new TextField();
            grid.add(numField, 0, 6);
            numField.setMinWidth(300);
            numField.setMinHeight(40);
            numField.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");
            numField.setText(utilisateur.getNumeroTel());

            Label nomLabel = new Label("Nom:");
            grid.add(nomLabel, 0, 7);
            nomLabel.setStyle("-fx-font-size: 20px");

            TextField nomField = new TextField();
            grid.add(nomField, 0, 8);
            nomField.setMinWidth(300);
            nomField.setMinHeight(40);
            nomField.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");
            nomField.setText(utilisateur.getNom());

            Label prenomLabel = new Label("Prenom:");
            grid.add(prenomLabel, 0, 9);
            prenomLabel.setStyle("-fx-font-size: 20px");

            TextField prenomField = new TextField();
            grid.add(prenomField, 0, 10);
            prenomField.setMinWidth(300);
            prenomField.setMinHeight(40);
            prenomField.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");
            prenomField.setText(utilisateur.getPrenom());

            TextField fonctionField = new TextField();
            if (utilisateur instanceof Dirigeant) {
                Label fonctionLabel = new Label("Fonction:");
                grid.add(fonctionLabel, 1, 1);
                fonctionLabel.setStyle("-fx-font-size: 20px");

                grid.add(fonctionField, 1, 2);
                fonctionField.setMinWidth(300);
                fonctionField.setMinHeight(40);
                fonctionField.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");
                fonctionField.setText(((Dirigeant) utilisateur).getFonction());
            }


            if(utilisateur instanceof Entraineur){
                Equipe equipe = EquipesManager.getInstance().getEquipeByEntraineur((Entraineur) this.utilisateur);
                Label entraineurLabel = new Label("Equipe: " + ((equipe == null) ? "Aucune" : equipe.getNom()));
                grid.add(entraineurLabel, 1, 1);
                entraineurLabel.setStyle("-fx-font-size: 20px");
            }

            ChoiceBox categorieChoice = new ChoiceBox();
            TextField posteField = new TextField();
            TextField numeroField = new TextField();
            TextField naissanceField = new TextField();
            TextField photoField = new TextField();
            if (utilisateur instanceof Joueur) {
                Label categorieLabel = new Label("Catégorie:");
                grid.add(categorieLabel, 1, 1);
                categorieLabel.setStyle("-fx-font-size: 20px");


                for (Categorie categorie : CategoriesManager.getInstance().getCategories()) {
                    categorieChoice.getItems().add(categorie.getNom());
                }
                grid.add(categorieChoice, 1, 2);
                categorieChoice.setMinWidth(350);
                categorieChoice.setMinHeight(40);
                categorieChoice.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");
                categorieChoice.setValue(((Joueur) utilisateur).getCategorie().getNom());

                Label posteLabel = new Label("Poste:");
                grid.add(posteLabel, 1, 3);
                posteLabel.setStyle("-fx-font-size: 20px");

                grid.add(posteField, 1, 4);
                posteField.setMinWidth(300);
                posteField.setMinHeight(40);
                posteField.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");
                posteField.setText(((Joueur) utilisateur).getPoste());

                Label numeroLabel = new Label("Numero de maillot:");
                grid.add(numeroLabel, 1, 5);
                numeroLabel.setStyle("-fx-font-size: 20px");

                grid.add(numeroField, 1, 6);
                numeroField.setMinWidth(300);
                numeroField.setMinHeight(40);
                numeroField.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");
                numeroField.setText(Integer.toString(((Joueur) utilisateur).getNumero()));


                Label naissanceLabel = new Label("Date de naissance:");
                grid.add(naissanceLabel, 1, 7);
                naissanceLabel.setStyle("-fx-font-size: 20px");

                grid.add(naissanceField, 1, 8);
                naissanceField.setMinWidth(300);
                naissanceField.setMinHeight(40);
                naissanceField.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");
                naissanceField.setText(((Joueur) utilisateur).getDatenaissance());


                Label photoLabel = new Label("URL de la photo:");
                grid.add(photoLabel, 1, 9);
                photoLabel.setStyle("-fx-font-size: 20px");

                grid.add(photoField, 1, 10);
                photoField.setMinWidth(300);
                photoField.setMinHeight(40);
                photoField.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");
                photoField.setText(((Joueur) utilisateur).getPhoto());

                Equipe equipe = EquipesManager.getInstance().getEquipeByJoueur((Joueur) this.utilisateur);
                Label equipeLabel = new Label("Equipe: " + ((equipe == null) ? "Aucune" : equipe.getNom()));
                grid.add(equipeLabel, 1, 11);
                equipeLabel.setStyle("-fx-font-size: 20px");

            }


            final Button modificationButton = new Button();
            modificationButton.setText("Mettre à jour");
            grid.add(modificationButton, 0, 13);
            modificationButton.setMinWidth(300);
            modificationButton.setMinHeight(30);
            modificationButton.setTranslateY(10);

            final Button suppressionButton = new Button();
            suppressionButton.setText("Supprimer un utilisateur");
            grid.add(suppressionButton, 0, 14);
            suppressionButton.setMinWidth(300);
            suppressionButton.setMinHeight(30);
            suppressionButton.setTranslateY(10);


            suppressionButton.setOnAction(e -> {
                UtilisateursManager.getInstance().getUtilisateurs().remove(utilisateur);
                DaoManager.getInstance().getUtilisateursDao().deleteUtilisateur(utilisateur);
                PanelsManager.getInstance().renderPanel("ListeUtilisateursPanel");
                PanelsManager.getInstance().setNotification("L'utilisateur a bien été supprimé.");
            });

            modificationButton.setOnAction(e -> {
                utilisateur.setEmail(mailField.getText());
                utilisateur.setMdp(passwordField.getText());
                utilisateur.setNumeroTel(numField.getText());
                utilisateur.setNom(nomField.getText());
                utilisateur.setPrenom(prenomField.getText());

                if (utilisateur instanceof Dirigeant) {
                    ((Dirigeant) utilisateur).setFonction(fonctionField.getText());
                }

                if (utilisateur instanceof Joueur) {
                    ((Joueur) utilisateur).setCategorie(CategoriesManager.getInstance().getCategorieByNom(categorieChoice.getValue().toString()));
                    ((Joueur) utilisateur).setPoste(posteField.getText());
                    ((Joueur) utilisateur).setNumero(Integer.parseInt(numeroField.getText()));
                    ((Joueur) utilisateur).setDatenaissance(naissanceField.getText());
                    ((Joueur) utilisateur).setPhoto(photoField.getText());
                }

                PanelsManager.getInstance().setNotification("Les informations ont bien été modifiées.");
                DaoManager.getInstance().getUtilisateursDao().updateUtilisateur(this.utilisateur);
            });


            this.pane.getChildren().add(grid);
        }
    }

    public void render() {
        this.draw();
        PanelsManager.getInstance().getRoot().getChildren().add(this.pane);
    }

    public AnchorPane getPane() {
        return this.pane;
    }

    public void setUtilisateur(Utilisateur utilisateur) {
        this.utilisateur = utilisateur;
    }

    public Utilisateur getUtilisateur() {
        return this.utilisateur;
    }
}
