package appinterface.panels.types.equipes;

import appinterface.panels.IPanel;
import appinterface.panels.PanelsManager;
import asfoot.categories.Categorie;
import asfoot.categories.CategoriesManager;
import asfoot.dao.DaoManager;
import asfoot.equipes.Equipe;
import asfoot.equipes.EquipesManager;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

public class AjouterEquipePanel implements IPanel {

    private AnchorPane pane;

    public AjouterEquipePanel() {
        this.pane = new AnchorPane();
        this.pane.setTranslateY(55);
        this.pane.setTranslateX(13);
        this.pane.setMinWidth(PanelsManager.getInstance().getRoot().getScene().getWidth() - 26);
        this.pane.setMinHeight(PanelsManager.getInstance().getRoot().getScene().getHeight() - 70);
        this.pane.setStyle("-fx-font-family: 'Trebuchet MS', Helvetica, sans-serif;");
    }

    @SuppressWarnings("Duplicates")
    public void draw() {
        GridPane grid = new GridPane();
        grid.setMinWidth(this.pane.getMinWidth());
        grid.setMinHeight(this.pane.getMinHeight());
        grid.setAlignment(Pos.CENTER);


        Text scenetitle = new Text("Ajouter une équipe");
        scenetitle.setStyle("-fx-font-size: 60px;");
        grid.add(scenetitle, 0, 0, 1, 1);

        Label categorieLabel = new Label("Catégorie:");
        grid.add(categorieLabel, 0, 1);
        categorieLabel.setStyle("-fx-font-size: 20px");


        ChoiceBox categorieChoice = new ChoiceBox();
        for (Categorie categorie : CategoriesManager.getInstance().getCategories()) {
            categorieChoice.getItems().add(categorie.getNom());
        }
        grid.add(categorieChoice, 0, 2);
        categorieChoice.setMinWidth(600);
        categorieChoice.setMinHeight(40);
        categorieChoice.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");


        Label nomLabel = new Label("Nom de l'équipe:");
        grid.add(nomLabel, 0, 3);
        nomLabel.setStyle("-fx-font-size: 20px");

        TextField nomField = new TextField();
        grid.add(nomField, 0, 4);
        nomField.setMinWidth(300);
        nomField.setMinHeight(40);
        nomField.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");


        Label ecussonLabel = new Label("Ecusson de l'équipe:");
        grid.add(ecussonLabel, 0, 5);
        ecussonLabel.setStyle("-fx-font-size: 20px");

        TextField ecussonField = new TextField();
        grid.add(ecussonField, 0, 6);
        ecussonField.setMinWidth(300);
        ecussonField.setMinHeight(40);
        ecussonField.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");

        final Button ajouterButton = new Button();
        ajouterButton.setText("Ajouter");
        grid.add(ajouterButton, 0, 7);
        ajouterButton.setMinWidth(600);
        ajouterButton.setMinHeight(30);
        ajouterButton.setTranslateY(10);


        ajouterButton.setOnAction(e -> {

            if (categorieChoice.getValue() != null && !nomField.getText().equals("") && !ecussonField.getText().equals("")) {
                Equipe equipe = new Equipe(
                        CategoriesManager.getInstance().getCategorieByNom(categorieChoice.getValue().toString()),
                        nomField.getText(),
                        ecussonField.getText()
                );

                EquipesManager.getInstance().addEquipe(equipe);
                DaoManager.getInstance().getEquipesDao().createEquipe(equipe);
                PanelsManager.getInstance().setNotification("L'équipe a bien été ajoutée.");
            } else {
                PanelsManager.getInstance().setNotification("Veuillez remplir les champs vide.");
            }

        });

        this.pane.getChildren().add(grid);
    }

    public void render() {
        this.draw();
        PanelsManager.getInstance().getRoot().getChildren().add(this.pane);
    }

    public AnchorPane getPane() {
        return this.pane;
    }
}
