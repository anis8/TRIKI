package appinterface.panels.types.equipes;

import appinterface.panels.IPanel;
import appinterface.panels.PanelsManager;
import asfoot.categories.Categorie;
import asfoot.categories.CategoriesManager;
import asfoot.dao.DaoManager;
import asfoot.equipes.Equipe;
import asfoot.equipes.EquipesManager;
import asfoot.utilisateurs.Utilisateur;
import asfoot.utilisateurs.UtilisateursManager;
import asfoot.utilisateurs.types.Dirigeant;
import asfoot.utilisateurs.types.Entraineur;
import asfoot.utilisateurs.types.Joueur;
import javafx.beans.property.SimpleStringProperty;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class ModifierEquipePanel implements IPanel {

    private AnchorPane pane;
    private Equipe equipe;

    public ModifierEquipePanel() {
        this.pane = new AnchorPane();
        this.pane.setTranslateY(55);
        this.pane.setTranslateX(13);
        this.pane.setMinWidth(PanelsManager.getInstance().getRoot().getScene().getWidth() - 26);
        this.pane.setMinHeight(PanelsManager.getInstance().getRoot().getScene().getHeight() - 70);
        this.pane.setStyle("-fx-font-family: 'Trebuchet MS', Helvetica, sans-serif;");
        this.equipe = null;
    }

    @SuppressWarnings("Duplicates")
    public void draw() {
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setMaxHeight(this.pane.getMinHeight());
        scrollPane.setMinWidth(this.pane.getMinWidth());

        GridPane grid = new GridPane();
        grid.setMinWidth(this.pane.getMinWidth());
        grid.setTranslateY(0);
        grid.setAlignment(Pos.CENTER);

        Text scenetitle = new Text("Gestion de l'équipe");
        scenetitle.setStyle("-fx-font-size: 60px;");
        grid.add(scenetitle, 0, 0, 2, 1);


        Label joueursTitle = new Label("Gestion des joueurs:");
        grid.add(joueursTitle, 0, 1);
        joueursTitle.setStyle("-fx-font-size: 20px");


        GridPane ajouterJoueurGrid = new GridPane();
        VBox ajouterJoueurBox = new VBox();
        ChoiceBox ajouterJoueurChoice = new ChoiceBox();
        for (Utilisateur utilisateur : UtilisateursManager.getInstance().getUtilisateurs()) {
            if (utilisateur instanceof Joueur) {
                ajouterJoueurChoice.getItems().add(utilisateur.getNom() + " - " + ((Joueur) utilisateur).getPoste());
            }
        }
        ajouterJoueurChoice.setMinWidth(350);
        ajouterJoueurChoice.setMinHeight(60);
        ajouterJoueurChoice.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");
        final Button ajouterButton = new Button();
        ajouterButton.setText("Ajouter le joueur");
        ajouterButton.setMinWidth(350);
        ajouterButton.setMinHeight(60);
        ajouterButton.setTranslateY(10);

        ajouterButton.setOnAction(event -> {
            if (ajouterJoueurChoice.getValue() != null) {
                Joueur joueur = (Joueur) UtilisateursManager.getInstance().getJoueurByChoice(ajouterJoueurChoice.getValue().toString());
                if (EquipesManager.getInstance().getEquipeByJoueur(joueur) == null) {
                    this.equipe.addJoueur(joueur);
                    PanelsManager.getInstance().renderPanel("ModifierEquipePanel");
                    DaoManager.getInstance().getEquipesDao().updateEquipe(this.equipe);
                } else  {
                    PanelsManager.getInstance().setNotification("Le joueur est déjà dans une équipe.");
                }
            } else {
                PanelsManager.getInstance().setNotification("Veuillez remplir les champs vide.");
            }
        });
        final Button supprimerButton = new Button();
        supprimerButton.setText("Supprimer le joueur");
        supprimerButton.setMinWidth(350);
        supprimerButton.setMinHeight(60);
        supprimerButton.setTranslateY(20);
        supprimerButton.setOnAction(event -> {
            if (ajouterJoueurChoice.getValue() != null) {
                this.equipe.removeJoueur((Joueur) UtilisateursManager.getInstance().getJoueurByChoice(ajouterJoueurChoice.getValue().toString()));
                PanelsManager.getInstance().renderPanel("ModifierEquipePanel");
                DaoManager.getInstance().getEquipesDao().updateEquipe(this.equipe);
            } else {
                PanelsManager.getInstance().setNotification("Veuillez remplir les champs vide.");
            }
        });


        ajouterJoueurBox.getChildren().addAll(ajouterJoueurChoice, ajouterButton, supprimerButton);
        ajouterJoueurGrid.add(ajouterJoueurBox, 0, 0);
        Label joueursLabel = new Label("Liste des joueurs:");
        ajouterJoueurGrid.add(joueursLabel, 1, 0);
        joueursLabel.setStyle("-fx-font-size: 20px");
        ScrollPane tableScroll = new ScrollPane();
        TableView<Joueur> table = new TableView<>();
        tableScroll.setContent(table);
        tableScroll.setMaxHeight(200);
        tableScroll.setMinWidth(300);
        table.setMaxWidth(305);
        table.setMaxHeight(200);
        TableColumn<Joueur, String> nom = new TableColumn<>("Nom");
        TableColumn<Joueur, String> prenom = new TableColumn<>("Prénom");
        TableColumn<Joueur, String> numero = new TableColumn<>("Numero");
        TableColumn<Joueur, String> poste = new TableColumn<>("Poste");
        nom.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getNom()));
        prenom.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getPrenom()));
        numero.setCellValueFactory(c -> new SimpleStringProperty(Integer.toString(c.getValue().getNumero())));
        poste.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getPoste()));
        for (Joueur joueur : this.equipe.getJoueurs()) {
            table.getItems().addAll(joueur);
        }

        table.getColumns().addAll(nom, prenom, numero, poste);
        ajouterJoueurGrid.add(tableScroll, 1, 0);
        grid.add(ajouterJoueurGrid, 0, 2);


        Label entraineursTitle = new Label("Gestion des entraineurs:");
        grid.add(entraineursTitle, 0, 3);
        entraineursTitle.setStyle("-fx-font-size: 20px");

        GridPane ajouterEntraineurGrid = new GridPane();
        VBox ajouterEntraineurBox = new VBox();
        ChoiceBox ajouterEntraineurChoice = new ChoiceBox();
        for (Utilisateur utilisateur : UtilisateursManager.getInstance().getUtilisateurs()) {
            if (utilisateur instanceof Entraineur) {
                ajouterEntraineurChoice.getItems().add(utilisateur.getNom() + " - " + utilisateur.getPrenom());

            }
        }
        ajouterEntraineurChoice.setMinWidth(350);
        ajouterEntraineurChoice.setMinHeight(60);
        ajouterEntraineurChoice.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");
        final Button ajouterEntraineurButton = new Button();
        ajouterEntraineurButton.setText("Affecter l'entraineur");
        ajouterEntraineurButton.setMinWidth(350);
        ajouterEntraineurButton.setMinHeight(60);
        ajouterEntraineurButton.setTranslateY(10);

        ajouterEntraineurButton.setOnAction(event -> {
            if (ajouterEntraineurChoice.getValue() != null) {
                Entraineur entraineur = (Entraineur) UtilisateursManager.getInstance().getEntraineurByChoice(ajouterEntraineurChoice.getValue().toString());

                if(EquipesManager.getInstance().getEquipeByEntraineur(entraineur) == null){
                    this.equipe.addEntraineur(entraineur);
                    PanelsManager.getInstance().renderPanel("ModifierEquipePanel");
                    DaoManager.getInstance().getEquipesDao().updateEquipe(this.equipe);
                } else {
                    PanelsManager.getInstance().setNotification("L'entraineur entraîne déjà une équipe.");
                }

            } else {
                PanelsManager.getInstance().setNotification("Veuillez remplir les champs vide.");
            }
        });


        final Button supprimerEntraineurButton = new Button();
        supprimerEntraineurButton.setText("Désaffecter l'entraineur");
        supprimerEntraineurButton.setMinWidth(350);
        supprimerEntraineurButton.setMinHeight(60);
        supprimerEntraineurButton.setTranslateY(20);

        supprimerEntraineurButton.setOnAction(event -> {
            if (ajouterEntraineurChoice.getValue() != null) {
                this.equipe.removeEntraineur((Entraineur) UtilisateursManager.getInstance().getEntraineurByChoice(ajouterEntraineurChoice.getValue().toString()));
                PanelsManager.getInstance().renderPanel("ModifierEquipePanel");
                DaoManager.getInstance().getEquipesDao().updateEquipe(this.equipe);
            } else {
                PanelsManager.getInstance().setNotification("Veuillez remplir les champs vide.");
            }
        });


        ajouterEntraineurBox.getChildren().addAll(ajouterEntraineurChoice, ajouterEntraineurButton, supprimerEntraineurButton);
        ajouterEntraineurGrid.add(ajouterEntraineurBox, 0, 0);
        Label entraineursLabel = new Label("Liste des joueurs:");
        ajouterEntraineurGrid.add(entraineursLabel, 1, 0);
        entraineursLabel.setStyle("-fx-font-size: 20px");
        ScrollPane tableEntraineurScroll = new ScrollPane();
        TableView<Entraineur> tableEntraineur = new TableView<>();
        tableEntraineurScroll.setContent(tableEntraineur);
        tableEntraineurScroll.setMaxHeight(200);
        tableEntraineurScroll.setMinWidth(325);
        tableEntraineur.setMinWidth(310);
        tableEntraineur.setMaxHeight(200);
        TableColumn<Entraineur, String> nomEntraineur = new TableColumn<>("Nom");
        TableColumn<Entraineur, String> prenomEntraineur = new TableColumn<>("Prénom");
        nomEntraineur.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getNom()));
        prenomEntraineur.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getPrenom()));
        for (Entraineur entraineur : this.equipe.getEntraineurs()) {
            tableEntraineur.getItems().addAll(entraineur);
        }
        tableEntraineur.getColumns().addAll(nomEntraineur, prenomEntraineur);
        ajouterEntraineurGrid.add(tableEntraineurScroll, 1, 0);
        grid.add(ajouterEntraineurGrid, 0, 4);


        if (UtilisateursManager.getInstance().getConnectedUtilisateur() instanceof Dirigeant) {
            Label categorieLabel = new Label("Catégorie:");
            grid.add(categorieLabel, 0, 5);
            categorieLabel.setStyle("-fx-font-size: 20px");

            ChoiceBox categorieChoice = new ChoiceBox();
            for (Categorie categorie : CategoriesManager.getInstance().getCategories()) {
                categorieChoice.getItems().add(categorie.getNom());
            }
            grid.add(categorieChoice, 0, 6);
            categorieChoice.setMinWidth(700);
            categorieChoice.setMinHeight(40);
            categorieChoice.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");
            categorieChoice.setValue(this.equipe.getCategorie().getNom());
            Label nomLabel = new Label("Nom de l'équipe:");
            grid.add(nomLabel, 0, 7);
            nomLabel.setStyle("-fx-font-size: 20px");

            TextField nomField = new TextField();
            grid.add(nomField, 0, 8);
            nomField.setMinWidth(650);
            nomField.setMinHeight(40);
            nomField.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");
            nomField.setText(this.equipe.getNom());

            Label ecussonLabel = new Label("Ecusson:");
            grid.add(ecussonLabel, 0, 9);
            ecussonLabel.setStyle("-fx-font-size: 20px");

            TextField ecussonField = new TextField();
            grid.add(ecussonField, 0, 10);
            ecussonField.setMinWidth(600);
            ecussonField.setMinHeight(40);
            ecussonField.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");
            ecussonField.setText(this.equipe.getEcusson());

            final Button modifierEquipeButton = new Button();
            modifierEquipeButton.setText("Enregistrer les informations de l'équipe");
            modifierEquipeButton.setMinWidth(700);
            modifierEquipeButton.setMinHeight(40);


            modifierEquipeButton.setOnAction(event -> {
                if (categorieChoice.getValue() != null && !ecussonField.getText().equals("") && !nomField.getText().equals("")) {
                    this.equipe.setNom(nomField.getText());
                    this.equipe.setEcusson(ecussonField.getText());
                    this.equipe.setCategorie(CategoriesManager.getInstance().getCategorieByNom(categorieChoice.getValue().toString()));

                    PanelsManager.getInstance().setNotification("Les données ont bien été enregistré.");
                    DaoManager.getInstance().getEquipesDao().updateEquipe(this.equipe);
                } else {
                    PanelsManager.getInstance().setNotification("Veuillez remplir les champs vide.");
                }
            });

            final Button supprimerEquipeButton = new Button();
            supprimerEquipeButton.setText("Supprimer l'équipe");
            supprimerEquipeButton.setMinWidth(700);
            supprimerEquipeButton.setMinHeight(40);

            supprimerEquipeButton.setOnAction(event -> {
                EquipesManager.getInstance().getEquipes().remove(this.equipe);
                DaoManager.getInstance().getEquipesDao().deleteEquipe(this.equipe);
                PanelsManager.getInstance().renderPanel("ListeEquipesPanel");
                PanelsManager.getInstance().setNotification("L'équipe a bien été supprimé.");
            });

            grid.add(modifierEquipeButton, 0, 11);
            grid.add(supprimerEquipeButton, 0, 12);

        }


        scrollPane.setContent(grid);


        this.pane.getChildren().add(scrollPane);
    }

    public void render() {
        this.draw();
        PanelsManager.getInstance().getRoot().getChildren().add(this.pane);
    }

    public AnchorPane getPane() {
        return this.pane;
    }

    public void setEquipe(Equipe equipe) {
        this.equipe = equipe;
    }
}

