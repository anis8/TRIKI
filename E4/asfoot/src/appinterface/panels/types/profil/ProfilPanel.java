package appinterface.panels.types.profil;

import appinterface.panels.IPanel;
import appinterface.panels.PanelsManager;
import asfoot.dao.DaoManager;
import asfoot.utilisateurs.Utilisateur;
import asfoot.utilisateurs.UtilisateursManager;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

public class ProfilPanel implements IPanel {

    AnchorPane pane;

    public ProfilPanel() {
        this.pane = new AnchorPane();
        this.pane.setTranslateY(55);
        this.pane.setTranslateX(13);
        this.pane.setMinWidth(PanelsManager.getInstance().getRoot().getScene().getWidth() - 26);
        this.pane.setMinHeight(PanelsManager.getInstance().getRoot().getScene().getHeight() - 70);
        this.pane.setStyle("-fx-font-family: 'Trebuchet MS', Helvetica, sans-serif;");
    }

    public void draw() {
        GridPane grid = new GridPane();
        grid.setMinWidth(this.pane.getMinWidth());
        grid.setMinHeight(this.pane.getMinHeight());
        grid.setAlignment(Pos.CENTER);
        if (UtilisateursManager.getInstance().getConnectedUtilisateur() == null) {


            Text scenetitle = new Text("Connexion");
            scenetitle.setStyle("-fx-font-size: 60px;");
            grid.add(scenetitle, 0, 0, 2, 1);


            Label mailLabel = new Label("Mon adresse email:");
            grid.add(mailLabel, 0, 2);
            mailLabel.setStyle("-fx-font-size: 20px");

            TextField mailField = new TextField();
            grid.add(mailField, 0, 3);
            mailField.setMinWidth(300);
            mailField.setMinHeight(40);
            mailField.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");


            Label passwordLabel = new Label("Mot de passe:");
            grid.add(passwordLabel, 0, 4);
            passwordLabel.setStyle("-fx-font-size: 20px");

            PasswordField passwordField = new PasswordField();
            grid.add(passwordField, 0, 5);
            passwordField.setMinWidth(300);
            passwordField.setMinHeight(40);
            passwordField.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");


            final Button connectionButton = new Button();
            connectionButton.setText("Connexion");
            grid.add(connectionButton, 0, 6);
            connectionButton.setMinWidth(300);
            connectionButton.setMinHeight(30);
            connectionButton.setTranslateY(10);



            final Button inscriptionButton = new Button();
            inscriptionButton.setText("Inscription");
            grid.add(inscriptionButton, 0, 7);
            inscriptionButton.setMinWidth(300);
            inscriptionButton.setMinHeight(30);
            inscriptionButton.setTranslateY(10);

            connectionButton.setOnAction(e -> {
                if (DaoManager.getInstance().getUtilisateursDao().connectUtilisateur(mailField.getText(), passwordField.getText())) {
                    PanelsManager.getInstance().renderPanel("ProfilPanel");
                } else {
                    PanelsManager.getInstance().setNotification("Les identifiants sont incorrects.");
                }
            });

            inscriptionButton.setOnAction(e -> {
                PanelsManager.getInstance().renderPanel("InscriptionPanel");
            });


        } else {
            grid.setMinWidth(this.pane.getMinWidth());
            grid.setMinHeight(this.pane.getMinHeight());
            grid.setAlignment(Pos.CENTER);

            Text phraseLabel = new Text("Bonjour "+UtilisateursManager.getInstance().getConnectedUtilisateur().getPrenom() + ", de retour sur ASFOOT.");
            grid.add(phraseLabel, 0, 1);
            phraseLabel.setStyle("-fx-font-size: 30px");

            Text phraseLabel2 = new Text("Connecté en tant que " + UtilisateursManager.getInstance().getUtilisateurType(UtilisateursManager.getInstance().getConnectedUtilisateur()));
            grid.add(phraseLabel2, 0, 2);
            phraseLabel2.setStyle("-fx-font-size: 30px");

        }


        this.pane.getChildren().add(grid);
    }

    public void render() {
        this.draw();
        PanelsManager.getInstance().getRoot().getChildren().add(this.pane);
    }

    public AnchorPane getPane() {
        return this.pane;
    }
}
