package appinterface.panels.types.matchs;

import appinterface.panels.IPanel;
import appinterface.panels.PanelsManager;
import asfoot.categories.CategoriesManager;
import asfoot.dao.DaoManager;
import asfoot.equipes.Equipe;
import asfoot.equipes.EquipesManager;
import asfoot.matchs.Match;
import asfoot.matchs.MatchsManager;
import asfoot.stades.Stade;
import asfoot.stades.StadesManager;
import asfoot.utilisateurs.Utilisateur;
import asfoot.utilisateurs.UtilisateursManager;
import asfoot.utilisateurs.types.Dirigeant;
import asfoot.utilisateurs.types.Entraineur;
import asfoot.utilisateurs.types.Joueur;
import asfoot.utilisateurs.types.Parent;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

public class AjouterMatchPanel implements IPanel {

    private AnchorPane pane;

    public AjouterMatchPanel() {
        this.pane = new AnchorPane();
        this.pane.setTranslateY(55);
        this.pane.setTranslateX(13);
        this.pane.setMinWidth(PanelsManager.getInstance().getRoot().getScene().getWidth() - 26);
        this.pane.setMinHeight(PanelsManager.getInstance().getRoot().getScene().getHeight() - 70);
        this.pane.setStyle("-fx-font-family: 'Trebuchet MS', Helvetica, sans-serif;");
    }

    @SuppressWarnings("Duplicates")
    public void draw() {
        GridPane grid = new GridPane();
        grid.setMinWidth(this.pane.getMinWidth());
        grid.setMinHeight(this.pane.getMinHeight());
        grid.setAlignment(Pos.CENTER);


        Text scenetitle = new Text("Ajouter un match");
        scenetitle.setStyle("-fx-font-size: 60px;");
        grid.add(scenetitle, 0, 0, 1, 1);

        Label stadeLabel = new Label("Stade:");
        grid.add(stadeLabel, 0, 1);
        stadeLabel.setStyle("-fx-font-size: 20px");


        ChoiceBox stadeChoice = new ChoiceBox();
        for (Stade stade : StadesManager.getInstance().getStades()) {
            stadeChoice.getItems().add(stade.getNom());
        }
        grid.add(stadeChoice, 0, 2);
        stadeChoice.setMinWidth(600);
        stadeChoice.setMinHeight(40);
        stadeChoice.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");


        Label equipeUneLabel = new Label("Equipe une:");
        grid.add(equipeUneLabel, 0, 3);
        equipeUneLabel.setStyle("-fx-font-size: 20px");


        ChoiceBox equipeUneChoice = new ChoiceBox();
        for (Equipe equipe : EquipesManager.getInstance().getEquipes()) {
            equipeUneChoice.getItems().add(equipe.getNom() + " - " + equipe.getCategorie().getNom());
        }
        grid.add(equipeUneChoice, 0, 4);
        equipeUneChoice.setMinWidth(600);
        equipeUneChoice.setMinHeight(40);
        equipeUneChoice.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");


        Label equipeDeuxLabel = new Label("Equipe deux:");
        grid.add(equipeDeuxLabel, 0, 5);
        equipeDeuxLabel.setStyle("-fx-font-size: 20px");


        ChoiceBox equipeDeuxChoice = new ChoiceBox();
        for (Equipe equipe : EquipesManager.getInstance().getEquipes()) {
            equipeDeuxChoice.getItems().add(equipe.getNom() + " - " + equipe.getCategorie().getNom());
        }
        grid.add(equipeDeuxChoice, 0, 6);
        equipeDeuxChoice.setMinWidth(600);
        equipeDeuxChoice.setMinHeight(40);
        equipeDeuxChoice.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");

        Label dateLabel = new Label("Date:");
        grid.add(dateLabel, 0, 7);
        dateLabel.setStyle("-fx-font-size: 20px");

        TextField dateField = new TextField();
        grid.add(dateField, 0, 8);
        dateField.setMinWidth(300);
        dateField.setMinHeight(40);
        dateField.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");


        final Button ajouterButton = new Button();
        ajouterButton.setText("Ajouter");
        grid.add(ajouterButton, 0, 9);
        ajouterButton.setMinWidth(600);
        ajouterButton.setMinHeight(30);
        ajouterButton.setTranslateY(10);


        ajouterButton.setOnAction(e -> {

            if (stadeChoice.getValue() != null && equipeUneChoice.getValue() != null && equipeDeuxChoice.getValue() != null) {
                Match match = new Match(
                        StadesManager.getInstance().getStadeByNom(stadeChoice.getValue().toString()),
                        EquipesManager.getInstance().getEquipeByChoice(equipeUneChoice.getValue().toString()),
                        EquipesManager.getInstance().getEquipeByChoice(equipeDeuxChoice.getValue().toString()),
                        0,
                        dateField.getText()
                );

                MatchsManager.getInstance().addMatch(match);
                DaoManager.getInstance().getMatchsDao().createMatch(match);
                PanelsManager.getInstance().setNotification("Le match a bien été ajouté.");
            } else {
                PanelsManager.getInstance().setNotification("Veuillez remplir les champs vide.");
            }

        });

        this.pane.getChildren().add(grid);
    }

    public void render() {
        this.draw();
        PanelsManager.getInstance().getRoot().getChildren().add(this.pane);
    }

    public AnchorPane getPane() {
        return this.pane;
    }
}
