package appinterface.panels.types.stades;

import appinterface.panels.IPanel;
import appinterface.panels.PanelsManager;
import appinterface.panels.types.matchs.ModifierMatchPanel;
import asfoot.matchs.Match;
import asfoot.matchs.MatchsManager;
import asfoot.matchs.types.MatchTermine;
import asfoot.stades.Stade;
import asfoot.stades.StadesManager;
import asfoot.utilisateurs.Utilisateur;
import asfoot.utilisateurs.UtilisateursManager;
import asfoot.utilisateurs.types.Dirigeant;
import asfoot.utilisateurs.types.Entraineur;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;

public class ListeStadesPanel implements IPanel {

    private AnchorPane pane;

    public ListeStadesPanel() {
        this.pane = new AnchorPane();
        this.pane.setTranslateY(55);
        this.pane.setTranslateX(13);
        this.pane.setMinWidth(PanelsManager.getInstance().getRoot().getScene().getWidth() - 26);
        this.pane.setMinHeight(PanelsManager.getInstance().getRoot().getScene().getHeight() - 70);
        this.pane.setStyle("-fx-font-family: 'Trebuchet MS', Helvetica, sans-serif;");
    }

    @SuppressWarnings("Duplicates")
    public void draw() {
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setMinHeight(this.pane.getMinHeight());
        scrollPane.setMinWidth(this.pane.getMinWidth());

        TableView<Stade> table = new TableView<>();
        table.setMinWidth(this.pane.getMinWidth());
        table.setMinHeight(this.pane.getMinHeight());

        TableColumn<Stade, String> nom = new TableColumn<>("Nom du stade");
        TableColumn<Stade, String> ville = new TableColumn<>("Ville");
        TableColumn<Stade, String> code = new TableColumn<>("Code postal");


        nom.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getNom()));
        ville.setCellValueFactory(c -> new SimpleStringProperty(c.getValue().getVille()));
        code.setCellValueFactory(c -> new SimpleStringProperty(Integer.toString(c.getValue().getCodePostal())));

        for (Stade stade : StadesManager.getInstance().getStades()) {
            table.getItems().addAll(stade);
        }

        table.setRowFactory(tv -> {
            TableRow<Stade> row = new TableRow<>();
            row.setOnMouseClicked(event -> {
                if (!row.isEmpty() && event.getButton() == MouseButton.PRIMARY
                        && event.getClickCount() == 2) {
                    Stade stade = row.getItem();
                    IPanel panel = PanelsManager.getInstance().getPanel("ModifierStadesPanel");
                    if (panel instanceof ModifierStadesPanel) {
                        ((ModifierStadesPanel) panel).setStade(stade);
                    }
                    PanelsManager.getInstance().renderPanel("ModifierStadesPanel");

                }
            });
            return row;
        });

        table.getColumns().addAll(nom, ville, code);
        scrollPane.setContent(table);
        this.pane.getChildren().add(scrollPane);

    }

    public void render() {
        this.draw();
        PanelsManager.getInstance().getRoot().getChildren().add(this.pane);
    }

    public AnchorPane getPane() {
        return this.pane;
    }
}

