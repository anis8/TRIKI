package appinterface.panels.types.stades;


import appinterface.panels.IPanel;
import appinterface.panels.PanelsManager;
import asfoot.dao.DaoManager;
import asfoot.stades.Stade;
import asfoot.stades.StadesManager;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;

public class ModifierStadesPanel implements IPanel {

    private AnchorPane pane;
    private Stade stade;

    public ModifierStadesPanel() {
        this.pane = new AnchorPane();
        this.pane.setTranslateY(55);
        this.pane.setTranslateX(13);
        this.pane.setMinWidth(PanelsManager.getInstance().getRoot().getScene().getWidth() - 26);
        this.pane.setMinHeight(PanelsManager.getInstance().getRoot().getScene().getHeight() - 70);
        this.pane.setStyle("-fx-font-family: 'Trebuchet MS', Helvetica, sans-serif;");
        this.stade = null;
    }

    @SuppressWarnings("Duplicates")
    public void draw() {
        GridPane grid = new GridPane();
        grid.setMinWidth(this.pane.getMinWidth());
        grid.setMinHeight(this.pane.getMinHeight());
        grid.setAlignment(Pos.CENTER);


        Text scenetitle = new Text("Modifier un stade");
        scenetitle.setStyle("-fx-font-size: 60px;");
        grid.add(scenetitle, 0, 0, 1, 1);

        Label nomLabel = new Label("Nom:");
        grid.add(nomLabel, 0, 1);
        nomLabel.setStyle("-fx-font-size: 20px");

        TextField nomField = new TextField();
        grid.add(nomField, 0, 2);
        nomField.setMinWidth(300);
        nomField.setMinHeight(40);
        nomField.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");
        nomField.setText(this.stade.getNom());


        Label villeLabel = new Label("Ville:");
        grid.add(villeLabel, 0, 3);
        villeLabel.setStyle("-fx-font-size: 20px");

        TextField villeField = new TextField();
        grid.add(villeField, 0, 4);
        villeField.setMinWidth(300);
        villeField.setMinHeight(40);
        villeField.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");
        villeField.setText(this.stade.getVille());

        Label postalLabel = new Label("Code postal:");
        grid.add(postalLabel, 0, 5);
        postalLabel.setStyle("-fx-font-size: 20px");

        TextField postalField = new TextField();
        grid.add(postalField, 0, 6);
        postalField.setMinWidth(300);
        postalField.setMinHeight(40);
        postalField.setStyle("-fx-text-box-border: rgb(127,127,127) ;-fx-focus-color: rgb(127,127,127);");
        postalField.setText(Integer.toString(this.stade.getCodePostal()));

        final Button ajouterButton = new Button();
        ajouterButton.setText("Modifier");
        grid.add(ajouterButton, 0, 9);
        ajouterButton.setMinWidth(600);
        ajouterButton.setMinHeight(30);
        ajouterButton.setTranslateY(10);
        ajouterButton.setOnAction(e -> {
            if (!nomField.getText().equals("") && !villeField.getText().equals("") && !postalField.getText().equals("")) {
                this.stade.setNom(nomField.getText());
                this.stade.setVille(villeField.getText());
                this.stade.setCodePostal(Integer.parseInt(postalField.getText()));
                DaoManager.getInstance().getStadesDao().updateStade(stade);
                PanelsManager.getInstance().setNotification("Le stade a bien été mis à jour");
            } else {
                PanelsManager.getInstance().setNotification("Veuillez remplir les champs vide.");
            }
        });

        final Button supprimerButton = new Button();
        supprimerButton.setText("Supprimer");
        grid.add(supprimerButton, 0, 10);
        supprimerButton.setMinWidth(600);
        supprimerButton.setMinHeight(30);
        supprimerButton.setTranslateY(10);
        supprimerButton.setOnAction(e -> {
            StadesManager.getInstance().getStades().remove(this.stade);
            DaoManager.getInstance().getStadesDao().deleteStade(this.stade);
            PanelsManager.getInstance().renderPanel("ListeStadesPanel");
            PanelsManager.getInstance().setNotification("Le stade a bien été supprimé");
        });


        this.pane.getChildren().add(grid);
    }

    public void render() {
        this.draw();
        PanelsManager.getInstance().getRoot().getChildren().add(this.pane);
    }

    public AnchorPane getPane() {
        return this.pane;
    }

    public void setStade(Stade stade) {
        this.stade = stade;
    }
}

